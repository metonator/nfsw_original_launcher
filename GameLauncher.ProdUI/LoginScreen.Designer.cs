using GameLauncher;
using GameLauncher.ProdUI.Controls;
using GameLauncher.ProdUI.Properties;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class LoginScreen : BaseScreen
	{
		private string mPictureBoxHorizontalLink = string.Empty;

		private string mPictureBoxVerticalLink = string.Empty;

		private GameLauncherUI parentForm;

		private IContainer components;

		private TextBox wTextBoxEmail;

		private TextBox wTextBoxPassword;

		private CheckBox wCheckBoxRememberMe;

		private LinkLabel wLinkLabelForgetPassword;

		private Label wLabelEmail;

		private Label wLabelPassword;

		private PictureBox wPictureBoxServerStatusOff;

		private Label wLabelServerStatus;

		private PictureBox wPictureBoxServerStatusOn;

		private PictureBox wPictureBoxHorizontal;

		private Label wLabelAdvertisement;

		private Label wLabelHavingTrouble;

		private LinkLabel wLinkLabelCustomerService;

		private PictureBox wPictureBoxVertical;

		private LinkLabel wLinkLabelNewAccount;

		private MainButton wLoginButton;

		private Label wLabelLoginInfo;

		private ShardComboBox wShardComboBox;

		private OptionsButton wOptionsButton;

		private Label wLabelShard;

		private System.Windows.Forms.Timer wTimerServerStatusCheck;

		public LoginScreen(GameLauncherUI launcherForm) : base(launcherForm)
		{
			this.parentForm = launcherForm;
			this.InitializeComponent();
			this.LocalizeFE();
			this.ApplyEmbeddedFonts();
			this.InitializeSettings();
		}

		protected override void ApplyEmbeddedFonts()
		{
			FontFamily fontFamily = FontWrapper.Instance.GetFontFamily("MyriadProSemiCondBold.ttf");
			FontWrapper.Instance.GetFontFamily("MyriadPro-SemiCn.ttf");
			FontFamily fontFamily1 = FontWrapper.Instance.GetFontFamily("Reg-DB-I.ttf");
			FontFamily fontFamily2 = FontWrapper.Instance.GetFontFamily("Reg-B-I.ttf");
			FontFamily fontFamily3 = FontWrapper.Instance.GetFontFamily("REGIDB__.TTF");
			float single = 1f;
			if (string.Compare(Thread.CurrentThread.CurrentUICulture.Name, "th-TH", true) == 0)
			{
				single = 1.2f;
			}
			if (string.Compare(Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName, "zh", true) != 0)
			{
				this.wCheckBoxRememberMe.Font = new System.Drawing.Font(fontFamily, 9f, FontStyle.Bold);
				this.wLinkLabelForgetPassword.Font = new System.Drawing.Font(fontFamily, 9f, FontStyle.Bold);
			}
			else
			{
				this.wCheckBoxRememberMe.Font = new System.Drawing.Font(fontFamily3, 9f);
				this.wLinkLabelForgetPassword.Font = new System.Drawing.Font(fontFamily3, 9f);
			}
			this.wLoginButton.wLabelButton.Font = new System.Drawing.Font(fontFamily2, 15f, FontStyle.Bold | FontStyle.Italic);
			this.wLabelServerStatus.Font = new System.Drawing.Font(fontFamily, 9.749999f * single, FontStyle.Bold);
			this.wLabelAdvertisement.Font = new System.Drawing.Font(fontFamily, 8f, FontStyle.Bold);
			this.wLabelLoginInfo.Font = new System.Drawing.Font(fontFamily1, 12.75f, FontStyle.Italic);
			this.wLabelEmail.Font = new System.Drawing.Font(fontFamily3, 11f);
			this.wLabelPassword.Font = new System.Drawing.Font(fontFamily3, 11f);
			this.wLabelHavingTrouble.Font = new System.Drawing.Font(fontFamily, 9.749999f * single, FontStyle.Bold);
			this.wLinkLabelCustomerService.Font = new System.Drawing.Font(fontFamily, 9.749999f * single, FontStyle.Bold);
			this.wLinkLabelNewAccount.Font = new System.Drawing.Font(fontFamily, 9.749999f * single, FontStyle.Bold);
			this.wLabelShard.Font = new System.Drawing.Font(fontFamily, 9.749999f * single, FontStyle.Bold);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.wTextBoxEmail = new TextBox();
			this.wTextBoxPassword = new TextBox();
			this.wCheckBoxRememberMe = new CheckBox();
			this.wLinkLabelForgetPassword = new LinkLabel();
			this.wLabelEmail = new Label();
			this.wLabelPassword = new Label();
			this.wPictureBoxServerStatusOff = new PictureBox();
			this.wLabelServerStatus = new Label();
			this.wPictureBoxServerStatusOn = new PictureBox();
			this.wPictureBoxHorizontal = new PictureBox();
			this.wLabelAdvertisement = new Label();
			this.wLabelHavingTrouble = new Label();
			this.wLinkLabelCustomerService = new LinkLabel();
			this.wPictureBoxVertical = new PictureBox();
			this.wLinkLabelNewAccount = new LinkLabel();
			this.wLoginButton = new MainButton();
			this.wLabelLoginInfo = new Label();
			this.wShardComboBox = new ShardComboBox();
			this.wOptionsButton = new OptionsButton();
			this.wLabelShard = new Label();
			this.wTimerServerStatusCheck = new System.Windows.Forms.Timer(this.components);
			((ISupportInitialize)this.wPictureBoxServerStatusOff).BeginInit();
			((ISupportInitialize)this.wPictureBoxServerStatusOn).BeginInit();
			((ISupportInitialize)this.wPictureBoxHorizontal).BeginInit();
			((ISupportInitialize)this.wPictureBoxVertical).BeginInit();
			base.SuspendLayout();
			this.wTextBoxEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.wTextBoxEmail.Location = new Point(33, 183);
			this.wTextBoxEmail.Name = "wTextBoxEmail";
			this.wTextBoxEmail.Size = new System.Drawing.Size(210, 20);
			this.wTextBoxEmail.TabIndex = 1;
			this.wTextBoxEmail.TextChanged += new EventHandler(this.wLogin_TextChanged);
			this.wTextBoxPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.wTextBoxPassword.Location = new Point(276, 183);
			this.wTextBoxPassword.MaxLength = 16;
			this.wTextBoxPassword.Name = "wTextBoxPassword";
			this.wTextBoxPassword.Size = new System.Drawing.Size(210, 20);
			this.wTextBoxPassword.TabIndex = 2;
			this.wTextBoxPassword.UseSystemPasswordChar = true;
			this.wTextBoxPassword.TextChanged += new EventHandler(this.wLogin_TextChanged);
			this.wTextBoxPassword.PreviewKeyDown += new PreviewKeyDownEventHandler(this.wLoginButton_PreviewKeyDown);
			this.wCheckBoxRememberMe.AutoSize = true;
			this.wCheckBoxRememberMe.BackColor = Color.Transparent;
			this.wCheckBoxRememberMe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999f, FontStyle.Bold);
			this.wCheckBoxRememberMe.ForeColor = Color.Transparent;
			this.wCheckBoxRememberMe.Location = new Point(33, 209);
			this.wCheckBoxRememberMe.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
			this.wCheckBoxRememberMe.Name = "wCheckBoxRememberMe";
			this.wCheckBoxRememberMe.Size = new System.Drawing.Size(244, 19);
			this.wCheckBoxRememberMe.TabIndex = 3;
			this.wCheckBoxRememberMe.Text = "REMEMBER MY EMAIL ADDRESS";
			this.wCheckBoxRememberMe.UseVisualStyleBackColor = false;
			this.wLinkLabelForgetPassword.ActiveLinkColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelForgetPassword.BackColor = Color.Transparent;
			this.wLinkLabelForgetPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLinkLabelForgetPassword.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelForgetPassword.LinkArea = new LinkArea(0, 100);
			this.wLinkLabelForgetPassword.LinkColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelForgetPassword.Location = new Point(276, 206);
			this.wLinkLabelForgetPassword.Name = "wLinkLabelForgetPassword";
			this.wLinkLabelForgetPassword.Size = new System.Drawing.Size(210, 21);
			this.wLinkLabelForgetPassword.TabIndex = 4;
			this.wLinkLabelForgetPassword.TabStop = true;
			this.wLinkLabelForgetPassword.Text = "I forgot my password.";
			this.wLinkLabelForgetPassword.TextAlign = ContentAlignment.BottomRight;
			this.wLinkLabelForgetPassword.UseCompatibleTextRendering = true;
			this.wLinkLabelForgetPassword.LinkClicked += new LinkLabelLinkClickedEventHandler(this.wLinkLabelForgetPassword_LinkClicked);
			this.wLabelEmail.BackColor = Color.Transparent;
			this.wLabelEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.wLabelEmail.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLabelEmail.Location = new Point(30, 161);
			this.wLabelEmail.Name = "wLabelEmail";
			this.wLabelEmail.Size = new System.Drawing.Size(212, 19);
			this.wLabelShard.TabStop = false;
			this.wLabelEmail.Text = "EMAIL ADDRESS:";
			this.wLabelEmail.TextAlign = ContentAlignment.BottomLeft;
			this.wLabelPassword.BackColor = Color.Transparent;
			this.wLabelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25f);
			this.wLabelPassword.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLabelPassword.Location = new Point(276, 161);
			this.wLabelPassword.Name = "wLabelPassword";
			this.wLabelPassword.Size = new System.Drawing.Size(210, 19);
			this.wLabelShard.TabStop = false;
			this.wLabelPassword.Text = "PASSWORD:";
			this.wLabelPassword.TextAlign = ContentAlignment.BottomLeft;
			this.wPictureBoxServerStatusOff.BackColor = Color.Transparent;
			this.wPictureBoxServerStatusOff.Image = Resources.launcher_redlight;
			this.wPictureBoxServerStatusOff.Location = new Point(20, 335);
			this.wPictureBoxServerStatusOff.Name = "wPictureBoxServerStatusOff";
			this.wPictureBoxServerStatusOff.Size = new System.Drawing.Size(16, 16);
			this.wLabelShard.TabStop = false;
			this.wPictureBoxServerStatusOff.TabStop = false;
			this.wPictureBoxServerStatusOff.Visible = false;
			this.wLabelServerStatus.BackColor = Color.Transparent;
			this.wLabelServerStatus.Cursor = Cursors.WaitCursor;
			this.wLabelServerStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLabelServerStatus.ForeColor = Color.White;
			this.wLabelServerStatus.Location = new Point(41, 312);
			this.wLabelServerStatus.Name = "wLabelServerStatus";
			this.wLabelServerStatus.Size = new System.Drawing.Size(740, 56);
			this.wLabelShard.TabStop = false;
			this.wLabelServerStatus.Text = "Retrieving server status...";
			this.wLabelServerStatus.TextAlign = ContentAlignment.MiddleLeft;
			this.wPictureBoxServerStatusOn.BackColor = Color.Transparent;
			this.wPictureBoxServerStatusOn.Image = Resources.launcher_greenlight;
			this.wPictureBoxServerStatusOn.Location = new Point(20, 323);
			this.wPictureBoxServerStatusOn.Name = "wPictureBoxServerStatusOn";
			this.wPictureBoxServerStatusOn.Size = new System.Drawing.Size(16, 16);
			this.wLabelShard.TabStop = false;
			this.wPictureBoxServerStatusOn.TabStop = false;
			this.wPictureBoxServerStatusOn.Visible = false;
			this.wPictureBoxHorizontal.BackColor = Color.Transparent;
			this.wPictureBoxHorizontal.Location = new Point(34, 389);
			this.wPictureBoxHorizontal.Name = "wPictureBoxHorizontal";
			this.wPictureBoxHorizontal.Size = new System.Drawing.Size(720, 88);
			this.wLabelShard.TabStop = false;
			this.wPictureBoxHorizontal.TabStop = false;
			this.wPictureBoxHorizontal.Visible = false;
			this.wPictureBoxHorizontal.Click += new EventHandler(this.wPictureBoxHorizontal_Click);
			this.wLabelAdvertisement.BackColor = Color.Transparent;
			this.wLabelAdvertisement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLabelAdvertisement.ForeColor = Color.FromArgb(69, 133, 193);
			this.wLabelAdvertisement.Location = new Point(308, 375);
			this.wLabelAdvertisement.Name = "wLabelAdvertisement";
			this.wLabelAdvertisement.Size = new System.Drawing.Size(200, 13);
			this.wLabelShard.TabStop = false;
			this.wLabelAdvertisement.Text = "ADVERTISEMENT";
			this.wLabelAdvertisement.TextAlign = ContentAlignment.MiddleCenter;
			this.wLabelHavingTrouble.AutoSize = true;
			this.wLabelHavingTrouble.BackColor = Color.Transparent;
			this.wLabelHavingTrouble.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLabelHavingTrouble.ForeColor = Color.White;
			this.wLabelHavingTrouble.Location = new Point(30, 255);
			this.wLabelHavingTrouble.Name = "wLabelHavingTrouble";
			this.wLabelHavingTrouble.Size = new System.Drawing.Size(148, 16);
			this.wLabelShard.TabStop = false;
			this.wLabelHavingTrouble.Text = "HAVING TROUBLE?";
			this.wLinkLabelCustomerService.ActiveLinkColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelCustomerService.AutoSize = true;
			this.wLinkLabelCustomerService.BackColor = Color.Transparent;
			this.wLinkLabelCustomerService.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLinkLabelCustomerService.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelCustomerService.LinkColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelCustomerService.Location = new Point(30, 271);
			this.wLinkLabelCustomerService.Name = "wLinkLabelCustomerService";
			this.wLinkLabelCustomerService.Size = new System.Drawing.Size(226, 16);
			this.wLinkLabelCustomerService.TabIndex = 5;
			this.wLinkLabelCustomerService.TabStop = true;
			this.wLinkLabelCustomerService.Text = "Visit our customer service page";
			this.wLinkLabelCustomerService.LinkClicked += new LinkLabelLinkClickedEventHandler(this.wLinkLabelCustomerService_LinkClicked);
			this.wPictureBoxVertical.BackColor = Color.Transparent;
			this.wPictureBoxVertical.Location = new Point(526, 107);
			this.wPictureBoxVertical.Name = "wPictureBoxVertical";
			this.wPictureBoxVertical.Size = new System.Drawing.Size(249, 118);
			this.wPictureBoxVertical.TabStop = false;
			this.wPictureBoxVertical.Visible = false;
			this.wPictureBoxVertical.Click += new EventHandler(this.wPictureBoxVertical_Click);
			this.wLinkLabelNewAccount.ActiveLinkColor = Color.FromArgb(181, 255, 33);
			this.wLinkLabelNewAccount.AutoSize = true;
			this.wLinkLabelNewAccount.BackColor = Color.Transparent;
			this.wLinkLabelNewAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLinkLabelNewAccount.ForeColor = Color.FromArgb(181, 255, 33);
			this.wLinkLabelNewAccount.LinkBehavior = LinkBehavior.AlwaysUnderline;
			this.wLinkLabelNewAccount.LinkColor = Color.FromArgb(181, 255, 33);
			this.wLinkLabelNewAccount.Location = new Point(527, 260);
			this.wLinkLabelNewAccount.Name = "wLinkLabelNewAccount";
			this.wLinkLabelNewAccount.Size = new System.Drawing.Size(248, 30);
			this.wLinkLabelNewAccount.TabIndex = 7;
			this.wLinkLabelNewAccount.TabStop = true;
			this.wLinkLabelNewAccount.Text = "DON'T HAVE AN ACCOUNT?\nCLICK HERE TO CREATE ONE NOW...";
			this.wLinkLabelNewAccount.LinkClicked += new LinkLabelLinkClickedEventHandler(this.wLinkLabelNewAccount_LinkClicked);
			this.wLoginButton.AutoSize = true;
			this.wLoginButton.BackColor = Color.Transparent;
			this.wLoginButton.LabelButtonEnabled = true;
			this.wLoginButton.Location = new Point(340, 250);
			this.wLoginButton.Name = "wLoginButton";
			this.wLoginButton.Size = new System.Drawing.Size(149, 37);
			this.wLoginButton.TabIndex = 6;
			this.wLoginButton.PreviewKeyDown += new PreviewKeyDownEventHandler(this.wLoginButton_PreviewKeyDown);
			this.wLabelLoginInfo.BackColor = Color.Transparent;
			this.wLabelLoginInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75f, FontStyle.Italic);
			this.wLabelLoginInfo.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLabelLoginInfo.Location = new Point(68, 121);
			this.wLabelLoginInfo.Name = "wLabelLoginInfo";
			this.wLabelLoginInfo.Size = new System.Drawing.Size(430, 21);
			this.wLabelShard.TabStop = false;
			this.wLabelLoginInfo.Text = "ENTER YOUR ACCOUNT INFORMATION TO LOG IN:";
			this.wLabelLoginInfo.TextAlign = ContentAlignment.BottomLeft;
			this.wShardComboBox.AutoSize = true;
			this.wShardComboBox.Location = new Point(548, 47);
			this.wShardComboBox.Name = "wShardComboBox";
			this.wShardComboBox.Size = new System.Drawing.Size(188, 26);
			this.wShardComboBox.TabIndex = 8;
			this.wOptionsButton.AutoSize = true;
			this.wOptionsButton.BackColor = Color.Transparent;
			this.wOptionsButton.Location = new Point(741, 44);
			this.wOptionsButton.MinimumSize = new System.Drawing.Size(31, 29);
			this.wOptionsButton.Name = "wOptionsButton";
			this.wOptionsButton.Size = new System.Drawing.Size(40, 29);
			this.wOptionsButton.TabIndex = 9;
			this.wOptionsButton.PreviewKeyDown += new PreviewKeyDownEventHandler(this.wOptionsButton_PreviewKeyDown);
			this.wLabelShard.BackColor = Color.Transparent;
			this.wLabelShard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold);
			this.wLabelShard.ForeColor = Color.White;
			this.wLabelShard.ImageAlign = ContentAlignment.MiddleRight;
			this.wLabelShard.Location = new Point(376, 51);
			this.wLabelShard.Name = "wLabelShard";
			this.wLabelShard.Size = new System.Drawing.Size(171, 15);
			this.wLabelShard.TabStop = false;
			this.wLabelShard.Text = "SELECT SHARD:";
			this.wLabelShard.TextAlign = ContentAlignment.MiddleRight;
			this.wTimerServerStatusCheck.Interval = 300000;
			this.wTimerServerStatusCheck.Tick += new EventHandler(this.wTimerServerStatusCheck_Tick);
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackColor = Color.Transparent;
			this.BackgroundImage = Resources.nfsw_lp_96dpi_bg_s1_w_1bit_alpha;
			base.Controls.Add(this.wLabelShard);
			base.Controls.Add(this.wOptionsButton);
			base.Controls.Add(this.wShardComboBox);
			base.Controls.Add(this.wLabelLoginInfo);
			base.Controls.Add(this.wLoginButton);
			base.Controls.Add(this.wLinkLabelNewAccount);
			base.Controls.Add(this.wPictureBoxVertical);
			base.Controls.Add(this.wLinkLabelCustomerService);
			base.Controls.Add(this.wLabelHavingTrouble);
			base.Controls.Add(this.wLabelAdvertisement);
			base.Controls.Add(this.wPictureBoxHorizontal);
			base.Controls.Add(this.wPictureBoxServerStatusOff);
			base.Controls.Add(this.wLabelServerStatus);
			base.Controls.Add(this.wPictureBoxServerStatusOn);
			base.Controls.Add(this.wLabelPassword);
			base.Controls.Add(this.wLabelEmail);
			base.Controls.Add(this.wLinkLabelForgetPassword);
			base.Controls.Add(this.wCheckBoxRememberMe);
			base.Controls.Add(this.wTextBoxPassword);
			base.Controls.Add(this.wTextBoxEmail);
			base.Name = "LoginScreen";
			base.Controls.SetChildIndex(this.wTextBoxEmail, 0);
			base.Controls.SetChildIndex(this.wTextBoxPassword, 0);
			base.Controls.SetChildIndex(this.wCheckBoxRememberMe, 0);
			base.Controls.SetChildIndex(this.wLinkLabelForgetPassword, 0);
			base.Controls.SetChildIndex(this.wLabelEmail, 0);
			base.Controls.SetChildIndex(this.wLabelPassword, 0);
			base.Controls.SetChildIndex(this.wPictureBoxServerStatusOn, 0);
			base.Controls.SetChildIndex(this.wLabelServerStatus, 0);
			base.Controls.SetChildIndex(this.wPictureBoxServerStatusOff, 0);
			base.Controls.SetChildIndex(this.wPictureBoxHorizontal, 0);
			base.Controls.SetChildIndex(this.wLabelAdvertisement, 0);
			base.Controls.SetChildIndex(this.wLabelHavingTrouble, 0);
			base.Controls.SetChildIndex(this.wLinkLabelCustomerService, 0);
			base.Controls.SetChildIndex(this.wPictureBoxVertical, 0);
			base.Controls.SetChildIndex(this.wLinkLabelNewAccount, 0);
			base.Controls.SetChildIndex(this.wLoginButton, 0);
			base.Controls.SetChildIndex(this.wLabelLoginInfo, 0);
			base.Controls.SetChildIndex(this.wShardComboBox, 0);
			base.Controls.SetChildIndex(this.wOptionsButton, 0);
			base.Controls.SetChildIndex(this.wLabelShard, 0);
			((ISupportInitialize)this.wPictureBoxServerStatusOff).EndInit();
			((ISupportInitialize)this.wPictureBoxServerStatusOn).EndInit();
			((ISupportInitialize)this.wPictureBoxHorizontal).EndInit();
			((ISupportInitialize)this.wPictureBoxVertical).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		protected override void InitializeSettings()
		{
			if (string.IsNullOrEmpty(this.parentForm.CommandArgEmail))
			{
				this.wCheckBoxRememberMe.Checked = Settings.Default.RememberEmail;
				if (Settings.Default.RememberEmail)
				{
					this.wTextBoxEmail.Text = Settings.Default.Email;
				}
			}
			else
			{
				this.wTextBoxEmail.Text = this.parentForm.CommandArgEmail;
				base.ActiveControl = this.wTextBoxPassword;
			}
			if (!string.IsNullOrEmpty(this.parentForm.CommandArgPassword))
			{
				this.wTextBoxPassword.Text = this.parentForm.CommandArgPassword;
			}
			if (!string.IsNullOrEmpty(this.parentForm.CommandArgShard))
			{
				this.wShardComboBox.SetSelectedValue(this.parentForm.CommandArgShard);
			}
			this.backgrounds = new Dictionary<double, Image>()
			{
				{ 96, Resources.nfsw_lp_96dpi_bg_s1_w_1bit_alpha },
				{ 120, Resources.nfsw_lp_120dpi_bg_s1_w_1bit_alpha },
				{ 144, Resources.nfsw_lp_144dpi_bg_s1_w_1bit_alpha }
			};
			this.BackgroundImage = base.SelectBackgroundImage();
			this.wLoginButton.wLabelButton.Click += new EventHandler(this.wLoginButton_Click);
			this.wOptionsButton.wLabelOptions.Click += new EventHandler(this.wOptionsButton_Click);
		}

		public override void LoadScreen()
		{
			base.LoadScreen();
			this.wShardComboBox.SetSelectedValue();
			this.wShardComboBox.ShardUrlChanged = new ShardUrlChanged(this.OnShardUrlChanged);
			this.parentForm.LoadServerDataFinished = new LoadServerDataFinished(this.OnLoadServerDataFinished);
			this.parentForm.LoadServerStatusFinished = new LoadServerStatusFinished(this.LoadServerStatusFinished);
			this.parentForm.LoadServerAdsFinished = new LoadServerAdsFinished(this.LoadServerAdsFinished);
			if (string.IsNullOrEmpty(this.wTextBoxEmail.Text))
			{
				base.ActiveControl = this.wTextBoxEmail;
				return;
			}
			base.ActiveControl = this.wTextBoxPassword;
		}

		private void LoadServerAdsFinished(string horizontalImage, string horizontalLink, string verticalImage, string verticalLink)
		{
			this.wPictureBoxHorizontal.Image = this.launcherImage.ResizeImage(LauncherImage.DownloadImage(horizontalImage));
			if (!this.wPictureBoxHorizontal.InvokeRequired)
			{
				this.wPictureBoxHorizontal.Visible = true;
			}
			else
			{
				this.wPictureBoxHorizontal.Invoke(new MethodInvoker(() => this.wPictureBoxHorizontal.Visible = true));
			}
			this.mPictureBoxHorizontalLink = horizontalLink;
			if (!this.wPictureBoxHorizontal.InvokeRequired)
			{
				this.wPictureBoxHorizontal.Cursor = Cursors.Hand;
			}
			else
			{
				this.wPictureBoxHorizontal.Invoke(new MethodInvoker(() => this.wPictureBoxHorizontal.Cursor = Cursors.Hand));
			}
			this.wPictureBoxVertical.Image = this.launcherImage.ResizeImage(LauncherImage.DownloadImage(verticalImage));
			if (!this.wPictureBoxVertical.InvokeRequired)
			{
				this.wPictureBoxVertical.Visible = true;
			}
			else
			{
				this.wPictureBoxVertical.Invoke(new MethodInvoker(() => this.wPictureBoxVertical.Visible = true));
			}
			this.mPictureBoxVerticalLink = verticalLink;
			if (!this.wPictureBoxVertical.InvokeRequired)
			{
				this.wPictureBoxVertical.Cursor = Cursors.Hand;
				return;
			}
			this.wPictureBoxVertical.Invoke(new MethodInvoker(() => this.wPictureBoxVertical.Cursor = Cursors.Hand));
		}

		private void LoadServerStatusFinished(bool serversUp, string serverStatusText)
		{
			this.parentForm.LoadServerAdsAsync();
			if (!serversUp)
			{
				this.wPictureBoxServerStatusOff.Image = this.launcherImage.ResizeImage(Resources.launcher_redlight);
				if (!this.wPictureBoxServerStatusOn.InvokeRequired)
				{
					this.wPictureBoxServerStatusOn.Visible = false;
					this.wPictureBoxServerStatusOff.Visible = true;
				}
				else
				{
					this.wPictureBoxServerStatusOn.Invoke(new MethodInvoker(() => {
						this.wPictureBoxServerStatusOn.Visible = false;
						this.wPictureBoxServerStatusOff.Visible = true;
					}));
				}
				this.wLabelServerStatus.ForeColor = Color.FromArgb(227, 88, 50);
			}
			else
			{
				this.wPictureBoxServerStatusOn.Image = this.launcherImage.ResizeImage(Resources.launcher_greenlight);
				if (!this.wPictureBoxServerStatusOn.InvokeRequired)
				{
					this.wPictureBoxServerStatusOn.Visible = true;
					this.wPictureBoxServerStatusOff.Visible = false;
				}
				else
				{
					this.wPictureBoxServerStatusOn.Invoke(new MethodInvoker(() => {
						this.wPictureBoxServerStatusOn.Visible = true;
						this.wPictureBoxServerStatusOff.Visible = false;
					}));
				}
				this.wLabelServerStatus.ForeColor = Color.FromArgb(181, 255, 33);
			}
			if (!this.wLabelServerStatus.InvokeRequired)
			{
				this.wLabelServerStatus.Text = serverStatusText;
			}
			else
			{
				this.wLabelServerStatus.Invoke(new MethodInvoker(() => this.wLabelServerStatus.Text = serverStatusText));
			}
			this.wLabelServerStatus.Cursor = Cursors.Default;
			this.wLogin_TextChanged(null, null);
		}

		protected override void LocalizeFE()
		{
			this.wCheckBoxRememberMe.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00015");
			this.wLabelEmail.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00016");
			this.wLoginButton.wLabelButton.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00017");
			this.wLabelPassword.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00018");
			this.wLinkLabelForgetPassword.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00028");
			this.wLabelLoginInfo.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00032");
			this.wLinkLabelNewAccount.Text = string.Format("{0}\n{1}", ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00034"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00035"));
			this.wLabelHavingTrouble.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00036");
			this.wLinkLabelCustomerService.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00037");
			this.wLabelAdvertisement.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00038");
			this.wLabelServerStatus.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00039");
			this.wLabelShard.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00072");
		}

		private void OnLoadServerDataFinished()
		{
		}

		private void OnShardUrlChanged()
		{
			if (base.IsActive)
			{
				this.wLoginButton.LabelButtonEnabled = true;
				this.wLabelServerStatus.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00039");
				this.wLabelServerStatus.ForeColor = Color.White;
				this.wLabelServerStatus.Update();
				this.wPictureBoxServerStatusOn.Visible = false;
				this.wPictureBoxServerStatusOff.Visible = false;
				this.parentForm.LoadServerData();
			}
		}

		private void PerformLogin()
		{
			if (this.wCheckBoxRememberMe.Checked)
			{
				Settings.Default.Email = this.wTextBoxEmail.Text;
				Settings.Default.RememberEmail = this.wCheckBoxRememberMe.Checked;
				Settings.Default.Save();
			}
			this.parentForm.PerformLogin(this.wTextBoxEmail.Text, this.wTextBoxPassword.Text);
		}

		public override void UnloadScreen()
		{
			base.UnloadScreen();
			this.wTimerServerStatusCheck.Enabled = false;
			this.wShardComboBox.ShardUrlChanged = null;
			this.parentForm.LoadServerDataFinished = null;
		}

		private void wLinkLabelCustomerService_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (string.IsNullOrEmpty(this.parentForm.WebApplicationData[0]))
			{
				return;
			}
			string empty = string.Empty;
			try
			{
				empty = this.parentForm.WebApplicationData[1].Replace("%1", string.Format(Settings.Default.UrlCustomerService, CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower()));
				Process.Start(this.parentForm.WebApplicationData[0], empty);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.Logger.ErrorFormat("Problem running {0} {1}", this.parentForm.WebApplicationData[0], empty);
				GameLauncherUI.Logger.Error(string.Concat("wLinkLabelCustomerService_LinkClicked Exception: ", exception.ToString()));
			}
		}

		private void wLinkLabelForgetPassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (string.IsNullOrEmpty(this.parentForm.WebApplicationData[0]))
			{
				return;
			}
			string empty = string.Empty;
			try
			{
				empty = this.parentForm.WebApplicationData[1].Replace("%1", string.Format("{0}?lang={1}", "https://www.origin.com/account/reset-password", CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower()));
				Process.Start(this.parentForm.WebApplicationData[0], empty);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.Logger.ErrorFormat("Problem running {0} {1}", this.parentForm.WebApplicationData[0], empty);
				GameLauncherUI.Logger.Error(string.Concat("wLinkLabelForgetPassword_LinkClicked Exception: ", exception.ToString()));
			}
		}

		private void wLinkLabelNewAccount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (string.IsNullOrEmpty(this.parentForm.WebApplicationData[0]))
			{
				return;
			}
			string empty = string.Empty;
			try
			{
				empty = this.parentForm.WebApplicationData[1].Replace("%1", string.Format("{0}?lang={1}", Settings.Default.UrlCreateNewAccount, CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower()));
				Process.Start(this.parentForm.WebApplicationData[0], empty);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.Logger.ErrorFormat("Problem running {0} {1}", this.parentForm.WebApplicationData[0], empty);
				GameLauncherUI.Logger.Error(string.Concat("wLinkLabelNewAccount_LinkClicked Exception: ", exception.ToString()));
			}
		}

		private void wLogin_TextChanged(object sender, EventArgs e)
		{
			this.wLoginButton.LabelButtonEnabled = true;
		}

		private void wLoginButton_Click(object sender, EventArgs e)
		{
			if (this.wLoginButton.LabelButtonEnabled)
			{
				this.Cursor = Cursors.WaitCursor;
				this.PerformLogin();
				this.Cursor = Cursors.Default;
			}
		}

		private void wLoginButton_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (e.KeyCode == Keys.Return && this.wLoginButton.LabelButtonEnabled)
			{
				this.wLoginButton.ButtonPressed(true);
				this.wLoginButton.Update();
				this.wLoginButton_Click(null, null);
				this.wLoginButton.ButtonPressed(false);
				this.wLoginButton.Update();
			}
		}

		private void wOptionsButton_Click(object sender, EventArgs e)
		{
			this.parentForm.SwitchScreen(ScreenType.Options);
		}

		private void wOptionsButton_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				this.wOptionsButton.ButtonPressed(true);
				this.wOptionsButton.Update();
				this.wOptionsButton_Click(null, null);
				this.wOptionsButton.ButtonPressed(false);
				this.wOptionsButton.Update();
			}
		}

		private void wPictureBoxHorizontal_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(this.mPictureBoxHorizontalLink))
			{
				if (string.IsNullOrEmpty(this.parentForm.WebApplicationData[0]))
				{
					return;
				}
				string empty = string.Empty;
				try
				{
					empty = this.parentForm.WebApplicationData[1].Replace("%1", this.mPictureBoxHorizontalLink);
					Process.Start(this.parentForm.WebApplicationData[0], empty);
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					GameLauncherUI.Logger.ErrorFormat("Problem running {0} {1}", this.parentForm.WebApplicationData[0], empty);
					GameLauncherUI.Logger.Error(string.Concat("wPictureBoxHorizontal_Click Exception: ", exception.ToString()));
				}
			}
		}

		private void wPictureBoxVertical_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(this.mPictureBoxVerticalLink))
			{
				if (string.IsNullOrEmpty(this.parentForm.WebApplicationData[0]))
				{
					return;
				}
				string empty = string.Empty;
				try
				{
					empty = this.parentForm.WebApplicationData[1].Replace("%1", this.mPictureBoxVerticalLink);
					Process.Start(this.parentForm.WebApplicationData[0], empty);
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					GameLauncherUI.Logger.ErrorFormat("Problem running {0} {1}", this.parentForm.WebApplicationData[0], empty);
					GameLauncherUI.Logger.Error(string.Concat("wPictureBoxVertical_Click Exception: ", exception.ToString()));
				}
			}
		}

		private void wTimerServerStatusCheck_Tick(object sender, EventArgs e)
		{
		}
	}
}