using System;

namespace GameLauncher.ProdUI
{
	public class DocumentCompleteEventArgs : EventArgs
	{
		private object ppDisp;

		private object url;

		public object PPDisp
		{
			get
			{
				return this.ppDisp;
			}
			set
			{
				this.ppDisp = value;
			}
		}

		public object Url
		{
			get
			{
				return this.url;
			}
			set
			{
				this.url = value;
			}
		}

		public DocumentCompleteEventArgs(object ppDisp, object url)
		{
			this.ppDisp = ppDisp;
			this.url = url;
		}
	}
}