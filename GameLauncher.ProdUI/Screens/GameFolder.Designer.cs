using GameLauncher;
using GameLauncher.ProdUI;
using GameLauncher.ProdUI.Controls;
using GameLauncher.ProdUI.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace GameLauncher.ProdUI.Screens
{
	public class GameFolder : BaseScreen
	{
		private Form parentForm;

		private string selectedFolder;

		private IContainer components;

		private MainButton wMainButtonContinue;

		private Label wLabelDescription;

		private TextBox wTextFolder;

		private FolderButton wFolderButton;

		private Label wLabelFolderHeader;

		public string SelectedFolder
		{
			get
			{
				return this.selectedFolder;
			}
		}

		public GameFolder(Form launcherForm) : base(launcherForm)
		{
			this.parentForm = launcherForm;
			this.InitializeComponent();
			this.InitializeSettings();
			this.ApplyEmbeddedFonts();
			this.LocalizeFE();
		}

		protected override void ApplyEmbeddedFonts()
		{
			FontFamily fontFamily = FontWrapper.Instance.GetFontFamily("MyriadProSemiCondBold.ttf");
			FontFamily fontFamily1 = FontWrapper.Instance.GetFontFamily("Reg-DB-I.ttf");
			FontFamily fontFamily2 = FontWrapper.Instance.GetFontFamily("Reg-B-I.ttf");
			this.wLabelFolderHeader.Font = new System.Drawing.Font(fontFamily1, 12.75f, FontStyle.Italic);
			this.wLabelDescription.Font = new System.Drawing.Font(fontFamily, 9.749999f, FontStyle.Bold);
			this.wMainButtonContinue.wLabelButton.Font = new System.Drawing.Font(fontFamily2, 15f, FontStyle.Bold | FontStyle.Italic);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.wMainButtonContinue = new MainButton();
			this.wLabelDescription = new Label();
			this.wTextFolder = new TextBox();
			this.wFolderButton = new FolderButton();
			this.wLabelFolderHeader = new Label();
			base.SuspendLayout();
			this.wMainButtonContinue.AutoSize = true;
			this.wMainButtonContinue.BackColor = Color.Transparent;
			this.wMainButtonContinue.LabelButtonEnabled = false;
			this.wMainButtonContinue.Location = new Point(613, 420);
			this.wMainButtonContinue.Name = "wMainButtonContinue";
			this.wMainButtonContinue.Size = new System.Drawing.Size(149, 37);
			this.wMainButtonContinue.TabIndex = 19;
			this.wLabelDescription.BackColor = Color.Transparent;
			this.wLabelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold);
			this.wLabelDescription.ForeColor = Color.White;
			this.wLabelDescription.Location = new Point(62, 213);
			this.wLabelDescription.Name = "wLabelDescription";
			this.wLabelDescription.Size = new System.Drawing.Size(663, 37);
			this.wLabelDescription.TabIndex = 25;
			this.wLabelDescription.Text = "Select a folder where the game will be downloaded, this folder needs to have write access for anyone playing the game.";
			this.wTextFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.wTextFolder.Location = new Point(65, 253);
			this.wTextFolder.Name = "wTextFolder";
			this.wTextFolder.ReadOnly = true;
			this.wTextFolder.Size = new System.Drawing.Size(600, 20);
			this.wTextFolder.TabIndex = 26;
			this.wFolderButton.AutoSize = true;
			this.wFolderButton.BackColor = Color.Transparent;
			this.wFolderButton.Location = new Point(671, 249);
			this.wFolderButton.MinimumSize = new System.Drawing.Size(31, 29);
			this.wFolderButton.Name = "wFolderButton";
			this.wFolderButton.Size = new System.Drawing.Size(35, 29);
			this.wFolderButton.TabIndex = 27;
			this.wLabelFolderHeader.BackColor = Color.Transparent;
			this.wLabelFolderHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold);
			this.wLabelFolderHeader.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLabelFolderHeader.Location = new Point(74, 122);
			this.wLabelFolderHeader.Name = "wLabelFolderHeader";
			this.wLabelFolderHeader.Size = new System.Drawing.Size(314, 25);
			this.wLabelFolderHeader.TabIndex = 28;
			this.wLabelFolderHeader.Text = "PLEASE SELECT YOUR GAME FOLDER";
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackgroundImage = Resources.nfsw_lp_96dpi_bg_s0_w_1bit_alpha;
			base.Controls.Add(this.wLabelFolderHeader);
			base.Controls.Add(this.wFolderButton);
			base.Controls.Add(this.wTextFolder);
			base.Controls.Add(this.wLabelDescription);
			base.Controls.Add(this.wMainButtonContinue);
			base.Name = "GameFolder";
			base.Controls.SetChildIndex(this.wMainButtonContinue, 0);
			base.Controls.SetChildIndex(this.wLabelDescription, 0);
			base.Controls.SetChildIndex(this.wTextFolder, 0);
			base.Controls.SetChildIndex(this.wFolderButton, 0);
			base.Controls.SetChildIndex(this.wLabelFolderHeader, 0);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		protected override void InitializeSettings()
		{
			this.backgrounds = new Dictionary<double, Image>()
			{
				{ 96, Resources.nfsw_lp_96dpi_bg_s0_w_1bit_alpha },
				{ 120, Resources.nfsw_lp_120dpi_bg_s0_w_1bit_alpha },
				{ 144, Resources.nfsw_lp_144dpi_bg_s0_w_1bit_alpha }
			};
			this.BackgroundImage = base.SelectBackgroundImage();
			this.wFolderButton.wLabelFolder.Click += new EventHandler(this.wFolderButton_Click);
			this.wMainButtonContinue.LabelButtonEnabled = false;
			this.wMainButtonContinue.wLabelButton.Click += new EventHandler(this.wContinueButton_Click);
			string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
			this.SetSelectedFolder(Path.Combine(folderPath, "Electronic Arts\\Need for Speed World"));
		}

		protected override void LocalizeFE()
		{
			this.wMainButtonContinue.wLabelButton.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00078");
			this.wLabelFolderHeader.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00079");
			this.wLabelDescription.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00080");
		}

		public void SetSelectedFolder(string folder)
		{
			this.selectedFolder = folder;
			this.wTextFolder.Text = this.selectedFolder;
			this.wMainButtonContinue.LabelButtonEnabled = !string.IsNullOrEmpty(this.selectedFolder);
		}

		private void wContinueButton_Click(object sender, EventArgs e)
		{
			this.parentForm.Close();
		}

		private void wFolderButton_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog()
			{
				SelectedPath = this.SelectedFolder
			};
			DialogResult dialogResult = folderBrowserDialog.ShowDialog();
			string empty = string.Empty;
			if (dialogResult == DialogResult.OK)
			{
				this.SetSelectedFolder(folderBrowserDialog.SelectedPath);
			}
		}
	}
}