using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class BaseScreen : UserControl
	{
		protected float mDpi;

		protected LauncherImage launcherImage;

		private Form launcherForm;

		private bool isActive;

		protected Dictionary<double, Image> backgrounds;

		private bool mLabelButtonMinimizeHover;

		private bool mLabelButtonCloseHover;

		private bool mDragging;

		private Point mDraggingStart;

		private Point mDraggingCursorStart;

		private IContainer components;

		private Label wLabelButtonMinimize;

		private Label wLabelButtonClose;

		private ImageList wImageListButtonMinimize96;

		private ImageList wImageListButtonMinimize120;

		private ImageList wImageListButtonMinimize144;

		private ImageList wImageListButtonClose96;

		private ImageList wImageListButtonClose120;

		private ImageList wImageListButtonClose144;

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
		}

		public BaseScreen()
		{
			this.InitializeComponent();
		}

		public BaseScreen(Form launcherForm)
		{
			this.InitializeComponent();
			this.launcherForm = launcherForm;
			using (Graphics graphic = base.CreateGraphics())
			{
				this.mDpi = graphic.DpiX;
			}
			this.launcherImage = new LauncherImage(this.mDpi);
			this.SelectImageList();
		}

		protected virtual void ApplyEmbeddedFonts()
		{
		}

		private void BaseScreen_MouseDown(object sender, MouseEventArgs e)
		{
			this.mDragging = true;
			this.mDraggingStart = this.launcherForm.Location;
			this.mDraggingCursorStart = System.Windows.Forms.Cursor.Position;
		}

		private void BaseScreen_MouseMove(object sender, MouseEventArgs e)
		{
			if (this.mDragging)
			{
				Form point = this.launcherForm;
				int x = this.mDraggingStart.X;
				Point position = System.Windows.Forms.Cursor.Position;
				int num = x + position.X - this.mDraggingCursorStart.X;
				int y = this.mDraggingStart.Y;
				Point position1 = System.Windows.Forms.Cursor.Position;
				point.Location = new Point(num, y + position1.Y - this.mDraggingCursorStart.Y);
			}
		}

		private void BaseScreen_MouseUp(object sender, MouseEventArgs e)
		{
			this.mDragging = false;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(BaseScreen));
			this.wLabelButtonMinimize = new Label();
			this.wImageListButtonMinimize96 = new ImageList(this.components);
			this.wLabelButtonClose = new Label();
			this.wImageListButtonClose96 = new ImageList(this.components);
			this.wImageListButtonMinimize120 = new ImageList(this.components);
			this.wImageListButtonMinimize144 = new ImageList(this.components);
			this.wImageListButtonClose120 = new ImageList(this.components);
			this.wImageListButtonClose144 = new ImageList(this.components);
			base.SuspendLayout();
			this.wLabelButtonMinimize.BackColor = Color.Transparent;
			this.wLabelButtonMinimize.ForeColor = Color.Transparent;
			this.wLabelButtonMinimize.ImageIndex = 0;
			this.wLabelButtonMinimize.ImageList = this.wImageListButtonMinimize96;
			this.wLabelButtonMinimize.Location = new Point(708, 1);
			this.wLabelButtonMinimize.Name = "wLabelButtonMinimize";
			this.wLabelButtonMinimize.Size = new System.Drawing.Size(34, 26);
			this.wLabelButtonMinimize.TabIndex = 17;
			this.wLabelButtonMinimize.MouseLeave += new EventHandler(this.wLabelButtonMinimize_MouseLeave);
			this.wLabelButtonMinimize.Click += new EventHandler(this.wLabelButtonMinimize_Click);
			this.wLabelButtonMinimize.MouseDown += new MouseEventHandler(this.wLabelButtonMinimize_MouseDown);
			this.wLabelButtonMinimize.MouseUp += new MouseEventHandler(this.wLabelButtonMinimize_MouseUp);
			this.wLabelButtonMinimize.MouseEnter += new EventHandler(this.wLabelButtonMinimize_MouseEnter);
			this.wImageListButtonMinimize96.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButtonMinimize96.ImageStream");
			this.wImageListButtonMinimize96.TransparentColor = Color.Red;
			this.wImageListButtonMinimize96.Images.SetKeyName(0, "nfsw_lp_96dpi_bt_minimize_enabled.bmp");
			this.wImageListButtonMinimize96.Images.SetKeyName(1, "nfsw_lp_96dpi_bt_minimize_rollover.bmp");
			this.wImageListButtonMinimize96.Images.SetKeyName(2, "nfsw_lp_96dpi_bt_minimize_down.bmp");
			this.wLabelButtonClose.BackColor = Color.Transparent;
			this.wLabelButtonClose.ForeColor = Color.Transparent;
			this.wLabelButtonClose.ImageIndex = 0;
			this.wLabelButtonClose.ImageList = this.wImageListButtonClose96;
			this.wLabelButtonClose.Location = new Point(742, 1);
			this.wLabelButtonClose.Name = "wLabelButtonClose";
			this.wLabelButtonClose.Size = new System.Drawing.Size(33, 26);
			this.wLabelButtonClose.TabIndex = 18;
			this.wLabelButtonClose.MouseLeave += new EventHandler(this.wLabelButtonClose_MouseLeave);
			this.wLabelButtonClose.Click += new EventHandler(this.wLabelButtonClose_Click);
			this.wLabelButtonClose.MouseDown += new MouseEventHandler(this.wLabelButtonClose_MouseDown);
			this.wLabelButtonClose.MouseUp += new MouseEventHandler(this.wLabelButtonClose_MouseUp);
			this.wLabelButtonClose.MouseEnter += new EventHandler(this.wLabelButtonClose_MouseEnter);
			this.wImageListButtonClose96.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButtonClose96.ImageStream");
			this.wImageListButtonClose96.TransparentColor = Color.Red;
			this.wImageListButtonClose96.Images.SetKeyName(0, "nfsw_lp_96dpi_bt_close_enabled.bmp");
			this.wImageListButtonClose96.Images.SetKeyName(1, "nfsw_lp_96dpi_bt_close_rollover.bmp");
			this.wImageListButtonClose96.Images.SetKeyName(2, "nfsw_lp_96dpi_bt_close_down.bmp");
			this.wImageListButtonMinimize120.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButtonMinimize120.ImageStream");
			this.wImageListButtonMinimize120.TransparentColor = Color.Red;
			this.wImageListButtonMinimize120.Images.SetKeyName(0, "nfsw_lp_120dpi_bt_minimize_enabled.bmp");
			this.wImageListButtonMinimize120.Images.SetKeyName(1, "nfsw_lp_120dpi_bt_minimize_rollover.bmp");
			this.wImageListButtonMinimize120.Images.SetKeyName(2, "nfsw_lp_120dpi_bt_minimize_down.bmp");
			this.wImageListButtonMinimize144.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButtonMinimize144.ImageStream");
			this.wImageListButtonMinimize144.TransparentColor = Color.Red;
			this.wImageListButtonMinimize144.Images.SetKeyName(0, "nfsw_lp_144dpi_bt_minimize_enabled.bmp");
			this.wImageListButtonMinimize144.Images.SetKeyName(1, "nfsw_lp_144dpi_bt_minimize_rollover.bmp");
			this.wImageListButtonMinimize144.Images.SetKeyName(2, "nfsw_lp_144dpi_bt_minimize_down.bmp");
			this.wImageListButtonClose120.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButtonClose120.ImageStream");
			this.wImageListButtonClose120.TransparentColor = Color.Red;
			this.wImageListButtonClose120.Images.SetKeyName(0, "nfsw_lp_120dpi_bt_close_enabled.bmp");
			this.wImageListButtonClose120.Images.SetKeyName(1, "nfsw_lp_120dpi_bt_close_rollover.bmp");
			this.wImageListButtonClose120.Images.SetKeyName(2, "nfsw_lp_120dpi_bt_close_down.bmp");
			this.wImageListButtonClose144.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButtonClose144.ImageStream");
			this.wImageListButtonClose144.TransparentColor = Color.Red;
			this.wImageListButtonClose144.Images.SetKeyName(0, "nfsw_lp_144dpi_bt_close_enabled.bmp");
			this.wImageListButtonClose144.Images.SetKeyName(1, "nfsw_lp_144dpi_bt_close_rollover.bmp");
			this.wImageListButtonClose144.Images.SetKeyName(2, "nfsw_lp_144dpi_bt_close_down.bmp");
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackColor = Color.Transparent;
			base.Controls.Add(this.wLabelButtonClose);
			base.Controls.Add(this.wLabelButtonMinimize);
			this.MinimumSize = new System.Drawing.Size(790, 490);
			base.Name = "BaseScreen";
			base.Size = new System.Drawing.Size(790, 490);
			base.MouseMove += new MouseEventHandler(this.BaseScreen_MouseMove);
			base.MouseDown += new MouseEventHandler(this.BaseScreen_MouseDown);
			base.MouseUp += new MouseEventHandler(this.BaseScreen_MouseUp);
			base.ResumeLayout(false);
		}

		protected virtual void InitializeSettings()
		{
		}

		public virtual void LoadScreen()
		{
			this.isActive = true;
		}

		protected virtual void LocalizeFE()
		{
		}

		protected Image SelectBackgroundImage()
		{
			Image item = null;
			if ((double)this.mDpi <= 96)
			{
				item = this.backgrounds[96];
			}
			else if ((double)this.mDpi > 120)
			{
				item = ((double)this.mDpi > 144 ? this.launcherImage.ResizeImage(this.backgrounds[144], 1440f) : this.backgrounds[144]);
			}
			else
			{
				item = this.backgrounds[120];
			}
			Bitmap bitmap = new Bitmap(item);
			Color pixel = bitmap.GetPixel(1, 1);
			bitmap.MakeTransparent(pixel);
			this.launcherForm.BackColor = pixel;
			this.launcherForm.TransparencyKey = pixel;
			return bitmap;
		}

		protected void SelectImageList()
		{
			if ((double)this.mDpi <= 96)
			{
				this.wLabelButtonMinimize.ImageList = this.wImageListButtonMinimize96;
				this.wLabelButtonClose.ImageList = this.wImageListButtonClose96;
				return;
			}
			if ((double)this.mDpi <= 120)
			{
				this.wLabelButtonMinimize.ImageList = this.wImageListButtonMinimize120;
				this.wLabelButtonClose.ImageList = this.wImageListButtonClose120;
				return;
			}
			if ((double)this.mDpi <= 144)
			{
				this.wLabelButtonMinimize.ImageList = this.wImageListButtonMinimize144;
				this.wLabelButtonClose.ImageList = this.wImageListButtonClose144;
				return;
			}
			this.wLabelButtonMinimize.ImageList = this.launcherImage.ResizeImageList(this.wImageListButtonMinimize144, 144f);
			this.wLabelButtonClose.ImageList = this.launcherImage.ResizeImageList(this.wImageListButtonClose144, 144f);
		}

		public virtual void UnloadScreen()
		{
			this.isActive = false;
		}

		private void wLabelButtonClose_Click(object sender, EventArgs e)
		{
			this.launcherForm.Close();
		}

		private void wLabelButtonClose_MouseDown(object sender, MouseEventArgs e)
		{
			this.wLabelButtonClose.ImageIndex = 2;
		}

		private void wLabelButtonClose_MouseEnter(object sender, EventArgs e)
		{
			if (!this.mLabelButtonCloseHover)
			{
				this.wLabelButtonClose.ImageIndex = 1;
				this.mLabelButtonCloseHover = true;
			}
		}

		private void wLabelButtonClose_MouseLeave(object sender, EventArgs e)
		{
			if (this.mLabelButtonCloseHover)
			{
				this.wLabelButtonClose.ImageIndex = 0;
				this.mLabelButtonCloseHover = false;
			}
		}

		private void wLabelButtonClose_MouseUp(object sender, MouseEventArgs e)
		{
			this.wLabelButtonClose.ImageIndex = 0;
		}

		private void wLabelButtonMinimize_Click(object sender, EventArgs e)
		{
			this.launcherForm.WindowState = FormWindowState.Minimized;
		}

		private void wLabelButtonMinimize_MouseDown(object sender, MouseEventArgs e)
		{
			this.wLabelButtonMinimize.ImageIndex = 2;
		}

		private void wLabelButtonMinimize_MouseEnter(object sender, EventArgs e)
		{
			if (!this.mLabelButtonMinimizeHover)
			{
				this.wLabelButtonMinimize.ImageIndex = 1;
				this.mLabelButtonMinimizeHover = true;
			}
		}

		private void wLabelButtonMinimize_MouseLeave(object sender, EventArgs e)
		{
			if (this.mLabelButtonMinimizeHover)
			{
				this.wLabelButtonMinimize.ImageIndex = 0;
				this.mLabelButtonMinimizeHover = false;
			}
		}

		private void wLabelButtonMinimize_MouseUp(object sender, MouseEventArgs e)
		{
			this.wLabelButtonMinimize.ImageIndex = 0;
		}
	}
}