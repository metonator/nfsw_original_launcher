using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace GameLauncher.ProdUI
{
	internal class FontWrapper
	{
		private PrivateFontCollection mPrivateFontCollection;

		private List<string> mFonts;

		private static FontWrapper _instance;

		public static FontWrapper Instance
		{
			get
			{
				return FontWrapper._instance;
			}
		}

		static FontWrapper()
		{
			FontWrapper._instance = new FontWrapper();
		}

		private FontWrapper()
		{
			this.mPrivateFontCollection = new PrivateFontCollection();
			this.mFonts = new List<string>();
		}

		public FontFamily GetFontFamily(string fontName)
		{
			if (!this.mFonts.Contains(fontName))
			{
				this.LoadEmbeddedFont(fontName);
			}
			int num = this.mFonts.IndexOf(fontName);
			return this.mPrivateFontCollection.Families[num];
		}

		private void LoadEmbeddedFont(string fontName)
		{
			Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("GameLauncher.ProdUI.Fonts.", fontName));
			IntPtr intPtr = Marshal.AllocCoTaskMem((int)manifestResourceStream.Length);
			byte[] numArray = new byte[checked((int)manifestResourceStream.Length)];
			manifestResourceStream.Read(numArray, 0, (int)manifestResourceStream.Length);
			Marshal.Copy(numArray, 0, intPtr, (int)manifestResourceStream.Length);
			uint num = 0;
			FontWrapper.UnsafeNativeMethods.AddFontMemResourceEx(intPtr, (uint)numArray.Length, IntPtr.Zero, ref num);
			this.mPrivateFontCollection.AddMemoryFont(intPtr, (int)manifestResourceStream.Length);
			manifestResourceStream.Close();
			Marshal.FreeCoTaskMem(intPtr);
			this.mFonts.Add(fontName);
			this.mFonts.Sort();
		}

		internal static class UnsafeNativeMethods
		{
			[DllImport("gdi32.dll", CharSet=CharSet.None, ExactSpelling=false)]
			public static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);
		}
	}
}