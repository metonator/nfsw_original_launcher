using System;

namespace GameLauncher.ProdUI
{
	public enum ScreenType
	{
		None,
		Options,
		Login,
		Download
	}
}