using GameLauncher.ProdUI.Properties;
using log4net;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Xml;

namespace GameLauncher.ProdUI
{
	public class UserSettings
	{
		public UserSettings()
		{
		}

		public static XmlDocument GetSettingsFileName()
		{
			XmlDocument xmlDocument;
			XmlNode xmlNodes;
			DirectoryInfo directoryInfo;
			XmlDocument xmlDocument1 = new XmlDocument();
			string settingsPath = UserSettings.GetSettingsPath();
			if (File.Exists(settingsPath))
			{
				try
				{
					xmlDocument1.Load(settingsPath);
					xmlDocument = xmlDocument1;
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					GameLauncherUI.Logger.Warn(string.Concat("GetSettingsFile Exception: ", exception.ToString()));
					GameLauncherUI.Logger.Warn(string.Concat("Deleting corrupted Settings file: ", settingsPath));
					File.SetAttributes(settingsPath, FileAttributes.Normal);
					File.Delete(settingsPath);
					xmlNodes = xmlDocument1.AppendChild(xmlDocument1.CreateElement("Settings"));
					directoryInfo = Directory.CreateDirectory(Path.GetDirectoryName(settingsPath));
					xmlDocument1.Save(settingsPath);
					return xmlDocument1;
				}
				return xmlDocument;
			}
			xmlNodes = xmlDocument1.AppendChild(xmlDocument1.CreateElement("Settings"));
			directoryInfo = Directory.CreateDirectory(Path.GetDirectoryName(settingsPath));
			xmlDocument1.Save(settingsPath);
			return xmlDocument1;
		}

		public static string GetSettingsPath()
		{
			string settingFile = Settings.Default.SettingFile;
			char[] chrArray = new char[] { '/', '\\' };
			string[] environmentVariable = settingFile.Split(chrArray);
			for (int i = 0; i < (int)environmentVariable.Length; i++)
			{
				if (environmentVariable[i].StartsWith("$(") && environmentVariable[i].EndsWith(")"))
				{
					environmentVariable[i] = Environment.GetEnvironmentVariable(environmentVariable[i].Trim(new char[] { '$', '(', ')', ' ' }));
				}
			}
			string str = environmentVariable[0];
			for (int j = 1; j < (int)environmentVariable.Length; j++)
			{
				str = string.Concat(str, "\\", environmentVariable[j]);
			}
			return str;
		}

		public static void InsertValue(XmlDocument settingsFile, XmlNode locationNode, string key, string value, string typeName)
		{
			XmlNode item = locationNode[key];
			if (item == null)
			{
				item = settingsFile.CreateElement(key);
				locationNode.AppendChild(item);
				XmlAttribute xmlAttribute = settingsFile.CreateAttribute("Type");
				xmlAttribute.Value = typeName;
				item.Attributes.Append(xmlAttribute);
			}
			item.InnerText = value;
		}

		public static bool UpdateUserSettingsXml()
		{
			bool flag;
			try
			{
				XmlDocument settingsFileName = UserSettings.GetSettingsFileName();
				string languagePath = Settings.Default.LanguagePath;
				char[] chrArray = new char[] { '/', '\\' };
				string[] strArrays = languagePath.Split(chrArray);
				XmlNode item = settingsFileName["Settings"];
				string[] strArrays1 = strArrays;
				for (int i = 0; i < (int)strArrays1.Length; i++)
				{
					string str = strArrays1[i];
					XmlNode xmlNodes = item[str];
					if (xmlNodes == null)
					{
						xmlNodes = settingsFileName.CreateElement(str);
						item.AppendChild(xmlNodes);
					}
					item = xmlNodes;
				}
				string str1 = Settings.Default.LanguageValues[Settings.Default.Language].Trim();
				char[] chrArray1 = new char[] { ',' };
				string str2 = str1.Split(chrArray1)[0];
				UserSettings.InsertValue(settingsFileName, item, Settings.Default.LanguageName, str2.ToUpper(), "string");
				string tracksName = Settings.Default.TracksName;
				int tracks = Settings.Default.Tracks;
				UserSettings.InsertValue(settingsFileName, item, tracksName, tracks.ToString(), "int");
				bool flag1 = false;
				int num = 10;
				string settingsPath = UserSettings.GetSettingsPath();
				XmlDocument xmlDocument = new XmlDocument();
				while (!flag1)
				{
					int num1 = num - 1;
					num = num1;
					if (num1 < 0)
					{
						break;
					}
					try
					{
						settingsFileName.Save(settingsPath);
						xmlDocument.Load(settingsPath);
						flag1 = true;
					}
					catch (XmlException xmlException)
					{
						try
						{
							if (File.Exists(settingsPath))
							{
								File.SetAttributes(settingsPath, FileAttributes.Normal);
								File.Delete(settingsPath);
							}
						}
						catch (Exception exception1)
						{
							Exception exception = exception1;
							GameLauncherUI.Logger.Error(string.Concat("Exception when trying to delete corrupted config file - retries: ", num));
							GameLauncherUI.Logger.Error(string.Concat("Main Exception: ", exception.ToString()));
						}
					}
					catch (Exception exception2)
					{
						GameLauncherUI.Logger.Error(string.Concat("UpdateUserSettingsXml Exception ", exception2));
					}
				}
				flag = true;
			}
			catch (Exception exception4)
			{
				Exception exception3 = exception4;
				GameLauncherUI.Logger.Error(string.Concat("UpdateUserSettingsXml Exception: ", exception3.ToString()));
				flag = false;
			}
			return flag;
		}
	}
}