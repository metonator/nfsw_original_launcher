using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class MainButton : UserControl
	{
		private IContainer components;

		private ImageList wImageList96;

		private ImageList wImageList120;

		private ImageList wImageList144;

		public Label wLabelButton;

		private bool mLabelButtonEnabled;

		private bool mLabelButtonHover;

		public bool LabelButtonEnabled
		{
			get
			{
				return this.mLabelButtonEnabled;
			}
			set
			{
				this.mLabelButtonEnabled = value;
				this.UpdateButton();
			}
		}

		public MainButton()
		{
			this.InitializeComponent();
			this.SetImageLists();
			this.UpdateButton();
		}

		public void ButtonPressed(bool isPressed)
		{
			if (!this.mLabelButtonEnabled)
			{
				return;
			}
			if (isPressed)
			{
				this.wLabelButton.ImageIndex = 3;
				return;
			}
			this.wLabelButton.ImageIndex = 2;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(MainButton));
			this.wImageList96 = new ImageList(this.components);
			this.wImageList120 = new ImageList(this.components);
			this.wImageList144 = new ImageList(this.components);
			this.wLabelButton = new Label();
			base.SuspendLayout();
			this.wImageList96.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList96.ImageStream");
			this.wImageList96.TransparentColor = Color.Red;
			this.wImageList96.Images.SetKeyName(0, "nfsw_lp_96dpi_bg_s1_bt_login_disabled.bmp");
			this.wImageList96.Images.SetKeyName(1, "nfsw_lp_96dpi_bg_s1_bt_login_enabled.bmp");
			this.wImageList96.Images.SetKeyName(2, "nfsw_lp_96dpi_bg_s1_bt_login_rollover.bmp");
			this.wImageList96.Images.SetKeyName(3, "nfsw_lp_96dpi_bg_s1_bt_login_down.bmp");
			this.wImageList120.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList120.ImageStream");
			this.wImageList120.TransparentColor = Color.Red;
			this.wImageList120.Images.SetKeyName(0, "nfsw_lp_120dpi_bg_s1_bt_login_disabled.bmp");
			this.wImageList120.Images.SetKeyName(1, "nfsw_lp_120dpi_bg_s1_bt_login_enabled.bmp");
			this.wImageList120.Images.SetKeyName(2, "nfsw_lp_120dpi_bg_s1_bt_login_rollover.bmp");
			this.wImageList120.Images.SetKeyName(3, "nfsw_lp_120dpi_bg_s1_bt_login_down.bmp");
			this.wImageList144.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList144.ImageStream");
			this.wImageList144.TransparentColor = Color.Red;
			this.wImageList144.Images.SetKeyName(0, "nfsw_lp_144dpi_bg_s1_bt_login_disabled.bmp");
			this.wImageList144.Images.SetKeyName(1, "nfsw_lp_144dpi_bg_s1_bt_login_enabled.bmp");
			this.wImageList144.Images.SetKeyName(2, "nfsw_lp_144dpi_bg_s1_bt_login_rollover.bmp");
			this.wImageList144.Images.SetKeyName(3, "nfsw_lp_144dpi_bg_s1_bt_login_down.bmp");
			this.wLabelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLabelButton.ForeColor = Color.White;
			this.wLabelButton.ImageIndex = 0;
			this.wLabelButton.ImageList = this.wImageList96;
			this.wLabelButton.Location = new Point(0, 0);
			this.wLabelButton.Name = "wLabelButton";
			this.wLabelButton.Size = new System.Drawing.Size(146, 37);
			this.wLabelButton.TabIndex = 0;
			this.wLabelButton.TextAlign = ContentAlignment.MiddleCenter;
			this.wLabelButton.MouseLeave += new EventHandler(this.wLabelButton_MouseLeave);
			this.wLabelButton.MouseDown += new MouseEventHandler(this.wLabelButton_MouseDown);
			this.wLabelButton.MouseUp += new MouseEventHandler(this.wLabelButton_MouseUp);
			this.wLabelButton.MouseEnter += new EventHandler(this.wLabelButton_MouseEnter);
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoSize = true;
			this.BackColor = Color.Transparent;
			base.Controls.Add(this.wLabelButton);
			base.Name = "MainButton";
			base.Size = new System.Drawing.Size(149, 37);
			base.GotFocus += new EventHandler(this.MainButton_GotFocus);
			base.LostFocus += new EventHandler(this.MainButton_LostFocus);
			base.ResumeLayout(false);
		}

		private void MainButton_GotFocus(object sender, EventArgs e)
		{
			this.wLabelButton_MouseEnter(null, null);
		}

		private void MainButton_LostFocus(object sender, EventArgs e)
		{
			this.wLabelButton_MouseLeave(null, null);
		}

		private void SetImageLists()
		{
			float dpiX;
			using (Graphics graphic = base.CreateGraphics())
			{
				dpiX = graphic.DpiX;
			}
			if ((double)dpiX <= 96)
			{
				this.wLabelButton.ImageList = this.wImageList96;
				return;
			}
			if ((double)dpiX <= 120)
			{
				this.wLabelButton.ImageList = this.wImageList120;
				return;
			}
			if ((double)dpiX <= 144)
			{
				this.wLabelButton.ImageList = this.wImageList144;
				return;
			}
			LauncherImage launcherImage = new LauncherImage(dpiX);
			this.wLabelButton.ImageList = launcherImage.ResizeImageList(this.wImageList144, 144f);
		}

		public void UpdateButton()
		{
			if (!this.mLabelButtonEnabled)
			{
				this.wLabelButton.ImageIndex = 0;
				this.wLabelButton.ForeColor = Color.FromArgb(153, 153, 153);
			}
			else
			{
				this.wLabelButton.ImageIndex = 1;
				this.wLabelButton.ForeColor = Color.FromArgb(255, 255, 255);
			}
			this.wLabelButton.Update();
		}

		private void wLabelButton_MouseDown(object sender, MouseEventArgs e)
		{
			this.ButtonPressed(true);
		}

		private void wLabelButton_MouseEnter(object sender, EventArgs e)
		{
			if (this.mLabelButtonEnabled && !this.mLabelButtonHover)
			{
				this.wLabelButton.ImageIndex = 2;
				this.mLabelButtonHover = true;
			}
		}

		private void wLabelButton_MouseLeave(object sender, EventArgs e)
		{
			if (this.mLabelButtonEnabled && this.mLabelButtonHover)
			{
				this.wLabelButton.ImageIndex = 1;
				this.mLabelButtonHover = false;
			}
		}

		private void wLabelButton_MouseUp(object sender, MouseEventArgs e)
		{
			this.ButtonPressed(false);
		}
	}
}