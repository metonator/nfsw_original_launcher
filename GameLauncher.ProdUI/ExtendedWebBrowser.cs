using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class ExtendedWebBrowser : WebBrowser
	{
		private AxHost.ConnectionPointCookie mCookie;

		private ExtendedWebBrowser.WebBrowserExtendedEvents mEvents;

		public object Application
		{
			get
			{
				ExtendedWebBrowser.IWebBrowser2 activeXInstance = base.ActiveXInstance as ExtendedWebBrowser.IWebBrowser2;
				if (activeXInstance == null)
				{
					return null;
				}
				return activeXInstance.Application;
			}
		}

		public ExtendedWebBrowser()
		{
		}

		protected override void CreateSink()
		{
			base.CreateSink();
			this.mEvents = new ExtendedWebBrowser.WebBrowserExtendedEvents(this);
		}

		protected override void DetachSink()
		{
			if (this.mCookie != null)
			{
				this.mCookie.Disconnect();
				this.mCookie = null;
			}
			base.DetachSink();
		}

		protected void OnCommandStateChange(long command, ref bool enable)
		{
			EventHandler<CommandStateChangeEventArgs> eventHandler = this.CommandStateChange;
			CommandStateChangeEventArgs commandStateChangeEventArg = new CommandStateChangeEventArgs(command, ref enable);
			if (eventHandler != null)
			{
				eventHandler(this, commandStateChangeEventArg);
			}
		}

		protected void OnDocumentComplete(object ppDisp, object url)
		{
			EventHandler<DocumentCompleteEventArgs> eventHandler = this.DocumentComplete;
			DocumentCompleteEventArgs documentCompleteEventArg = new DocumentCompleteEventArgs(ppDisp, url);
			if (eventHandler != null)
			{
				eventHandler(this, documentCompleteEventArg);
			}
			ppDisp = documentCompleteEventArg.PPDisp;
		}

		protected void OnNewWindow2(ref object ppDisp, ref bool cancel)
		{
			EventHandler<NewWindow2EventArgs> eventHandler = this.NewWindow2;
			NewWindow2EventArgs newWindow2EventArg = new NewWindow2EventArgs(ref ppDisp, ref cancel);
			if (eventHandler != null)
			{
				eventHandler(this, newWindow2EventArg);
			}
			cancel = newWindow2EventArg.Cancel;
			ppDisp = newWindow2EventArg.PPDisp;
		}

		public event EventHandler<CommandStateChangeEventArgs> CommandStateChange;

		public event EventHandler<DocumentCompleteEventArgs> DocumentComplete;

		public event EventHandler<NewWindow2EventArgs> NewWindow2;

		[Guid("34A715A0-6587-11D0-924A-0020AFC7AC4D")]
		[InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
		[TypeLibType(TypeLibTypeFlags.FHidden)]
		public interface DWebBrowserEvents2
		{
			[DispId(105)]
			void CommandStateChange([In] long command, [In] bool enable);

			[DispId(259)]
			void DocumentComplete([In] object pDisp, [In] ref object URL);

			[DispId(251)]
			void NewWindow2([In][Out] ref object pDisp, [In][Out] ref bool cancel);
		}

		[Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E")]
		[TypeLibType(TypeLibTypeFlags.FHidden | TypeLibTypeFlags.FDual | TypeLibTypeFlags.FOleAutomation)]
		public interface IWebBrowser2
		{
			[DispId(555)]
			bool AddressBar
			{
				get;
				set;
			}

			[DispId(200)]
			object Application
			{
				get;
			}

			[DispId(212)]
			bool Busy
			{
				get;
			}

			[DispId(202)]
			object Container
			{
				get;
			}

			[DispId(203)]
			object Document
			{
				get;
			}

			[DispId(400)]
			string FullName
			{
				get;
			}

			[DispId(407)]
			bool FullScreen
			{
				get;
				set;
			}

			[DispId(209)]
			int Height
			{
				get;
				set;
			}

			[DispId(-515)]
			int HWND
			{
				get;
			}

			[DispId(206)]
			int Left
			{
				get;
				set;
			}

			[DispId(210)]
			string LocationName
			{
				get;
			}

			[DispId(211)]
			string LocationURL
			{
				get;
			}

			[DispId(406)]
			bool MenuBar
			{
				get;
				set;
			}

			[DispId(0)]
			string Name
			{
				get;
			}

			[DispId(550)]
			bool Offline
			{
				get;
				set;
			}

			[DispId(201)]
			object Parent
			{
				get;
			}

			[DispId(401)]
			string Path
			{
				get;
			}

			[DispId(-525)]
			WebBrowserReadyState ReadyState
			{
				get;
			}

			[DispId(552)]
			bool RegisterAsBrowser
			{
				get;
				set;
			}

			[DispId(553)]
			bool RegisterAsDropTarget
			{
				get;
				set;
			}

			[DispId(556)]
			bool Resizable
			{
				get;
				set;
			}

			[DispId(551)]
			bool Silent
			{
				get;
				set;
			}

			[DispId(403)]
			bool StatusBar
			{
				get;
				set;
			}

			[DispId(404)]
			string StatusText
			{
				get;
				set;
			}

			[DispId(554)]
			bool TheaterMode
			{
				get;
				set;
			}

			[DispId(405)]
			int ToolBar
			{
				get;
				set;
			}

			[DispId(207)]
			int Top
			{
				get;
				set;
			}

			[DispId(204)]
			bool TopLevelContainer
			{
				get;
			}

			[DispId(205)]
			string Type
			{
				get;
			}

			[DispId(402)]
			bool Visible
			{
				get;
				set;
			}

			[DispId(208)]
			int Width
			{
				get;
				set;
			}

			[DispId(301)]
			void ClientToWindow(out int pcx, out int pcy);

			[DispId(303)]
			object GetProperty([In] string property);

			[DispId(100)]
			void GoBack();

			[DispId(101)]
			void GoForward();

			[DispId(102)]
			void GoHome();

			[DispId(103)]
			void GoSearch();

			[DispId(104)]
			void Navigate([In] string Url, [In] ref object flags, [In] ref object targetFrameName, [In] ref object postData, [In] ref object headers);

			[DispId(500)]
			void Navigate2([In] ref object URL, [In] ref object flags, [In] ref object targetFrameName, [In] ref object postData, [In] ref object headers);

			[DispId(302)]
			void PutProperty([In] string property, [In] object vtValue);

			[DispId(300)]
			void Quit();

			[DispId(-550)]
			void Refresh();

			[DispId(105)]
			void Refresh2([In] ref object level);

			[DispId(503)]
			void ShowBrowserBar([In] ref object pvaClsid, [In] ref object pvarShow, [In] ref object pvarSize);

			[DispId(106)]
			void Stop();
		}

		public class WebBrowserExtendedEvents : StandardOleMarshalObject, ExtendedWebBrowser.DWebBrowserEvents2
		{
			private ExtendedWebBrowser mBrowser;

			public WebBrowserExtendedEvents(ExtendedWebBrowser browser)
			{
				this.mBrowser = browser;
			}

			public void CommandStateChange(long command, bool enable)
			{
				this.mBrowser.OnCommandStateChange(command, ref enable);
			}

			public void DocumentComplete(object pDisp, ref object url)
			{
				this.mBrowser.OnDocumentComplete(pDisp, url);
			}

			public void NewWindow2(ref object pDisp, ref bool cancel)
			{
				this.mBrowser.OnNewWindow2(ref pDisp, ref cancel);
			}
		}
	}
}