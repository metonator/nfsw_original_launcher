using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class ShardComboBox : UserControl
	{
		public GameLauncher.ProdUI.ShardUrlChanged ShardUrlChanged;

		public GameLauncher.ProdUI.ShardRegionChanged ShardRegionChanged;

		private IContainer components;

		private ComboBox wComboBoxShard;

		public ShardComboBox()
		{
			this.InitializeComponent();
			FontFamily fontFamily = FontWrapper.Instance.GetFontFamily("MyriadProSemiCondBold.ttf");
			this.wComboBoxShard.Font = new System.Drawing.Font(fontFamily, 9.749999f, FontStyle.Bold);
			this.FillShardCombo();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void FillShardCombo()
		{
			if (ShardManager.Shards.Values.Count == 0)
			{
				return;
			}
			foreach (string key in ShardManager.Shards.Keys)
			{
				this.wComboBoxShard.Items.Add(key);
			}
			this.SetSelectedValue();
			this.wComboBoxShard.SelectedValueChanged += new EventHandler(this.wComboBoxShard_SelectedValueChanged);
		}

		private void InitializeComponent()
		{
			this.wComboBoxShard = new ComboBox();
			base.SuspendLayout();
			this.wComboBoxShard.BackColor = Color.White;
			this.wComboBoxShard.DropDownStyle = ComboBoxStyle.DropDownList;
			this.wComboBoxShard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wComboBoxShard.ForeColor = Color.FromArgb(51, 51, 51);
			this.wComboBoxShard.FormattingEnabled = true;
			this.wComboBoxShard.Location = new Point(0, 0);
			this.wComboBoxShard.Name = "wComboBoxShard";
			this.wComboBoxShard.Size = new System.Drawing.Size(185, 24);
			this.wComboBoxShard.TabIndex = 40;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoSize = true;
			base.Controls.Add(this.wComboBoxShard);
			base.Name = "ShardComboBox";
			base.Size = new System.Drawing.Size(188, 27);
			base.ResumeLayout(false);
		}

		public void SetSelectedValue(string shardKey)
		{
			if (!ShardManager.Shards.ContainsKey(shardKey))
			{
				this.SetSelectedValue();
				return;
			}
			ShardManager.ShardKey = shardKey;
			this.wComboBoxShard.SelectedItem = shardKey;
		}

		public void SetSelectedValue()
		{
			string shardKey;
			ShardInfo shardInfo = null;
			ShardManager.Shards.TryGetValue(ShardManager.ShardKey, out shardInfo);
			if (shardInfo != null)
			{
				shardKey = shardInfo.ShardKey;
			}
			else
			{
				shardKey = (string)this.wComboBoxShard.Items[0];
				ShardManager.ShardKey = shardKey;
			}
			this.wComboBoxShard.SelectedItem = shardKey;
		}

		private void wComboBoxShard_SelectedValueChanged(object sender, EventArgs e)
		{
			string selectedItem = (string)this.wComboBoxShard.SelectedItem;
			ShardInfo item = ShardManager.Shards[selectedItem];
			ShardInfo shardInfo = ShardManager.Shards[ShardManager.ShardKey];
			ShardManager.ShardKey = selectedItem;
			if (!string.Equals(item.Url, shardInfo.Url))
			{
				base.BeginInvoke(this.ShardUrlChanged);
				return;
			}
			if (item.RegionId != shardInfo.RegionId)
			{
				base.BeginInvoke(this.ShardRegionChanged);
			}
		}
	}
}