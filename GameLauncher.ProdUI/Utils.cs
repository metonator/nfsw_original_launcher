using log4net;
using Microsoft.Win32;
using System;
using System.IO;

namespace GameLauncher.ProdUI
{
	public class Utils
	{
		public Utils()
		{
		}

		public static float Gauss(float x, float a, float b, float c, float floor, bool inverted)
		{
			if (c == 0f)
			{
				c = 1f;
			}
			a = 0.3989f / c;
			float single = a * (float)Math.Pow(2.71828182845905, -Math.Pow((double)(x - b), 2) / 2 * (double)c * (double)c);
			if (!inverted)
			{
				return single + floor;
			}
			return -single + floor;
		}

		public static string[] RetrieveWebApplicationData()
		{
			string[] strArrays = new string[2];
			string value = (string)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\Shell\\Associations\\UrlAssociations\\http\\UserChoice", "Progid", string.Empty);
			string str = (string)Registry.GetValue(string.Format("HKEY_CURRENT_USER\\Software\\Classes\\{0}\\shell\\open\\command", value), "", string.Empty);
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue(string.Format("HKEY_CLASSES_ROOT\\{0}\\shell\\open\\command", value), "", string.Empty);
			}
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Classes\\http\\shell\\open\\command", "", string.Empty);
			}
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue("HKEY_CLASSES_ROOT\\http\\shell\\open\\command", "", string.Empty);
			}
			GameLauncherUI.Logger.Info(string.Concat("HTTP: ", str));
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Applications\\iexplore.exe\\shell\\open\\command", "", string.Empty);
			}
			if (string.IsNullOrEmpty(str))
			{
				string environmentVariable = Environment.GetEnvironmentVariable("ProgramFiles(x86)");
				if (string.IsNullOrEmpty(environmentVariable))
				{
					environmentVariable = Environment.GetEnvironmentVariable("ProgramFiles");
				}
				string str1 = Path.Combine(environmentVariable, "Internet Explorer\\IEXPLORE.EXE");
				if (!File.Exists(str1))
				{
					return strArrays;
				}
				strArrays[0] = str1;
			}
			string[] strArrays1 = str.Split(new char[] { '\"' });
			if ((int)strArrays1.Length > 1)
			{
				strArrays[0] = strArrays1[1];
			}
			if ((int)strArrays1.Length > 2)
			{
				strArrays[1] = string.Join("\"", strArrays1, 2, (int)strArrays1.Length - 2);
			}
			if (!strArrays[1].Contains("%1"))
			{
				strArrays[1] = string.Concat(strArrays[1], " \"%1\"");
			}
			return strArrays;
		}
	}
}