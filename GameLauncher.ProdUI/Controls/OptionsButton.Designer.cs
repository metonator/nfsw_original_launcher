using GameLauncher.ProdUI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace GameLauncher.ProdUI.Controls
{
	public class OptionsButton : UserControl
	{
		private bool mLabelOptionsHover;

		private IContainer components;

		private ImageList wImageList96;

		private ImageList wImageList120;

		private ImageList wImageList144;

		public Label wLabelOptions;

		public OptionsButton()
		{
			this.InitializeComponent();
			this.SetImageLists();
		}

		public void ButtonPressed(bool isPressed)
		{
			if (isPressed)
			{
				this.wLabelOptions.ImageIndex = 3;
				return;
			}
			this.wLabelOptions.ImageIndex = 2;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(OptionsButton));
			this.wImageList96 = new ImageList(this.components);
			this.wImageList120 = new ImageList(this.components);
			this.wImageList144 = new ImageList(this.components);
			this.wLabelOptions = new Label();
			base.SuspendLayout();
			this.wImageList96.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList96.ImageStream");
			this.wImageList96.TransparentColor = Color.Red;
			this.wImageList96.Images.SetKeyName(0, "nfsw_lp_96dpi_bt_options_enabled.png");
			this.wImageList96.Images.SetKeyName(1, "nfsw_lp_96dpi_bt_options_rollover.png");
			this.wImageList96.Images.SetKeyName(2, "nfsw_lp_96dpi_bt_options_down.png");
			this.wImageList120.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList120.ImageStream");
			this.wImageList120.TransparentColor = Color.Red;
			this.wImageList120.Images.SetKeyName(0, "nfsw_lp_120dpi_bt_options_enabled.png");
			this.wImageList120.Images.SetKeyName(1, "nfsw_lp_120dpi_bt_options_rollover.png");
			this.wImageList120.Images.SetKeyName(2, "nfsw_lp_120dpi_bt_options_down.png");
			this.wImageList144.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList144.ImageStream");
			this.wImageList144.TransparentColor = Color.Red;
			this.wImageList144.Images.SetKeyName(0, "nfsw_lp_144dpi_bt_options_enabled.png");
			this.wImageList144.Images.SetKeyName(1, "nfsw_lp_144dpi_bt_options_rollover.png");
			this.wImageList144.Images.SetKeyName(2, "nfsw_lp_144dpi_bt_options_down.png");
			this.wLabelOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 15f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLabelOptions.ForeColor = Color.White;
			this.wLabelOptions.ImageIndex = 0;
			this.wLabelOptions.ImageList = this.wImageList96;
			this.wLabelOptions.Location = new Point(1, 0);
			this.wLabelOptions.Name = "wLabelOptions";
			this.wLabelOptions.Size = new System.Drawing.Size(31, 29);
			this.wLabelOptions.TabStop = false;
			this.wLabelOptions.TextAlign = ContentAlignment.MiddleCenter;
			this.wLabelOptions.MouseLeave += new EventHandler(this.wLabelOptions_MouseLeave);
			this.wLabelOptions.MouseDown += new MouseEventHandler(this.wLabelOptions_MouseDown);
			this.wLabelOptions.MouseUp += new MouseEventHandler(this.wLabelOptions_MouseUp);
			this.wLabelOptions.MouseEnter += new EventHandler(this.wLabelOptions_MouseEnter);
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoSize = true;
			this.BackColor = Color.Transparent;
			base.Controls.Add(this.wLabelOptions);
			this.MinimumSize = new System.Drawing.Size(31, 29);
			base.Name = "OptionsButton";
			base.Size = new System.Drawing.Size(35, 29);
			base.ResumeLayout(false);
			base.GotFocus += new EventHandler(this.OptionsButton_GotFocus);
			base.LostFocus += new EventHandler(this.OptionsButton_LostFocus);
		}

		private void OptionsButton_GotFocus(object sender, EventArgs e)
		{
			this.wLabelOptions_MouseEnter(null, null);
		}

		private void OptionsButton_LostFocus(object sender, EventArgs e)
		{
			this.wLabelOptions_MouseLeave(null, null);
		}

		private void SetImageLists()
		{
			float dpiX;
			using (Graphics graphic = base.CreateGraphics())
			{
				dpiX = graphic.DpiX;
			}
			if ((double)dpiX <= 96)
			{
				this.wLabelOptions.ImageList = this.wImageList96;
				return;
			}
			if ((double)dpiX <= 120)
			{
				this.wLabelOptions.ImageList = this.wImageList120;
				return;
			}
			if ((double)dpiX <= 144)
			{
				this.wLabelOptions.ImageList = this.wImageList144;
				return;
			}
			LauncherImage launcherImage = new LauncherImage(dpiX);
			this.wLabelOptions.ImageList = launcherImage.ResizeImageList(this.wImageList144, 144f);
		}

		private void wLabelOptions_MouseDown(object sender, MouseEventArgs e)
		{
			this.ButtonPressed(true);
		}

		private void wLabelOptions_MouseEnter(object sender, EventArgs e)
		{
			if (!this.mLabelOptionsHover)
			{
				this.wLabelOptions.ImageIndex = 2;
				this.mLabelOptionsHover = true;
			}
		}

		private void wLabelOptions_MouseLeave(object sender, EventArgs e)
		{
			if (this.mLabelOptionsHover)
			{
				this.wLabelOptions.ImageIndex = 1;
				this.mLabelOptionsHover = false;
			}
		}

		private void wLabelOptions_MouseUp(object sender, MouseEventArgs e)
		{
			this.ButtonPressed(false);
		}
	}
}