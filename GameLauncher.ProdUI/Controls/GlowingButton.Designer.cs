using GameLauncher.ProdUI;
using log4net;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace GameLauncher.ProdUI.Controls
{
	public class GlowingButton : UserControl
	{
		private IContainer components;

		public Label wLabelButton;

		private Timer wTimerGlowingButton;

		private ImageList wImageListButton96;

		private ImageList wImageListButton120;

		private ImageList wImageListButton144;

		private Image mLabelButtonImage;

		private float mCurrentRange = -8f;

		private float mMinRange = -8f;

		private float mMaxRange = 8f;

		private bool mLabelButtonHover;

		public GlowingButton()
		{
			this.InitializeComponent();
			this.SetImageLists();
		}

		public void ButtonPressed(bool isPressed)
		{
			if (isPressed)
			{
				this.wLabelButton.ImageIndex = 3;
				return;
			}
			this.wLabelButton.ImageIndex = 2;
		}

		public void DisableButton(string text)
		{
			if (this.wLabelButton.ImageList == null)
			{
				this.SetImageLists();
			}
			this.wLabelButton.ImageIndex = 0;
			this.wLabelButton.ForeColor = Color.FromArgb(153, 153, 153);
			this.wLabelButton.Text = text;
			this.mCurrentRange = this.mMinRange;
			this.wTimerGlowingButton.Enabled = false;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		public void EnableButton(string text)
		{
			if (this.wLabelButton.ImageList == null)
			{
				this.SetImageLists();
			}
			this.wLabelButton.ImageIndex = 1;
			this.wLabelButton.ForeColor = Color.FromArgb(255, 255, 255);
			this.wLabelButton.Text = text;
			this.mCurrentRange = this.mMinRange;
			this.wTimerGlowingButton.Enabled = true;
		}

		private void GlowingButton_GotFocus(object sender, EventArgs e)
		{
			this.wLabelButton_MouseEnter(null, null);
		}

		private void GlowingButton_LostFocus(object sender, EventArgs e)
		{
			this.wLabelButton_MouseLeave(null, null);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(GlowingButton));
			this.wLabelButton = new Label();
			this.wImageListButton96 = new ImageList(this.components);
			this.wTimerGlowingButton = new Timer(this.components);
			this.wImageListButton120 = new ImageList(this.components);
			this.wImageListButton144 = new ImageList(this.components);
			base.SuspendLayout();
			this.wLabelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.75f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLabelButton.ForeColor = Color.White;
			this.wLabelButton.ImageIndex = 0;
			this.wLabelButton.ImageList = this.wImageListButton96;
			this.wLabelButton.Location = new Point(0, 0);
			this.wLabelButton.Name = "wLabelButton";
			this.wLabelButton.Size = new System.Drawing.Size(165, 58);
			this.wLabelButton.TabIndex = 31;
			this.wLabelButton.TextAlign = ContentAlignment.MiddleCenter;
			this.wLabelButton.MouseEnter += new EventHandler(this.wLabelButton_MouseEnter);
			this.wLabelButton.MouseLeave += new EventHandler(this.wLabelButton_MouseLeave);
			this.wLabelButton.MouseDown += new MouseEventHandler(this.wLabelButton_MouseDown);
			this.wLabelButton.MouseUp += new MouseEventHandler(this.wLabelButton_MouseUp);
			this.wImageListButton96.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButton96.ImageStream");
			this.wImageListButton96.TransparentColor = Color.Red;
			this.wImageListButton96.Images.SetKeyName(0, "nfsw_lp_96dpi_bg_s2_bt_play_disabled.bmp");
			this.wImageListButton96.Images.SetKeyName(1, "nfsw_lp_96dpi_bg_s2_bt_play_enabled.bmp");
			this.wImageListButton96.Images.SetKeyName(2, "nfsw_lp_96dpi_bg_s2_bt_play_rollover.bmp");
			this.wImageListButton96.Images.SetKeyName(3, "nfsw_lp_96dpi_bg_s2_bt_play_down.bmp");
			this.wTimerGlowingButton.Tick += new EventHandler(this.wTimerGlowingButton_Tick);
			this.wImageListButton120.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButton120.ImageStream");
			this.wImageListButton120.TransparentColor = Color.Red;
			this.wImageListButton120.Images.SetKeyName(0, "nfsw_lp_120dpi_bg_s2_bt_play_disabled.bmp");
			this.wImageListButton120.Images.SetKeyName(1, "nfsw_lp_120dpi_bg_s2_bt_play_enabled.bmp");
			this.wImageListButton120.Images.SetKeyName(2, "nfsw_lp_120dpi_bg_s2_bt_play_rollover.bmp");
			this.wImageListButton120.Images.SetKeyName(3, "nfsw_lp_120dpi_bg_s2_bt_play_down.bmp");
			this.wImageListButton144.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButton144.ImageStream");
			this.wImageListButton144.TransparentColor = Color.Red;
			this.wImageListButton144.Images.SetKeyName(0, "nfsw_lp_144dpi_bg_s2_bt_play_disabled.bmp");
			this.wImageListButton144.Images.SetKeyName(1, "nfsw_lp_144dpi_bg_s2_bt_play_enabled.bmp");
			this.wImageListButton144.Images.SetKeyName(2, "nfsw_lp_144dpi_bg_s2_bt_play_rollover.bmp");
			this.wImageListButton144.Images.SetKeyName(3, "nfsw_lp_144dpi_bg_s2_bt_play_down.bmp");
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoSize = true;
			base.Controls.Add(this.wLabelButton);
			base.Name = "GlowingButton";
			base.Size = new System.Drawing.Size(168, 58);
			base.ResumeLayout(false);
			base.GotFocus += new EventHandler(this.GlowingButton_GotFocus);
			base.LostFocus += new EventHandler(this.GlowingButton_LostFocus);
		}

		private void SetImageLists()
		{
			float dpiX;
			using (Graphics graphic = base.CreateGraphics())
			{
				dpiX = graphic.DpiX;
			}
			if ((double)dpiX <= 96)
			{
				this.wLabelButton.ImageList = this.wImageListButton96;
				return;
			}
			if ((double)dpiX <= 120)
			{
				this.wLabelButton.ImageList = this.wImageListButton120;
				return;
			}
			if ((double)dpiX <= 144)
			{
				this.wLabelButton.ImageList = this.wImageListButton144;
				return;
			}
			LauncherImage launcherImage = new LauncherImage(dpiX);
			this.wLabelButton.ImageList = launcherImage.ResizeImageList(this.wImageListButton144, 144f);
		}

		private void wLabelButton_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.wLabelButton.ImageIndex != 0)
			{
				this.ButtonPressed(true);
			}
		}

		private void wLabelButton_MouseEnter(object sender, EventArgs e)
		{
			if (this.wLabelButton.ImageIndex != 0 && !this.mLabelButtonHover)
			{
				this.wLabelButton.ImageIndex = 2;
				this.mLabelButtonHover = true;
			}
		}

		private void wLabelButton_MouseLeave(object sender, EventArgs e)
		{
			if (this.wLabelButton.ImageIndex != 0 && this.mLabelButtonHover)
			{
				this.wLabelButton.ImageIndex = 1;
				this.mLabelButtonHover = false;
			}
		}

		private void wLabelButton_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.wLabelButton.ImageIndex != 0)
			{
				this.ButtonPressed(false);
			}
		}

		private void wTimerGlowingButton_Tick(object sender, EventArgs e)
		{
			try
			{
				if (this.wLabelButton.ImageIndex != -1)
				{
					this.SetImageLists();
					if (this.wLabelButton.ImageIndex == 1)
					{
						this.mLabelButtonImage = this.wLabelButton.ImageList.Images[this.wLabelButton.ImageIndex];
					}
					else
					{
						return;
					}
				}
				float single = Utils.Gauss(this.mCurrentRange, 1f, 0f, 0.4f, 0f, false);
				this.wLabelButton.Image = LauncherImage.SetImgOpacity(this.mLabelButtonImage, single);
				if (this.mCurrentRange >= this.mMaxRange)
				{
					this.mCurrentRange = this.mMinRange;
				}
				else
				{
					GlowingButton glowingButton = this;
					glowingButton.mCurrentRange = glowingButton.mCurrentRange + 0.8f;
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.wTimerGlowingButton.Enabled = false;
				GameLauncherUI.Logger.Error("Exception in the glowing timer, turning glowing off");
				GameLauncherUI.Logger.Error(string.Concat("wTimerGlowingButton_Tick Exception: ", exception.ToString()));
				this.SetImageLists();
				this.wLabelButton.ImageIndex = 1;
			}
		}
	}
}