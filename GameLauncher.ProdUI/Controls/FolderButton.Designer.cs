using GameLauncher.ProdUI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace GameLauncher.ProdUI.Controls
{
	public class FolderButton : UserControl
	{
		private bool mLabelFolderHover;

		private IContainer components;

		public Label wLabelFolder;

		private ImageList wImageList96;

		private ImageList wImageList144;

		private ImageList wImageList120;

		public FolderButton()
		{
			this.InitializeComponent();
			this.SetImageLists();
		}

		public void ButtonPressed(bool isPressed)
		{
			if (isPressed)
			{
				this.wLabelFolder.ImageIndex = 3;
				return;
			}
			this.wLabelFolder.ImageIndex = 2;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FolderButton));
			this.wLabelFolder = new Label();
			this.wImageList96 = new ImageList(this.components);
			this.wImageList144 = new ImageList(this.components);
			this.wImageList120 = new ImageList(this.components);
			base.SuspendLayout();
			this.wLabelFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLabelFolder.ForeColor = Color.White;
			this.wLabelFolder.ImageIndex = 0;
			this.wLabelFolder.ImageList = this.wImageList96;
			this.wLabelFolder.Location = new Point(1, 0);
			this.wLabelFolder.Name = "wLabelFolder";
			this.wLabelFolder.Size = new System.Drawing.Size(31, 29);
			this.wLabelFolder.TabIndex = 0;
			this.wLabelFolder.TextAlign = ContentAlignment.MiddleCenter;
			this.wLabelFolder.MouseLeave += new EventHandler(this.wLabelFolder_MouseLeave);
			this.wLabelFolder.MouseDown += new MouseEventHandler(this.wLabelFolder_MouseDown);
			this.wLabelFolder.MouseUp += new MouseEventHandler(this.wLabelFolder_MouseUp);
			this.wLabelFolder.MouseEnter += new EventHandler(this.wLabelFolder_MouseEnter);
			this.wImageList96.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList96.ImageStream");
			this.wImageList96.TransparentColor = Color.Red;
			this.wImageList96.Images.SetKeyName(0, "nfsw_lp_120dpi_bt_folder_enabled.png");
			this.wImageList96.Images.SetKeyName(1, "nfsw_lp_120dpi_bt_folder_rollover.png");
			this.wImageList96.Images.SetKeyName(2, "nfsw_lp_120dpi_bt_folder_down.png");
			this.wImageList144.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList144.ImageStream");
			this.wImageList144.TransparentColor = Color.Red;
			this.wImageList144.Images.SetKeyName(0, "nfsw_lp_144dpi_bt_folder_enabled.png");
			this.wImageList144.Images.SetKeyName(1, "nfsw_lp_144dpi_bt_folder_rollover.png");
			this.wImageList144.Images.SetKeyName(2, "nfsw_lp_144dpi_bt_folder_down.png");
			this.wImageList120.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageList120.ImageStream");
			this.wImageList120.TransparentColor = Color.Red;
			this.wImageList120.Images.SetKeyName(0, "nfsw_lp_120dpi_bt_folder_enabled.png");
			this.wImageList120.Images.SetKeyName(1, "nfsw_lp_120dpi_bt_folder_rollover.png");
			this.wImageList120.Images.SetKeyName(2, "nfsw_lp_120dpi_bt_folder_down.png");
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoSize = true;
			this.BackColor = Color.Transparent;
			base.Controls.Add(this.wLabelFolder);
			this.MinimumSize = new System.Drawing.Size(31, 29);
			base.Name = "FolderButton";
			base.Size = new System.Drawing.Size(35, 29);
			base.GotFocus += new EventHandler(this.OptionsButton_GotFocus);
			base.LostFocus += new EventHandler(this.OptionsButton_LostFocus);
			base.ResumeLayout(false);
		}

		private void OptionsButton_GotFocus(object sender, EventArgs e)
		{
			this.wLabelFolder_MouseEnter(null, null);
		}

		private void OptionsButton_LostFocus(object sender, EventArgs e)
		{
			this.wLabelFolder_MouseLeave(null, null);
		}

		private void SetImageLists()
		{
			float dpiX;
			using (Graphics graphic = base.CreateGraphics())
			{
				dpiX = graphic.DpiX;
			}
			if ((double)dpiX <= 96)
			{
				this.wLabelFolder.ImageList = this.wImageList96;
				return;
			}
			if ((double)dpiX <= 120)
			{
				this.wLabelFolder.ImageList = this.wImageList120;
				return;
			}
			if ((double)dpiX <= 144)
			{
				this.wLabelFolder.ImageList = this.wImageList144;
				return;
			}
			LauncherImage launcherImage = new LauncherImage(dpiX);
			this.wLabelFolder.ImageList = launcherImage.ResizeImageList(this.wImageList144, 144f);
		}

		private void wLabelFolder_MouseDown(object sender, MouseEventArgs e)
		{
			this.ButtonPressed(true);
		}

		private void wLabelFolder_MouseEnter(object sender, EventArgs e)
		{
			if (!this.mLabelFolderHover)
			{
				this.wLabelFolder.ImageIndex = 2;
				this.mLabelFolderHover = true;
			}
		}

		private void wLabelFolder_MouseLeave(object sender, EventArgs e)
		{
			if (this.mLabelFolderHover)
			{
				this.wLabelFolder.ImageIndex = 1;
				this.mLabelFolderHover = false;
			}
		}

		private void wLabelFolder_MouseUp(object sender, MouseEventArgs e)
		{
			this.ButtonPressed(false);
		}
	}
}