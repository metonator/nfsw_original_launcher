using System;
using System.CodeDom.Compiler;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace GameLauncher.ProdUI.Properties
{
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance;

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool AutoPlay
		{
			get
			{
				return (bool)this["AutoPlay"];
			}
			set
			{
				this["AutoPlay"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string BackGroundImage
		{
			get
			{
				return (string)this["BackGroundImage"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("Data")]
		public string BuildFolder
		{
			get
			{
				return (string)this["BuildFolder"];
			}
		}

		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		[UserScopedSetting]
		public string Email
		{
			get
			{
				return (string)this["Email"];
			}
			set
			{
				this["Email"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string ForceLocale
		{
			get
			{
				return (string)this["ForceLocale"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("nfsw.exe")]
		public string GameExecutable
		{
			get
			{
				return (string)this["GameExecutable"];
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		[UserScopedSetting]
		public int Language
		{
			get
			{
				return (int)this["Language"];
			}
			set
			{
				this["Language"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("Language =")]
		public string LanguageIdentifier
		{
			get
			{
				return (string)this["LanguageIdentifier"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("Language")]
		public string LanguageName
		{
			get
			{
				return (string)this["LanguageName"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("UI")]
		public string LanguagePath
		{
			get
			{
				return (string)this["LanguagePath"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <string>English</string>\r\n  <string>Language = EN</string>\r\n  <string>Deutsch</string>\r\n  <string>Language = DE</string>\r\n  <string>Español</string>\r\n  <string>Language = ES</string>\r\n  <string>Français</string>\r\n  <string>Language = FR</string>\r\n  <string>Polski</string>\r\n  <string>Language = PL</string>\r\n  <string>Русский</string>\r\n  <string>Language = RU</string>\r\n  <string>Português (Brasil)</string>\r\n  <string>Language = PT</string>\r\n  <string>繁體中文</string>\r\n  <string>Language = TC</string>\r\n  <string>简体中文</string>\r\n  <string>Language = SC</string>\r\n  <string>ภาษาไทย</string>\r\n  <string>Language = TH</string>\r\n  <string>Türkçe</string>\r\n  <string>Language = TR</string>\r\n</ArrayOfString>")]
		public StringCollection Languages
		{
			get
			{
				return (StringCollection)this["Languages"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <string>[System</string>\r\n  <string>{0}</string>\r\n  <string>]</string>\r\n</ArrayOfString>")]
		public StringCollection LanguageTemplate
		{
			get
			{
				return (StringCollection)this["LanguageTemplate"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <string>en,en</string>\r\n  <string>de,de</string>\r\n  <string>es,es</string>\r\n  <string>fr,fr</string>\r\n  <string>pl,en</string>\r\n  <string>ru,ru</string>\r\n  <string>pt,en</string>\r\n  <string>tc,tw</string>\r\n  <string>sc,tw</string>\r\n  <string>th,en</string>\r\n  <string>tr,en</string>\r\n</ArrayOfString>")]
		public StringCollection LanguageValues
		{
			get
			{
				return (StringCollection)this["LanguageValues"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
        [DefaultSettingValue("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <string>http://37.233.101.12:1337/nfsw/Engine.svc</string>\r\n</ArrayOfString>")]
		public StringCollection MasterShardUrls
		{
			get
			{
				return (StringCollection)this["MasterShardUrls"];
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool RememberEmail
		{
			get
			{
				return (bool)this["RememberEmail"];
			}
			set
			{
				this["RememberEmail"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("$(APPDATA)/Need for Speed World/Settings/UserSettings.xml")]
		public string SettingFile
		{
			get
			{
				return (string)this["SettingFile"];
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		[UserScopedSetting]
		public string Shard
		{
			get
			{
				return (string)this["Shard"];
			}
			set
			{
				this["Shard"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		[UserScopedSetting]
		public string TOSHash
		{
			get
			{
				return (string)this["TOSHash"];
			}
			set
			{
				this["TOSHash"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("-1")]
		[UserScopedSetting]
		public int Tracks
		{
			get
			{
				return (int)this["Tracks"];
			}
			set
			{
				this["Tracks"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <string>TracksHigh</string>\r\n  <string>Tracks</string>\r\n</ArrayOfString>")]
		public StringCollection TracksFolders
		{
			get
			{
				return (StringCollection)this["TracksFolders"];
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("Tracks")]
		[UserScopedSetting]
		public string TracksName
		{
			get
			{
				return (string)this["TracksName"];
			}
			set
			{
				this["TracksName"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("http://world.needforspeed.com/register")]
		public string UrlCreateNewAccount
		{
			get
			{
				return (string)this["UrlCreateNewAccount"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("https://help.ea.com/{0}/need-for-speed/need-for-speed-world")]
		public string UrlCustomerService
		{
			get
			{
				return (string)this["UrlCustomerService"];
			}
		}

		static Settings()
		{
			Settings.defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
		}

		public Settings()
		{
		}
	}
}