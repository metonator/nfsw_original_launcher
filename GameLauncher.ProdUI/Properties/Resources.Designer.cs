using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace GameLauncher.ProdUI.Properties
{
	[CompilerGenerated]
	[DebuggerNonUserCode]
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
	internal class Resources
	{
		private static System.Resources.ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Resources.resourceCulture;
			}
			set
			{
				Resources.resourceCulture = value;
			}
		}

		internal static Bitmap launcher_greenlight
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("launcher_greenlight", Resources.resourceCulture);
			}
		}

		internal static Bitmap launcher_redlight
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("launcher_redlight", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_120dpi_bg_s0_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_120dpi_bg_s0_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_120dpi_bg_s1_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_120dpi_bg_s1_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_120dpi_bg_s2_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_120dpi_bg_s2_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_144dpi_bg_s0_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_144dpi_bg_s0_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_144dpi_bg_s1_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_144dpi_bg_s1_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_144dpi_bg_s2_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_144dpi_bg_s2_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_96dpi_bg_s0_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_96dpi_bg_s0_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_96dpi_bg_s1_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_96dpi_bg_s1_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_96dpi_bg_s2_w_1bit_alpha
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_96dpi_bg_s2_w_1bit_alpha", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_tos_120dpi_bg
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_tos_120dpi_bg", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_tos_144dpi_bg
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_tos_144dpi_bg", Resources.resourceCulture);
			}
		}

		internal static Bitmap nfsw_lp_tos_96dpi_bg
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("nfsw_lp_tos_96dpi_bg", Resources.resourceCulture);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static System.Resources.ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(Resources.resourceMan, null))
				{
					Resources.resourceMan = new System.Resources.ResourceManager("GameLauncher.ProdUI.Properties.Resources", typeof(Resources).Assembly);
				}
				return Resources.resourceMan;
			}
		}

		internal Resources()
		{
		}
	}
}