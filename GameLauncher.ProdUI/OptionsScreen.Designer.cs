using GameLauncher;
using GameLauncher.ProdUI.Properties;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Threading;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class OptionsScreen : BaseScreen
	{
		private IContainer components;

		private ComboBox wComboBoxLanguage;

		private Label wLabelLanguage;

		private Label wLabelTracks;

		private ComboBox wComboBoxTracks;

		private Label wLabelOptionsHeader;

		private MainButton wMainButtonContinue;

		private Label wLabelHavingTrouble;

		private LinkLabel wLinkLabelCustomerService;

		private Label wLabelLanguageDesc;

		private Label wLabelDownloadDesc;

		private GameLauncherUI parentForm;

		public OptionsScreen(GameLauncherUI launcherForm) : base(launcherForm)
		{
			this.parentForm = launcherForm;
			this.InitializeComponent();
			this.InitializeSettings();
			this.ApplyEmbeddedFonts();
			this.LocalizeFE();
		}

		protected override void ApplyEmbeddedFonts()
		{
			FontFamily fontFamily = FontWrapper.Instance.GetFontFamily("MyriadProSemiCondBold.ttf");
			FontFamily fontFamily1 = FontWrapper.Instance.GetFontFamily("Reg-DB-I.ttf");
			FontFamily fontFamily2 = FontWrapper.Instance.GetFontFamily("Reg-B-I.ttf");
			float single = 1f;
			float single1 = 1f;
			if (string.Compare(Thread.CurrentThread.CurrentUICulture.Name, "th-TH", true) == 0)
			{
				single = 1.4f;
				single1 = 1.3f;
			}
			this.wLabelOptionsHeader.Font = new System.Drawing.Font(fontFamily1, 12.75f * single1, FontStyle.Italic);
			this.wLabelLanguage.Font = new System.Drawing.Font(fontFamily, 9.749999f * single1, FontStyle.Bold);
			this.wComboBoxLanguage.Font = new System.Drawing.Font(fontFamily, 9.749999f, FontStyle.Bold);
			this.wLabelLanguageDesc.Font = new System.Drawing.Font(fontFamily, 9.749999f * single, FontStyle.Bold);
			this.wLabelTracks.Font = new System.Drawing.Font(fontFamily, 9.749999f * single1, FontStyle.Bold);
			this.wComboBoxTracks.Font = new System.Drawing.Font(fontFamily, 9.749999f * single1, FontStyle.Bold);
			this.wLabelDownloadDesc.Font = new System.Drawing.Font(fontFamily, 9.749999f * single, FontStyle.Bold);
			this.wLabelHavingTrouble.Font = new System.Drawing.Font(fontFamily, 9.749999f * single1, FontStyle.Bold);
			this.wLinkLabelCustomerService.Font = new System.Drawing.Font(fontFamily, 9.749999f * single1, FontStyle.Bold);
			this.wMainButtonContinue.wLabelButton.Font = new System.Drawing.Font(fontFamily2, 15f, FontStyle.Bold | FontStyle.Italic);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void FillLanguageCombo()
		{
			for (int i = 0; i < Settings.Default.Languages.Count; i = i + 2)
			{
				this.wComboBoxLanguage.Items.Add(Settings.Default.Languages[i]);
			}
			bool flag = false;
			if (!string.IsNullOrEmpty(this.parentForm.CommandArgLanguage) && this.wComboBoxLanguage.Items.Contains(this.parentForm.CommandArgLanguage))
			{
				this.wComboBoxLanguage.SelectedItem = this.parentForm.CommandArgLanguage;
				flag = true;
			}
			if (!flag && Settings.Default.Language < this.wComboBoxLanguage.Items.Count)
			{
				this.wComboBoxLanguage.SelectedIndex = Settings.Default.Language;
			}
		}

		private void FillTracksCombo()
		{
			for (int i = 0; i < Settings.Default.TracksFolders.Count; i++)
			{
				this.wComboBoxTracks.Items.Add(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", string.Concat("GAMELAUNCHERDESIGNER0005", i)));
			}
			bool flag = false;
			if (!string.IsNullOrEmpty(this.parentForm.CommandArgTracks) && this.wComboBoxTracks.Items.Contains(this.parentForm.CommandArgTracks))
			{
				this.wComboBoxTracks.SelectedItem = this.parentForm.CommandArgTracks;
				flag = true;
			}
			if (Settings.Default.Tracks == -1)
			{
				if (!Directory.Exists("Data\\Tracks"))
				{
					Settings.Default.Tracks = 0;
				}
				else
				{
					Settings.Default.Tracks = 1;
				}
				Settings.Default.Save();
			}
			if (!flag && Settings.Default.Tracks < this.wComboBoxTracks.Items.Count)
			{
				this.wComboBoxTracks.SelectedIndex = Settings.Default.Tracks;
			}
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(OptionsScreen));
			this.wComboBoxLanguage = new ComboBox();
			this.wLabelLanguage = new Label();
			this.wLabelTracks = new Label();
			this.wComboBoxTracks = new ComboBox();
			this.wLabelOptionsHeader = new Label();
			this.wMainButtonContinue = new MainButton();
			this.wLabelHavingTrouble = new Label();
			this.wLinkLabelCustomerService = new LinkLabel();
			this.wLabelLanguageDesc = new Label();
			this.wLabelDownloadDesc = new Label();
			base.SuspendLayout();
			this.wComboBoxLanguage.BackColor = Color.White;
			this.wComboBoxLanguage.DropDownStyle = ComboBoxStyle.DropDownList;
			this.wComboBoxLanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wComboBoxLanguage.ForeColor = Color.FromArgb(51, 51, 51);
			this.wComboBoxLanguage.FormattingEnabled = true;
			this.wComboBoxLanguage.Location = new Point(77, 198);
			this.wComboBoxLanguage.Name = "wComboBoxLanguage";
			this.wComboBoxLanguage.Size = new System.Drawing.Size(185, 24);
			this.wComboBoxLanguage.TabIndex = 1;
			this.wLabelLanguage.BackColor = Color.Transparent;
			this.wLabelLanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold);
			this.wLabelLanguage.ForeColor = Color.White;
			this.wLabelLanguage.Location = new Point(74, 180);
			this.wLabelLanguage.Name = "wLabelLanguage";
			this.wLabelLanguage.Size = new System.Drawing.Size(171, 15);
			this.wLabelLanguage.TabStop = false;
			this.wLabelLanguage.Text = "SELECT LANGUAGE:";
			this.wLabelTracks.BackColor = Color.Transparent;
			this.wLabelTracks.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold);
			this.wLabelTracks.ForeColor = Color.White;
			this.wLabelTracks.Location = new Point(74, 251);
			this.wLabelTracks.Name = "wLabelTracks";
			this.wLabelTracks.Size = new System.Drawing.Size(171, 15);
			this.wLabelLanguage.TabStop = false;
			this.wLabelTracks.Text = "SELECT DOWNLOAD SIZE:";
			this.wComboBoxTracks.BackColor = Color.White;
			this.wComboBoxTracks.DropDownStyle = ComboBoxStyle.DropDownList;
			this.wComboBoxTracks.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wComboBoxTracks.ForeColor = Color.FromArgb(51, 51, 51);
			this.wComboBoxTracks.FormattingEnabled = true;
			this.wComboBoxTracks.Location = new Point(77, 269);
			this.wComboBoxTracks.Name = "wComboBoxTracks";
			this.wComboBoxTracks.Size = new System.Drawing.Size(189, 24);
			this.wComboBoxTracks.TabIndex = 2;
			this.wComboBoxTracks.SelectedIndexChanged += new EventHandler(this.wComboBoxTracks_SelectedIndexChanged);
			this.wLabelOptionsHeader.BackColor = Color.Transparent;
			this.wLabelOptionsHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold);
			this.wLabelOptionsHeader.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLabelOptionsHeader.Location = new Point(74, 117);
			this.wLabelOptionsHeader.Name = "wLabelOptionsHeader";
			this.wLabelOptionsHeader.Size = new System.Drawing.Size(314, 30);
			this.wLabelLanguage.TabStop = false;
			this.wLabelOptionsHeader.Text = "PLEASE SELECT YOUR GAME SETTINGS:";
			this.wLabelOptionsHeader.TextAlign = ContentAlignment.MiddleLeft;
			this.wMainButtonContinue.AutoSize = true;
			this.wMainButtonContinue.BackColor = Color.Transparent;
			this.wMainButtonContinue.LabelButtonEnabled = false;
			this.wMainButtonContinue.Location = new Point(613, 420);
			this.wMainButtonContinue.Name = "wMainButtonContinue";
			this.wMainButtonContinue.Size = new System.Drawing.Size(149, 37);
			this.wMainButtonContinue.TabIndex = 4;
			this.wMainButtonContinue.PreviewKeyDown += new PreviewKeyDownEventHandler(this.OptionsScreen_PreviewKeyDown);
			this.wLabelHavingTrouble.AutoSize = true;
			this.wLabelHavingTrouble.BackColor = Color.Transparent;
			this.wLabelHavingTrouble.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLabelHavingTrouble.ForeColor = Color.White;
			this.wLabelHavingTrouble.Location = new Point(32, 415);
			this.wLabelHavingTrouble.Name = "wLabelHavingTrouble";
			this.wLabelHavingTrouble.Size = new System.Drawing.Size(148, 16);
			this.wLabelLanguage.TabStop = false;
			this.wLabelHavingTrouble.Text = "HAVING TROUBLE?";
			this.wLinkLabelCustomerService.ActiveLinkColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelCustomerService.AutoSize = true;
			this.wLinkLabelCustomerService.BackColor = Color.Transparent;
			this.wLinkLabelCustomerService.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLinkLabelCustomerService.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelCustomerService.LinkColor = Color.FromArgb(76, 178, 255);
			this.wLinkLabelCustomerService.Location = new Point(32, 433);
			this.wLinkLabelCustomerService.Name = "wLinkLabelCustomerService";
			this.wLinkLabelCustomerService.Size = new System.Drawing.Size(226, 16);
			this.wLinkLabelCustomerService.TabIndex = 3;
			this.wLinkLabelCustomerService.TabStop = true;
			this.wLinkLabelCustomerService.Text = "Visit our customer service page";
			this.wLinkLabelCustomerService.LinkClicked += new LinkLabelLinkClickedEventHandler(this.wLinkLabelCustomerService_LinkClicked);
			this.wLabelLanguageDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25f);
			this.wLabelLanguageDesc.ForeColor = Color.White;
			this.wLabelLanguageDesc.ImageAlign = ContentAlignment.MiddleLeft;
			this.wLabelLanguageDesc.Location = new Point(314, 179);
			this.wLabelLanguageDesc.Name = "wLabelLanguageDesc";
			this.wLabelLanguageDesc.Size = new System.Drawing.Size(427, 56);
			this.wLabelLanguage.TabStop = false;
			this.wLabelLanguageDesc.Text = "Select the language that the game text and audio should be displayed in. This will not affect your chat or server options.";
			this.wLabelLanguageDesc.TextAlign = ContentAlignment.MiddleLeft;
			this.wLabelDownloadDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25f);
			this.wLabelDownloadDesc.ForeColor = Color.White;
			this.wLabelDownloadDesc.ImageAlign = ContentAlignment.MiddleLeft;
			this.wLabelDownloadDesc.Location = new Point(313, 242);
			this.wLabelDownloadDesc.Name = "wLabelDownloadDesc";
			this.wLabelDownloadDesc.Size = new System.Drawing.Size(405, 85);
			this.wLabelLanguage.TabStop = false;
			this.wLabelDownloadDesc.Text = componentResourceManager.GetString("wLabelDownloadDesc.Text");
			this.wLabelDownloadDesc.TextAlign = ContentAlignment.MiddleLeft;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackColor = Color.Transparent;
			this.BackgroundImage = Resources.nfsw_lp_96dpi_bg_s0_w_1bit_alpha;
			base.Controls.Add(this.wLabelDownloadDesc);
			base.Controls.Add(this.wLabelLanguageDesc);
			base.Controls.Add(this.wLinkLabelCustomerService);
			base.Controls.Add(this.wLabelHavingTrouble);
			base.Controls.Add(this.wMainButtonContinue);
			base.Controls.Add(this.wLabelOptionsHeader);
			base.Controls.Add(this.wComboBoxTracks);
			base.Controls.Add(this.wLabelTracks);
			base.Controls.Add(this.wLabelLanguage);
			base.Controls.Add(this.wComboBoxLanguage);
			base.Name = "OptionsScreen";
			base.PreviewKeyDown += new PreviewKeyDownEventHandler(this.OptionsScreen_PreviewKeyDown);
			base.Controls.SetChildIndex(this.wComboBoxLanguage, 0);
			base.Controls.SetChildIndex(this.wLabelLanguage, 0);
			base.Controls.SetChildIndex(this.wLabelTracks, 0);
			base.Controls.SetChildIndex(this.wComboBoxTracks, 0);
			base.Controls.SetChildIndex(this.wLabelOptionsHeader, 0);
			base.Controls.SetChildIndex(this.wMainButtonContinue, 0);
			base.Controls.SetChildIndex(this.wLabelHavingTrouble, 0);
			base.Controls.SetChildIndex(this.wLinkLabelCustomerService, 0);
			base.Controls.SetChildIndex(this.wLabelLanguageDesc, 0);
			base.Controls.SetChildIndex(this.wLabelDownloadDesc, 0);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		protected override void InitializeSettings()
		{
			this.backgrounds = new Dictionary<double, Image>()
			{
				{ 96, Resources.nfsw_lp_96dpi_bg_s0_w_1bit_alpha },
				{ 120, Resources.nfsw_lp_120dpi_bg_s0_w_1bit_alpha },
				{ 144, Resources.nfsw_lp_144dpi_bg_s0_w_1bit_alpha }
			};
			this.BackgroundImage = base.SelectBackgroundImage();
			this.FillTracksCombo();
			this.FillLanguageCombo();
			this.wMainButtonContinue.LabelButtonEnabled = true;
			this.wMainButtonContinue.wLabelButton.Click += new EventHandler(this.wMainButtonContinue_Click);
		}

		public override void LoadScreen()
		{
			base.LoadScreen();
			base.ActiveControl = this.wComboBoxLanguage;
		}

		protected override void LocalizeFE()
		{
			this.wLabelOptionsHeader.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00073");
			this.wLabelLanguage.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00074");
			this.wLabelLanguageDesc.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00075");
			this.wLabelTracks.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00076");
			this.wLabelDownloadDesc.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00077");
			this.wLabelHavingTrouble.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00036");
			this.wLinkLabelCustomerService.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00037");
			this.wMainButtonContinue.wLabelButton.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00078");
		}

		private void OptionsScreen_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				this.wMainButtonContinue.ButtonPressed(true);
				this.wMainButtonContinue.Update();
				this.wMainButtonContinue_Click(null, null);
				this.wMainButtonContinue.ButtonPressed(false);
				this.wMainButtonContinue.Update();
			}
		}

		private void SetSelectedLanguage()
		{
			if (this.wComboBoxLanguage.SelectedIndex == Settings.Default.Language)
			{
				return;
			}
			GameLauncherUI.Logger.Info(string.Concat("ComboBox Language changed to ", this.wComboBoxLanguage.SelectedItem.ToString()));
			this.Cursor = Cursors.WaitCursor;
			this.parentForm.StopDownload();
			this.Cursor = Cursors.Default;
			Settings.Default.Language = this.wComboBoxLanguage.SelectedIndex;
		}

		private void SetSelectedTracks()
		{
			if (this.wComboBoxTracks.SelectedIndex == Settings.Default.Tracks)
			{
				return;
			}
			this.Cursor = Cursors.WaitCursor;
			this.parentForm.StopDownload();
			this.Cursor = Cursors.Default;
			Settings.Default.Tracks = this.wComboBoxTracks.SelectedIndex;
		}

		private void wComboBoxTracks_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.wComboBoxTracks.SelectedIndex == 1 && Settings.Default.Tracks != 1 && MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00069"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00070"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				this.wComboBoxTracks.SelectedIndex = 0;
			}
		}

		private void wLinkLabelCustomerService_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (string.IsNullOrEmpty(this.parentForm.WebApplicationData[0]))
			{
				return;
			}
			string empty = string.Empty;
			try
			{
				empty = this.parentForm.WebApplicationData[1].Replace("%1", string.Format(Settings.Default.UrlCustomerService, CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower()));
				Process.Start(this.parentForm.WebApplicationData[0], empty);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.Logger.ErrorFormat("Problem running {0} {1}", this.parentForm.WebApplicationData[0], empty);
				GameLauncherUI.Logger.Error(string.Concat("wLinkLabelCustomerService_LinkClicked Exception: ", exception.ToString()));
			}
		}

		private void wMainButtonContinue_Click(object sender, EventArgs e)
		{
			if (this.wMainButtonContinue.LabelButtonEnabled)
			{
				this.SetSelectedLanguage();
				this.SetSelectedTracks();
				this.parentForm.SwitchToPreviousScreen();
			}
		}
	}
}