using GameLauncher;
using GameLauncher.ProdUI.Properties;
using log4net;
using log4net.Config;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace GameLauncher.ProdUI
{
	public class GameLauncherUI : Form
	{
		private GameLauncher.ProdUI.LoadServerDataFinished mLoadServerDataFinished;

		private GameLauncher.ProdUI.LoadServerStatusFinished mLoadServerStatusFinished;

		private GameLauncher.ProdUI.LoadServerAdsFinished mLoadServerAdsFinished;

		private GameLauncher.ProdUI.DownloadProgressUpdated mDownloadProgressUpdated;

		private GameLauncher.ProdUI.DownloadStarted mDownloadStarted;

		private GameLauncher.ProdUI.DownloadFinished mDownloadFinished;

		private GameLauncher.ProdUI.LoginFinished mLoginFinished;

		private GameLauncher.ProdUI.UpdateStarted mUpdateStarted;

		private string mCommandArgLanguage = string.Empty;

		private string mCommandArgTracks = string.Empty;

		private string mCommandArgShard = string.Empty;

		private string mCommandArgEmail = string.Empty;

		private string mCommandArgPassword = string.Empty;

		private bool mCommandBypassServerCheck;

		private string mUserId = string.Empty;

		private string mUserName = string.Empty;

		private string mRemoteUserId = string.Empty;

		private string mAuthKey = string.Empty;

		private string mWebAuthKey = string.Empty;

		private System.Threading.Timer mHeartbeatTimer;

		private bool mEngineUp;

		private bool mPortalUp;

		private string mPortalDomain = string.Empty;

		private string[] mWebApplicationData = new string[2];

		private Downloader mDownloader;

		private CommandManager mCommandManager;

		private bool mDownloadStopped = true;

		private bool mDownloadCompleted;

		private DateTime mDownloadStartTime;

		private DateTime mDownloadEndTime;

		private TimeSpan mAccumulatedDownloadTime;

		private string[] mDataUnitNames;

		private ulong mFreeBytesAvailable;

		private string mOnVerifyProgressUpdatedText = string.Empty;

		private string mOnDownloadProgressUpdatedText = string.Empty;

		private bool mPlaySuccessful;

		private int mGameLauncherSelfUpdateClosing;

		private bool mClosePending;

		private bool mFormClosing;

		private object mFormClosingLock = new object();

		public Dictionary<ScreenType, BaseScreen> screens;

		private ScreenType previousScreen;

		private ScreenType currentScreen;

		private Dictionary<string, bool> mTelemetrySentActions = new Dictionary<string, bool>();

		private readonly static ILog mLogger;

		private IContainer components;

		private System.Windows.Forms.Timer wTimerServerCheck;

		public string CommandArgEmail
		{
			get
			{
				return this.mCommandArgEmail;
			}
		}

		public string CommandArgLanguage
		{
			get
			{
				return this.mCommandArgLanguage;
			}
		}

		public string CommandArgPassword
		{
			get
			{
				return this.mCommandArgPassword;
			}
		}

		public string CommandArgShard
		{
			get
			{
				return this.mCommandArgShard;
			}
		}

		public string CommandArgTracks
		{
			get
			{
				return this.mCommandArgTracks;
			}
		}

		public bool CommandBypassServerCheck
		{
			get
			{
				return this.mCommandBypassServerCheck;
			}
		}

		public GameLauncher.ProdUI.DownloadFinished DownloadFinished
		{
			get
			{
				return this.mDownloadFinished;
			}
			set
			{
				this.mDownloadFinished = value;
			}
		}

		public GameLauncher.ProdUI.DownloadProgressUpdated DownloadProgressUpdated
		{
			get
			{
				return this.mDownloadProgressUpdated;
			}
			set
			{
				this.mDownloadProgressUpdated = value;
			}
		}

		public GameLauncher.ProdUI.DownloadStarted DownloadStarted
		{
			get
			{
				return this.mDownloadStarted;
			}
			set
			{
				this.mDownloadStarted = value;
			}
		}

		public bool EngineUp
		{
			get
			{
				return this.mEngineUp;
			}
		}

		public GameLauncher.ProdUI.LoadServerAdsFinished LoadServerAdsFinished
		{
			get
			{
				return this.mLoadServerAdsFinished;
			}
			set
			{
				this.mLoadServerAdsFinished = value;
			}
		}

		public GameLauncher.ProdUI.LoadServerDataFinished LoadServerDataFinished
		{
			get
			{
				return this.mLoadServerDataFinished;
			}
			set
			{
				this.mLoadServerDataFinished = value;
			}
		}

		public GameLauncher.ProdUI.LoadServerStatusFinished LoadServerStatusFinished
		{
			get
			{
				return this.mLoadServerStatusFinished;
			}
			set
			{
				this.mLoadServerStatusFinished = value;
			}
		}

		public static ILog Logger
		{
			get
			{
				return GameLauncherUI.mLogger;
			}
		}

		public GameLauncher.ProdUI.LoginFinished LoginFinished
		{
			get
			{
				return this.mLoginFinished;
			}
			set
			{
				this.mLoginFinished = value;
			}
		}

		public string PortalDomain
		{
			get
			{
				return this.mPortalDomain;
			}
		}

		public bool PortalUp
		{
			get
			{
				return this.mPortalUp;
			}
			set
			{
				this.mPortalUp = value;
			}
		}

		public GameLauncher.ProdUI.UpdateStarted UpdateStarted
		{
			get
			{
				return this.mUpdateStarted;
			}
			set
			{
				this.mUpdateStarted = value;
			}
		}

		public string[] WebApplicationData
		{
			get
			{
				return this.mWebApplicationData;
			}
		}

		static GameLauncherUI()
		{
			GameLauncherUI.mLogger = LogManager.GetLogger(typeof(GameLauncherUI));
		}

		public GameLauncherUI(string[] args)
		{
			this.ReadCommandLine(args);
			this.InitializeComponent();
		}

		private string CalculateHash(string fileName)
		{
			string empty = string.Empty;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				using (SHA512 sHA512Managed = new SHA512Managed())
				{
					empty = Convert.ToBase64String(sHA512Managed.ComputeHash(fileStream));
					GameLauncherUI.mLogger.DebugFormat("Hash = '{0}'", empty);
				}
			}
			return empty;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void DownloadFailed()
		{
			this.SendTelemetry("download_failed");
			if (this.mUpdateStarted != null)
			{
				base.BeginInvoke(this.mUpdateStarted);
			}
			if (this.mClosePending)
			{
				base.Close();
			}
		}

		private string EncodeBase64(string text)
		{
			byte[] numArray = new byte[text.Length];
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(text));
		}

		private string EstimateFinishTime(long current, long total, DateTime downloadPartStart)
		{
			double num = (double)current / (double)total;
			if (num < 0.0500000007450581)
			{
				return ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00018");
			}
			TimeSpan now = DateTime.Now - downloadPartStart;
			TimeSpan timeSpan = TimeSpan.FromTicks((long)((double)now.Ticks / num)) - now;
			object hours = timeSpan.Hours;
			string str = timeSpan.Minutes.ToString("D02");
			int seconds = timeSpan.Seconds;
			return string.Format("{0}:{1}:{2}", hours, str, seconds.ToString("D02"));
		}

		private string FormatFileSize(long byteCount)
		{
			double[] numArray = new double[] { 1073741824, 1048576, 1024, 1 };
			for (int i = 0; i < (int)numArray.Length; i++)
			{
				if ((double)byteCount >= numArray[i])
				{
					return string.Concat(string.Format("{0:0.00}", (double)byteCount / numArray[i]), this.mDataUnitNames[i]);
				}
			}
			return string.Concat("0 ", this.mDataUnitNames[3]);
		}

		private void GameLauncher_FormClosing(object sender, FormClosingEventArgs e)
		{
			lock (this.mFormClosingLock)
			{
				if (this.mFormClosing)
				{
					GameLauncherUI.mLogger.Debug("Form already closing");
				}
				else
				{
					this.mFormClosing = true;
					if (!string.IsNullOrEmpty(this.mAuthKey) && !this.mPlaySuccessful)
					{
						this.SendClosingTelemetry(e.CloseReason);
					}
					if (this.mHeartbeatTimer != null)
					{
						this.mHeartbeatTimer.Dispose();
						this.mHeartbeatTimer = null;
					}
					if (this.mClosePending || this.mCommandManager.CurrentCommand == null || !this.mCommandManager.CurrentCommand.Downloader.Verifying && !this.mCommandManager.CurrentCommand.Downloader.Downloading)
					{
						GameLauncherUI.mLogger.Info("Application closing");
					}
					else
					{
						this.mCommandManager.CurrentCommand.Downloader.Stop();
						e.Cancel = true;
						this.mClosePending = true;
						while (this.mCommandManager.CurrentCommand.Downloader.Verifying || this.mCommandManager.CurrentCommand.Downloader.Downloading)
						{
							Thread.Sleep(100);
						}
					}
					bool flag = false;
					int num = 0;
					while (!flag)
					{
						int num1 = num + 1;
						num = num1;
						if (num1 >= 10)
						{
							break;
						}
						try
						{
							ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).Save(ConfigurationSaveMode.Full, true);
							flag = true;
						}
						catch (ConfigurationErrorsException configurationErrorsException1)
						{
							ConfigurationErrorsException configurationErrorsException = configurationErrorsException1;
							try
							{
								string filename = configurationErrorsException.Filename;
								if (File.Exists(filename))
								{
									File.SetAttributes(filename, FileAttributes.Normal);
									File.Delete(filename);
								}
							}
							catch (Exception exception1)
							{
								Exception exception = exception1;
								GameLauncherUI.mLogger.Error(string.Concat("Exception when trying to delete corrupted config file - retries: ", num));
								GameLauncherUI.mLogger.Error(string.Concat("Main Exception: ", exception.ToString()));
							}
						}
						catch (Exception exception2)
						{
							GameLauncherUI.mLogger.Error(string.Concat("GameLauncher_FormClosing Exception ", exception2));
						}
					}
					bool cancel = !e.Cancel;
					GameLauncherUI.mLogger.Debug(string.Concat("Closing: ", cancel.ToString()));
				}
			}
		}

		private void GameLauncherUI_Load(object sender, EventArgs e)
		{
			BasicConfigurator.Configure();
			this.mCommandManager = new CommandManager();
			this.mAccumulatedDownloadTime = new TimeSpan((long)0);
			this.mWebApplicationData = GameLauncher.ProdUI.Utils.RetrieveWebApplicationData();
			this.InitializeDataUnitNames();
			this.InitializeScreens();
			BackgroundWorker backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += new DoWorkEventHandler(this.loadServerDataWorker_DoWork);
			backgroundWorker.RunWorkerAsync();
			this.mOnVerifyProgressUpdatedText = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00046");
			this.mOnDownloadProgressUpdatedText = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00019");
		}

		private string GetWebSecurityToken()
		{
			string[][] strArrays = new string[][] { new string[] { "Content-Type", "text/xml;charset=utf-8" }, new string[] { "Content-Length", "0" }, null, null };
			string[] strArrays1 = new string[] { "userId", this.mUserId };
			strArrays[2] = strArrays1;
			string[] strArrays2 = new string[] { "securityToken", this.mAuthKey };
			strArrays[3] = strArrays2;
			string[][] strArrays3 = strArrays;
			WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
			string str = webServicesWrapper.DoCall(ShardManager.ShardUrl, "/security/generatewebtoken", null, strArrays3, null, RequestMethod.POST);
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.LoadXml(str);
			return xmlDocument.InnerText;
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(GameLauncherUI));
			this.wTimerServerCheck = new System.Windows.Forms.Timer(this.components);
			base.SuspendLayout();
			this.wTimerServerCheck.Interval = 30000;
			this.wTimerServerCheck.Enabled = true;
			this.wTimerServerCheck.Tick += new EventHandler(this.wTimerServerCheck_Tick);
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackColor = Color.White;
			base.ClientSize = new System.Drawing.Size(790, 490);
			this.ForeColor = Color.FromArgb(51, 153, 255);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			base.Icon = (System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(790, 490);
			base.Name = "GameLauncherUI";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "Need for Speed™ World";
			base.TransparencyKey = Color.Red;
			base.Load += new EventHandler(this.GameLauncherUI_Load);
			base.FormClosing += new FormClosingEventHandler(this.GameLauncher_FormClosing);
			base.ResumeLayout(false);
		}

		private void InitializeDataUnitNames()
		{
			string[] str = new string[] { ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00065"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00066"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00067"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00068") };
			this.mDataUnitNames = str;
		}

		private void InitializeScreens()
		{
			this.screens = new Dictionary<ScreenType, BaseScreen>()
			{
				{ ScreenType.Options, new OptionsScreen(this) },
				{ ScreenType.Login, new SSOLoginScreen(this) },
				{ ScreenType.Download, new DownloadScreen(this) }
			};
			this.SwitchScreen(ScreenType.Login);
		}

		private void LoadServerAds_BackgroundWork(object sender, DoWorkEventArgs args)
		{
			LoadServerAdsArgs argument = args.Argument as LoadServerAdsArgs;
			LoadServerAdsResult loadServerAdsResult = new LoadServerAdsResult();
			try
			{
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				string str = webServicesWrapper.DoCall(argument.portalDomain, string.Format("/SpeedAPI/ws/game/nfsw/launcher/content?locale={0}", CultureInfo.CurrentCulture.Name.Replace('-', '\u005F')), null, null, RequestMethod.GET);
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.LoadXml(str);
				foreach (XmlNode xmlNodes in xmlDocument.SelectNodes("/launcherContent/*"))
				{
					string name = xmlNodes.Name;
					string str1 = name;
					if (name == null)
					{
						continue;
					}
					if (str1 == "horizontalPromo")
					{
						foreach (XmlNode childNode in xmlNodes.ChildNodes)
						{
							string name1 = childNode.Name;
							string str2 = name1;
							if (name1 == null)
							{
								continue;
							}
							if (str2 == "image")
							{
								loadServerAdsResult.horizontalImage = childNode.InnerText;
							}
							else if (str2 == "link")
							{
								loadServerAdsResult.horizontalLink = childNode.InnerText;
							}
						}
					}
					else if (str1 == "verticalPromo")
					{
						foreach (XmlNode childNode1 in xmlNodes.ChildNodes)
						{
							string name2 = childNode1.Name;
							string str3 = name2;
							if (name2 == null)
							{
								continue;
							}
							if (str3 == "image")
							{
								loadServerAdsResult.verticalImage = childNode1.InnerText;
							}
							else if (str3 == "link")
							{
								loadServerAdsResult.verticalLink = childNode1.InnerText;
							}
						}
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.Logger.Error(string.Concat("LoadAdData Exception: ", exception.ToString()));
			}
			args.Result = loadServerAdsResult;
		}

		private void LoadServerAds_BackgroundWorkComplete(object sender, RunWorkerCompletedEventArgs e)
		{
			LoadServerAdsResult result = e.Result as LoadServerAdsResult;
			if (this.LoadServerAdsFinished != null)
			{
				GameLauncher.ProdUI.LoadServerAdsFinished loadServerAdsFinished = this.LoadServerAdsFinished;
				object[] objArray = new object[] { result.horizontalImage, result.horizontalLink, result.verticalImage, result.verticalLink };
				base.BeginInvoke(loadServerAdsFinished, objArray);
			}
		}

		public void LoadServerAdsAsync()
		{
			if (!this.EngineUp || !this.PortalUp)
			{
				return;
			}
			LoadServerAdsArgs loadServerAdsArg = new LoadServerAdsArgs()
			{
				shardName = ShardManager.ShardName,
				portalDomain = this.PortalDomain
			};
			BackgroundWorker backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += new DoWorkEventHandler(this.LoadServerAds_BackgroundWork);
			backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.LoadServerAds_BackgroundWorkComplete);
			backgroundWorker.RunWorkerAsync(loadServerAdsArg);
		}

		public void LoadServerData()
		{
			this.mEngineUp = false;
			if (string.IsNullOrEmpty(Settings.Default.Shard))
			{
				return;
			}
			string shardUrl = ShardManager.ShardUrl;
			try
			{
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				string str = webServicesWrapper.DoCall(shardUrl, "/systeminfo", null, null, RequestMethod.GET);
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.LoadXml(str);
				CultureInfo.CurrentUICulture.TwoLetterISOLanguageName.ToLower();
				foreach (XmlNode childNode in xmlDocument.FirstChild.ChildNodes)
				{
					if (childNode.Name != "PortalDomain")
					{
						this.mEngineUp = true;
					}
					else
					{
						this.mPortalDomain = childNode.InnerText;
						if (this.mPortalDomain.ToLower().StartsWith("http://"))
						{
							break;
						}
						this.mPortalDomain = string.Concat("http://", this.mPortalDomain);
						break;
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("LoadServerData Exception: ", exception.ToString()));
				this.mPortalDomain = "http://webkit.world.needforspeed.com";
				this.mEngineUp = false;
			}
			if (this.mLoadServerDataFinished != null)
			{
				base.BeginInvoke(this.mLoadServerDataFinished);
			}
			this.LoadServerStatusAsync();
		}

		private void loadServerDataWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			this.LoadServerData();
		}

		private void LoadServerStatus_BackgroundWork(object sender, DoWorkEventArgs args)
		{
			LoadServerStatusArgs argument = args.Argument as LoadServerStatusArgs;
			LoadServerStatusResult loadServerStatusResult = new LoadServerStatusResult();
			try
			{
				string empty = string.Empty;
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				string str = webServicesWrapper.DoCall(argument.portalDomain, string.Format("/SpeedAPI/ws/game/nfsw/server/status?locale={0}&shard={1}", CultureInfo.CurrentCulture.Name.Replace('-', '\u005F'), argument.shardName), null, null, RequestMethod.GET);
				loadServerStatusResult.portalUp = true;
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.LoadXml(str);
				foreach (XmlNode xmlNodes in xmlDocument.SelectNodes("/worldServerStatus/*"))
				{
					string name = xmlNodes.Name;
					string str1 = name;
					if (name == null)
					{
						continue;
					}
					if (str1 == "status")
					{
						empty = xmlNodes.InnerText;
					}
					else if (str1 == "localizedMessage")
					{
						loadServerStatusResult.statusMessage = xmlNodes.InnerText;
					}
				}
				loadServerStatusResult.serversUp = empty == "UP";
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.Logger.Error(string.Concat("LoadServerStatus Exception: ", exception.ToString()));
				loadServerStatusResult.statusMessage = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00064");
			}
			args.Result = loadServerStatusResult;
		}

		private void LoadServerStatus_BackgroundWorkComplete(object sender, RunWorkerCompletedEventArgs e)
		{
			LoadServerStatusResult result = e.Result as LoadServerStatusResult;
			this.PortalUp = result.portalUp;
			if (this.LoadServerStatusFinished != null)
			{
				GameLauncher.ProdUI.LoadServerStatusFinished loadServerStatusFinished = this.LoadServerStatusFinished;
				object[] objArray = new object[] { result.serversUp, result.statusMessage };
				base.BeginInvoke(loadServerStatusFinished, objArray);
			}
		}

		private void LoadServerStatusAsync()
		{
			if (this.CommandBypassServerCheck)
			{
				GameLauncher.ProdUI.LoadServerStatusFinished loadServerStatusFinished = this.LoadServerStatusFinished;
				object[] objArray = new object[] { true, "Server check bypassed, have a nice day!" };
				base.BeginInvoke(loadServerStatusFinished, objArray);
				return;
			}
			LoadServerStatusArgs loadServerStatusArg = new LoadServerStatusArgs()
			{
				shardName = ShardManager.ShardName,
				portalDomain = this.PortalDomain
			};
			BackgroundWorker backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += new DoWorkEventHandler(this.LoadServerStatus_BackgroundWork);
			backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.LoadServerStatus_BackgroundWorkComplete);
			backgroundWorker.RunWorkerAsync(loadServerStatusArg);
		}

		private void Login(string username, string password)
		{
            LoginSuccessful(username);
		}

		private void LoginSuccessful(string username)
		{
			this.SendTelemetry("login_success");
			this.mUserName = username;
			this.wTimerServerCheck.Enabled = false;
			this.SwitchScreen(ScreenType.Download);
		}

		private void OnDeleteFailed(Exception ex)
		{
			GameLauncherUI.mLogger.Info("Delete Failed");
			this.SendTelemetry("delete_failed");
			System.Windows.Forms.Cursor.Current = Cursors.Default;
			if (ex == null)
			{
				if (this.mClosePending)
				{
					base.Close();
					return;
				}
				if (this.mDownloadStopped)
				{
					return;
				}
				if (this.mCommandManager.Count > 0)
				{
					GameLauncherUI.mLogger.Debug("Executing next in command queue");
					this.mCommandManager.ExecuteNext();
					return;
				}
				GameLauncherUI.mLogger.Error("No command scheduled after delete");
				this.mDownloadCompleted = false;
				if (this.mUpdateStarted != null)
				{
					base.BeginInvoke(this.mUpdateStarted);
				}
			}
		}

		private void OnDeleteFinished()
		{
			GameLauncherUI.mLogger.Info("Delete Finished");
			System.Windows.Forms.Cursor.Current = Cursors.Default;
			if (this.mClosePending)
			{
				base.Close();
				return;
			}
			if (this.mDownloadStopped)
			{
				return;
			}
			if (this.mCommandManager.Count > 0)
			{
				GameLauncherUI.mLogger.Debug("Executing next in command queue");
				this.mCommandManager.ExecuteNext();
				return;
			}
			GameLauncherUI.mLogger.Error("No command scheduled after delete");
			this.mDownloadCompleted = false;
			if (this.mUpdateStarted != null)
			{
				base.BeginInvoke(this.mUpdateStarted);
			}
		}

		private void OnDownloadFailed(Exception ex)
		{
			GameLauncherUI.mLogger.Info("Download Failed");
			this.DownloadFailed();
			this.mDownloadEndTime = DateTime.Now;
			ILog log = GameLauncherUI.mLogger;
			TimeSpan timeSpan = new TimeSpan(this.mDownloadEndTime.Ticks - this.mDownloadStartTime.Ticks);
			log.InfoFormat("Download failed time: {0} s", timeSpan.TotalSeconds);
			TimeSpan timeSpan1 = new TimeSpan(this.mDownloadEndTime.Ticks - this.mDownloadStartTime.Ticks);
			if (timeSpan1 > this.mAccumulatedDownloadTime)
			{
				this.mAccumulatedDownloadTime = timeSpan1;
			}
			if (this.mClosePending)
			{
				base.Close();
				return;
			}
			if (this.mDownloadStopped)
			{
				return;
			}
			if (ex != null)
			{
				GameLauncherUI.mLogger.Error(string.Concat("OnDownloadFailed Exception: ", ex.ToString()));
				if (ex is UncompressionException)
				{
					if (((UncompressionException)ex).ErrorCode == 9)
					{
						MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00055"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
					MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00051"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				if (!(ex is DownloaderException))
				{
					MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00051"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					using (CustomMessageBox customMessageBox = new CustomMessageBox(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00062"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), CustomMessageBox.CustomMessageBoxButtons.OK, CustomMessageBox.CustomMessageBoxIcon.Error))
					{
						customMessageBox.ShowDialog(this);
					}
				}
			}
		}

		private void OnDownloadFinished()
		{
			GameLauncherUI.mLogger.Info("Download Finished");
			if (this.mDownloadProgressUpdated != null)
			{
				GameLauncher.ProdUI.DownloadProgressUpdated downloadProgressUpdated = this.mDownloadProgressUpdated;
				object[] str = new object[] { true, 100, ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00020") };
				base.BeginInvoke(downloadProgressUpdated, str);
			}
			this.mDownloadEndTime = DateTime.Now;
			TimeSpan timeSpan = this.mDownloadEndTime - this.mDownloadStartTime;
			GameLauncherUI.mLogger.InfoFormat("Download successful time: {0} s", timeSpan.TotalSeconds);
			if (timeSpan > this.mAccumulatedDownloadTime)
			{
				this.mAccumulatedDownloadTime = timeSpan;
			}
			if (this.mClosePending)
			{
				base.Close();
				return;
			}
			if (this.mDownloadStopped)
			{
				return;
			}
			if (this.mCommandManager.Count <= 0)
			{
				this.mDownloadCompleted = true;
				if (this.mDownloadFinished != null)
				{
					base.BeginInvoke(this.mDownloadFinished);
				}
				return;
			}
			if (this.mDownloadProgressUpdated != null)
			{
				GameLauncher.ProdUI.DownloadProgressUpdated downloadProgressUpdated1 = this.mDownloadProgressUpdated;
				object[] objArray = new object[] { true, 0, ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00017") };
				base.BeginInvoke(downloadProgressUpdated1, objArray);
			}
			GameLauncherUI.mLogger.Debug("Executing next in command queue");
			this.mCommandManager.ExecuteNext();
		}

		private void OnDownloadProgressUpdated(long downloadLength, long downloadCurrent, long compressedLength, string filename, DateTime downloadPartStart)
		{
			string str = null;
			if (downloadCurrent < compressedLength)
			{
				str = string.Format(this.mOnDownloadProgressUpdatedText, this.FormatFileSize(downloadCurrent), this.FormatFileSize(compressedLength), this.EstimateFinishTime(downloadCurrent, compressedLength, downloadPartStart));
			}
			if (this.mDownloadProgressUpdated != null)
			{
				GameLauncher.ProdUI.DownloadProgressUpdated downloadProgressUpdated = this.mDownloadProgressUpdated;
				object[] objArray = new object[] { true, (int)((long)100 * downloadCurrent / compressedLength), str };
				base.BeginInvoke(downloadProgressUpdated, objArray);
			}
		}

		private void OnShowMessage(string message, string header)
		{
			MessageBox.Show(message, header);
		}

		private void OnVerifyFailed(Exception ex)
		{
			GameLauncherUI.mLogger.Info("Verify Failed");
			this.SendTelemetry("verify_failed");
			System.Windows.Forms.Cursor.Current = Cursors.Default;
			if (ex == null)
			{
				this.DownloadFailed();
			}
			else
			{
				if (!(ex is VerificationException))
				{
					if (ex is DownloaderException)
					{
						this.SendTelemetry("verify_failed_download");
						this.mCommandManager.Clear();
						MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00044"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
						this.DownloadFailed();
						return;
					}
					this.SendTelemetry("verify_failed_unknown");
					this.mCommandManager.Clear();
					MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00045"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.DownloadFailed();
					return;
				}
				ulong sizeRequired = ((VerificationException)ex).SizeRequired;
				if ((double)((float)sizeRequired) * 1.05 <= (double)((float)this.mFreeBytesAvailable))
				{
					this.SendTelemetry("verify_failed_exception");
					return;
				}
				this.SendTelemetry("verify_failed_disk_full");
				string fullPath = Path.GetFullPath(Environment.CurrentDirectory);
				char[] chrArray = new char[] { '\\' };
				string str = fullPath.Split(chrArray)[0];
				MessageBox.Show(string.Format(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00056"), str, (double)((float)sizeRequired) * 1.05 / 1024 / 1024, this.mFreeBytesAvailable / (long)1024 / (long)1024), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.mDownloadCompleted = false;
				if (this.mDownloadProgressUpdated != null)
				{
					GameLauncher.ProdUI.DownloadProgressUpdated downloadProgressUpdated = this.mDownloadProgressUpdated;
					object[] objArray = new object[] { true, 0, ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00025") };
					base.BeginInvoke(downloadProgressUpdated, objArray);
				}
				if (this.mClosePending)
				{
					base.Close();
					return;
				}
			}
		}

		private void OnVerifyFinished()
		{
			GameLauncherUI.mLogger.Info("Verify Finished");
			System.Windows.Forms.Cursor.Current = Cursors.Default;
			if (this.mDownloadProgressUpdated != null)
			{
				GameLauncher.ProdUI.DownloadProgressUpdated downloadProgressUpdated = this.mDownloadProgressUpdated;
				object[] str = new object[] { true, 100, ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00047") };
				base.BeginInvoke(downloadProgressUpdated, str);
			}
		}

		private void OnVerifyProgressUpdated(long downloadLength, long downloadCurrent, long compressedLength, string filename, DateTime downloadPartStart)
		{
			string str;
			str = (downloadCurrent >= downloadLength ? ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00047") : string.Format(this.mOnVerifyProgressUpdatedText, this.FormatFileSize(downloadCurrent), this.FormatFileSize(downloadLength), this.EstimateFinishTime(downloadCurrent, downloadLength, downloadPartStart)));
			if (this.mDownloadProgressUpdated != null)
			{
				GameLauncher.ProdUI.DownloadProgressUpdated downloadProgressUpdated = this.mDownloadProgressUpdated;
				object[] objArray = new object[] { true, (int)((long)100 * downloadCurrent / downloadLength), str };
				base.BeginInvoke(downloadProgressUpdated, objArray);
			}
		}

		public void PerformLogin(string username, string password)
		{
			GameLauncherUI.mLogger.Info("Login button pressed");
			this.Login(username, password);
		}

		public bool PerformLogout()
		{
			this.Cursor = Cursors.WaitCursor;
			this.StopDownload();
			this.SendTelemetry("logout");
			this.mAuthKey = null;
			this.wTimerServerCheck.Enabled = true;
			System.Windows.Forms.Cursor.Current = Cursors.Default;
			this.mHeartbeatTimer.Dispose();
			this.mHeartbeatTimer = null;
			return true;
		}

		public void PerformPlay()
		{
			if (this.mDownloadCompleted)
			{
				try
				{
					GameLauncherUI.mLogger.Info("Play button pressed");
					this.SendTelemetry("play_button_start");
					if (!UserSettings.UpdateUserSettingsXml())
					{
						this.SendTelemetry("save_settings_failed");
					}
					else
					{
						string str = ShardManager.ShardUrl.Trim();
						string shardRegion = ShardManager.ShardRegion;
						Process process = new Process();
						string str1 = Path.Combine(Directory.GetCurrentDirectory(), Settings.Default.BuildFolder);
						process.StartInfo.FileName = Path.Combine(str1, Settings.Default.GameExecutable);
						if (File.Exists(process.StartInfo.FileName))
						{
							try
							{
								X509Certificate x509Certificate = X509Certificate.CreateFromSignedFile(process.StartInfo.FileName);
								X509Certificate2 x509Certificate2 = new X509Certificate2(x509Certificate);
								if (!x509Certificate2.SubjectName.Name.StartsWith("CN=Electronic Arts"))
								{
									this.SendTelemetry("certificate_wrong");
									GameLauncherUI.mLogger.Error(string.Concat("The game executable is signed with the wrong certificate: ", x509Certificate2.SubjectName.Name));
									MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00054"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
									return;
								}
							}
							catch (Exception exception1)
							{
								Exception exception = exception1;
								this.SendTelemetry("certificate_failed");
								GameLauncherUI.mLogger.Error(string.Concat("wLabelButtonPlay_Click Exception: ", exception.ToString()));
								MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00054"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
							this.SendBinaryHash(process.StartInfo.FileName, "Client");
							ProcessStartInfo startInfo = process.StartInfo;
							object[] objArray = new object[] { shardRegion, str, this.mAuthKey, this.mUserId };
							startInfo.Arguments = string.Format("{0} {1} {2} {3}", objArray);
							if (this.SendStartTelemetry(str))
							{
								this.mPlaySuccessful = true;
								this.SendClosingTelemetry(System.Windows.Forms.CloseReason.UserClosing);
								this.SendTelemetry("game_start");
								process.Start();
								this.SendTelemetry("game_started");
								base.Close();
							}
							else
							{
								this.SendTelemetry("engine_telemetry_failed");
								MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00059"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
								this.PerformLogout();
								return;
							}
						}
						else
						{
							this.SendTelemetry("file_exists_failed");
							MessageBox.Show(string.Format(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00030"), process.StartInfo.FileName), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
				}
				catch (Exception exception3)
				{
					Exception exception2 = exception3;
					this.SendTelemetry("play_button_failed");
					GameLauncherUI.mLogger.Error(string.Concat("PerformPlay Exception: ", exception2.ToString()));
					MessageBox.Show(string.Concat(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00031"), Environment.NewLine, Environment.NewLine, exception2.Message), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		public void PerformUpdate()
		{
			if (!this.mDownloadCompleted)
			{
				GameLauncherUI.mLogger.Info("Update button pressed");
				this.SendTelemetry("update_button_start");
				this.StartDownload();
			}
		}

		private void ReadCommandLine(string[] args)
		{
			string[] strArrays = args;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				if (str.ToLower().Contains("language="))
				{
					this.mCommandArgLanguage = str.Replace("language=", "");
				}
				else if (str.ToLower().Contains("tracks="))
				{
					this.mCommandArgTracks = str.Replace("tracks=", "");
				}
				else if (str.ToLower().Contains("email="))
				{
					this.mCommandArgEmail = str.Replace("email=", "");
				}
				else if (str.ToLower().Contains("password="))
				{
					this.mCommandArgPassword = str.Replace("password=", "");
				}
				else if (str.ToLower().Contains("bypassservercheck="))
				{
					this.mCommandBypassServerCheck = true;
				}
			}
		}

		public void ReplaceScreen(ScreenType screenType, BaseScreen screen)
		{
			if (this.currentScreen != screenType)
			{
				return;
			}
			base.Controls.Remove(this.screens[screenType]);
			this.screens[screenType] = screen;
			base.Controls.Add(this.screens[screenType]);
			this.screens[screenType].LoadScreen();
		}

		private void ScheduleDownloadingProcess()
		{
			this.mDownloader = new Downloader(this)
			{
				DeleteFinished = new DownloaderActionFinished(this.OnDeleteFinished),
				DeleteFailed = new DownloaderActionFailed(this.OnDeleteFailed)
			};
			DeleteCommand deleteCommand = new DeleteCommand(this.mDownloader);
			this.mCommandManager.AddBack(deleteCommand, new DeleteCommandArgument(Program.CdnUrl, string.Empty, Settings.Default.BuildFolder));
			this.mDownloader = new Downloader(this, 3, 2, 64)
			{
				VerifyProgressUpdated = new DownloaderActionUpdated(this.OnVerifyProgressUpdated),
				VerifyFinished = new DownloaderActionFinished(this.OnVerifyFinished),
				VerifyFailed = new DownloaderActionFailed(this.OnVerifyFailed),
				DownloadProgressUpdated = new DownloaderActionUpdated(this.OnDownloadProgressUpdated),
				DownloadFinished = new DownloaderActionFinished(this.OnDownloadFinished),
				DownloadFailed = new DownloaderActionFailed(this.OnDownloadFailed),
				ShowMessage = new ShowMessage(this.OnShowMessage),
				SendTelemetry = new SendTelemetry(this.SendTelemetry)
			};
			VerifyCommand verifyCommand = new VerifyCommand(this.mDownloader);
			this.mCommandManager.AddBack(verifyCommand, new VerifyCommandArgument(Program.CdnUrl, string.Empty, Settings.Default.BuildFolder, false, false, false, true));
			string empty = string.Empty;
			try
			{
				empty = Settings.Default.TracksFolders[Settings.Default.Tracks];
				GameLauncherUI.mLogger.DebugFormat("Tracks package for '{0}' found, scheduling verification", empty);
				this.mCommandManager.AddBack(deleteCommand, new DeleteCommandArgument(Program.CdnUrl, empty, Settings.Default.BuildFolder));
				this.mCommandManager.AddBack(verifyCommand, new VerifyCommandArgument(Program.CdnUrl, empty, Settings.Default.BuildFolder, false, false, false, true));
			}
			catch (Exception exception)
			{
				GameLauncherUI.mLogger.ErrorFormat("The tracks package '{0}' does not exit", empty);
			}
			string str = string.Empty;
			try
			{
				string str1 = Settings.Default.LanguageValues[Settings.Default.Language].Trim();
				char[] chrArray = new char[] { ',' };
				str = str1.Split(chrArray)[1];
				GameLauncherUI.mLogger.DebugFormat("Language package for '{0}' found, scheduling verification", str);
				this.mCommandManager.AddBack(deleteCommand, new DeleteCommandArgument(Program.CdnUrl, str, Settings.Default.BuildFolder));
				this.mCommandManager.AddBack(verifyCommand, new VerifyCommandArgument(Program.CdnUrl, str, Settings.Default.BuildFolder, false, false, false, true));
			}
			catch (Exception exception1)
			{
				GameLauncherUI.mLogger.ErrorFormat("The language package '{0}' does not exit", str);
			}
		}

		private void SendBinaryHash(string fileName, string hashId)
		{
			GameLauncherUI.mLogger.DebugFormat("Calculate the hash for {0} {1}", hashId, fileName);
			try
			{
				this.SendHash(this.CalculateHash(fileName), hashId);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("SendBinaryHash Exception: ", exception.ToString()));
			}
		}

		private void SendClosingTelemetry(System.Windows.Forms.CloseReason reason)
		{
			string str;
			string str1 = "0";
			if (this.mDownloadCompleted)
			{
				long totalSeconds = (long)this.mAccumulatedDownloadTime.TotalSeconds;
				str1 = totalSeconds.ToString(CultureInfo.InvariantCulture);
			}
			double totalMinutes = this.mAccumulatedDownloadTime.TotalMinutes;
			if (totalMinutes < 15)
			{
				str = "15";
			}
			else if (totalMinutes < 30)
			{
				str = "30";
			}
			else if (totalMinutes < 45)
			{
				str = "45";
			}
			else if (totalMinutes < 60)
			{
				str = "60";
			}
			else if (totalMinutes < 90)
			{
				str = "90";
			}
			else if (totalMinutes < 120)
			{
				str = "120";
			}
			else if (totalMinutes >= 180)
			{
				str = (totalMinutes >= 240 ? "240plus" : "240");
			}
			else
			{
				str = "180";
			}
			this.SendTelemetry(string.Format("patch_time_{0}_{1}", str, (this.mDownloadCompleted ? "completed" : "partial")));
			try
			{
				GameLauncherUI.mLogger.Info("Sending closing gamelauncher telemetry");
				string shardUrl = ShardManager.ShardUrl;
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				StringBuilder stringBuilder = new StringBuilder();
				XmlWriter xmlTextWriter = new XmlTextWriter(new StringWriter(stringBuilder));
				xmlTextWriter.WriteStartElement("LauncherEndTrans", "http://schemas.datacontract.org/2004/07/Victory.DataLayer.Serialization");
				string str2 = "3";
				switch (reason)
				{
					case System.Windows.Forms.CloseReason.WindowsShutDown:
					{
						str2 = "4";
						goto case System.Windows.Forms.CloseReason.MdiFormClosing;
					}
					case System.Windows.Forms.CloseReason.MdiFormClosing:
					{
						xmlTextWriter.WriteStartElement("leaveReasonID");
						xmlTextWriter.WriteString(str2);
						xmlTextWriter.WriteEndElement();
						xmlTextWriter.WriteStartElement("patchTime");
						xmlTextWriter.WriteString(str1);
						xmlTextWriter.WriteEndElement();
						xmlTextWriter.WriteStartElement("userID");
						xmlTextWriter.WriteString(this.mUserId);
						xmlTextWriter.WriteEndElement();
						xmlTextWriter.WriteEndElement();
						xmlTextWriter.Flush();
						xmlTextWriter.Close();
						string[][] strArrays = new string[][] { new string[] { "Content-Type", "text/xml;charset=utf-8" }, null, null, null };
						string[] strArrays1 = new string[] { "Content-Length", null };
						int length = stringBuilder.Length;
						strArrays1[1] = length.ToString(CultureInfo.InvariantCulture);
						strArrays[1] = strArrays1;
						string[] strArrays2 = new string[] { "userId", this.mUserId };
						strArrays[2] = strArrays2;
						string[] strArrays3 = new string[] { "securityToken", this.mAuthKey };
						strArrays[3] = strArrays3;
						string[][] strArrays4 = strArrays;
						webServicesWrapper.DoCall(shardUrl, "/Reporting/LauncherPatcherEnd/", null, strArrays4, stringBuilder.ToString(), RequestMethod.POST);
						GameLauncherUI.mLogger.Info(string.Format("Closing telemetry sent successfully : leave reason : {0}", str2));
						this.SendTelemetry(string.Format("leave_reason_{0}", str2));
						break;
					}
					case System.Windows.Forms.CloseReason.UserClosing:
					{
						if (this.mGameLauncherSelfUpdateClosing == 1)
						{
							str2 = "6";
							goto case System.Windows.Forms.CloseReason.MdiFormClosing;
						}
						else if (this.mGameLauncherSelfUpdateClosing == 2)
						{
							str2 = "7";
							goto case System.Windows.Forms.CloseReason.MdiFormClosing;
						}
						else if (this.mGameLauncherSelfUpdateClosing != 3)
						{
							str2 = (this.mPlaySuccessful ? "1" : "2");
							goto case System.Windows.Forms.CloseReason.MdiFormClosing;
						}
						else
						{
							str2 = "8";
							goto case System.Windows.Forms.CloseReason.MdiFormClosing;
						}
					}
					case System.Windows.Forms.CloseReason.TaskManagerClosing:
					{
						str2 = "5";
						goto case System.Windows.Forms.CloseReason.MdiFormClosing;
					}
					default:
					{
						goto case System.Windows.Forms.CloseReason.MdiFormClosing;
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("SendClosingTelemetry Exception: ", exception.ToString()));
			}
		}

		private void SendHash(string hash, string hashId)
		{
			try
			{
				GameLauncherUI.mLogger.Debug(string.Concat("Sending Hash for ", hashId));
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				StringBuilder stringBuilder = new StringBuilder();
				XmlWriter xmlTextWriter = new XmlTextWriter(new StringWriter(stringBuilder));
				xmlTextWriter.WriteStartElement("FraudDetectionCollection", "http://schemas.datacontract.org/2004/07/Victory.DataLayer.Serialization");
				xmlTextWriter.WriteStartElement("FraudDetectionLogs");
				xmlTextWriter.WriteStartElement("FraudDetection");
				xmlTextWriter.WriteStartElement("IsEncrypted");
				xmlTextWriter.WriteString("false");
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("ModuleName");
				xmlTextWriter.WriteString(this.EncodeBase64(hashId));
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("ModulePath");
				xmlTextWriter.WriteString(this.EncodeBase64("GameLauncher"));
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("ModuleValue");
				xmlTextWriter.WriteString(hash);
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.Flush();
				xmlTextWriter.Close();
				string[][] strArrays = new string[][] { new string[] { "Content-Type", "text/xml;charset=utf-8" }, null, null, null };
				string[] str = new string[] { "Content-Length", null };
				int length = stringBuilder.Length;
				str[1] = length.ToString(CultureInfo.InvariantCulture);
				strArrays[1] = str;
				string[] strArrays1 = new string[] { "userId", this.mUserId };
				strArrays[2] = strArrays1;
				string[] strArrays2 = new string[] { "securityToken", this.mAuthKey };
				strArrays[3] = strArrays2;
				string[][] strArrays3 = strArrays;
				webServicesWrapper.DoCall(ShardManager.ShardUrl, "/security/logFraudDetectionEvents", null, strArrays3, stringBuilder.ToString(), RequestMethod.POST);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("SendHash Exception: ", exception.ToString()));
			}
		}

		private void SendHeartbeat(object stateInfo)
		{
			try
			{
				GameLauncherUI.mLogger.Debug("Sending heartbeat");
				string shardUrl = ShardManager.ShardUrl;
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				string[][] strArrays = new string[][] { new string[] { "Content-Type", "text/xml;charset=utf-8" }, new string[] { "Content-Length", "0" }, null, null };
				string[] strArrays1 = new string[] { "userId", this.mUserId };
				strArrays[2] = strArrays1;
				string[] strArrays2 = new string[] { "securityToken", this.mAuthKey };
				strArrays[3] = strArrays2;
				string[][] strArrays3 = strArrays;
				webServicesWrapper.DoCall(shardUrl, "/heartbeatLauncher", null, strArrays3, null, RequestMethod.POST);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("SendHeartbeat Exception: ", exception.ToString()));
			}
		}

		private bool SendStartTelemetry(string serverUrl)
		{
			bool flag;
			try
			{
				GameLauncherUI.mLogger.Info("Sending start client telemetry");
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				StringBuilder stringBuilder = new StringBuilder();
				XmlWriter xmlTextWriter = new XmlTextWriter(new StringWriter(stringBuilder));
				xmlTextWriter.WriteStartElement("LauncherStartTrans", "http://schemas.datacontract.org/2004/07/Victory.DataLayer.Serialization");
				xmlTextWriter.WriteStartElement("autoLogin");
				xmlTextWriter.WriteString((Settings.Default.RememberEmail ? "1" : "0"));
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("autoStart");
				xmlTextWriter.WriteString("0");
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("language");
				xmlTextWriter.WriteString(string.Format("{0}", Settings.Default.Language + 1));
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("region");
				xmlTextWriter.WriteString(string.Format("{0}", ShardManager.ShardRegionId));
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("sku");
				xmlTextWriter.WriteString("");
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteStartElement("userID");
				xmlTextWriter.WriteString(this.mUserId);
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.WriteEndElement();
				xmlTextWriter.Flush();
				xmlTextWriter.Close();
				string[][] strArrays = new string[][] { new string[] { "Content-Type", "text/xml;charset=utf-8" }, null, null, null };
				string[] str = new string[] { "Content-Length", null };
				int length = stringBuilder.Length;
				str[1] = length.ToString(CultureInfo.InvariantCulture);
				strArrays[1] = str;
				string[] strArrays1 = new string[] { "userId", this.mUserId };
				strArrays[2] = strArrays1;
				string[] strArrays2 = new string[] { "securityToken", this.mAuthKey };
				strArrays[3] = strArrays2;
				string[][] strArrays3 = strArrays;
				webServicesWrapper.DoCall(serverUrl, "/Reporting/LauncherPatcherStart/", null, strArrays3, stringBuilder.ToString(), RequestMethod.POST);
				GameLauncherUI.mLogger.Info("start client telemetry sent successfully");
			}
			catch (WebServicesWrapperServerException webServicesWrapperServerException1)
			{
				WebServicesWrapperServerException webServicesWrapperServerException = webServicesWrapperServerException1;
				GameLauncherUI.mLogger.Error(string.Concat("SendStartTelemetry WebServicesWrapperServerException: ", webServicesWrapperServerException.ToString()));
				if (webServicesWrapperServerException.ErrorCode == 1503)
				{
					flag = false;
					return flag;
				}
			}
			catch (WebServicesWrapperHttpException webServicesWrapperHttpException1)
			{
				WebServicesWrapperHttpException webServicesWrapperHttpException = webServicesWrapperHttpException1;
				GameLauncherUI.mLogger.Error(string.Concat("SendStartTelemetry WebServicesWrapperHTTPException: ", webServicesWrapperHttpException.ToString()));
				flag = false;
				return flag;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("SendStartTelemetry Exception: ", exception.ToString()));
			}
			return true;
		}

		private void SendTelemetry(string action)
		{
			if (!this.mPortalUp)
			{
				return;
			}
			if (!this.mTelemetrySentActions.ContainsKey(action))
			{
				BackgroundWorker backgroundWorker = new BackgroundWorker();
				backgroundWorker.DoWork += new DoWorkEventHandler(this.SendTelemetry_DoWork);
				backgroundWorker.RunWorkerAsync(action);
				this.mTelemetrySentActions.Add(action, true);
			}
		}

		private void SendTelemetry_DoWork(object sender, DoWorkEventArgs args)
		{
			string argument = args.Argument as string;
			try
			{
				string str = "http";
				GameLauncherUI.mLogger.Info(string.Format("Sending telemetry with action={0}", argument));
				string str1 = string.Format("gl_{0}_{1}", str, Program.AssemblyVersion);
				WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
				webServicesWrapper.DoCall(this.mPortalDomain, string.Format("/SpeedAPI/ws/game/nfsw/telemetry?category={0}&action={1}", str1, argument), null, null, RequestMethod.POST);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("SendTelemetry Exception: ", exception.ToString()));
			}
		}

		public bool SetRegion()
		{
			bool flag;
			try
			{
				Engine.Instance.SetRegion(ShardManager.ShardUrl, this.mUserId, this.mAuthKey, ShardManager.ShardRegionId);
				return true;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.mLogger.Error(string.Concat("Set Region Exception: ", exception.ToString()));
				flag = false;
			}
			return flag;
		}

		private void ShowErrorMessage(string message)
		{
			using (CustomMessageBox customMessageBox = new CustomMessageBox(message, ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), CustomMessageBox.CustomMessageBoxButtons.OK, CustomMessageBox.CustomMessageBoxIcon.Error))
			{
				customMessageBox.ShowDialog(this);
			}
		}

		private void ShowInternalErrorMessage(int errorCode, string message)
		{
			
		}

		private bool ShowTos(bool force)
		{
			try
			{
				using (WebClient webClient = new WebClient())
				{
					byte[] numArray = webClient.DownloadData(Program.TermsOfService);
					using (MD5 mD5 = MD5.Create())
					{
						string base64String = Convert.ToBase64String(mD5.ComputeHash(numArray));
						if (force || Settings.Default.TOSHash != base64String)
						{
							this.SendTelemetry("tos_show");
							using (Tos to = new Tos(Encoding.UTF8.GetString(numArray)))
							{
								if (to.ShowDialog() == System.Windows.Forms.DialogResult.OK)
								{
									Settings.Default.TOSHash = base64String;
									Settings.Default.Save();
									this.SendTelemetry("tos_accepted");
									GameLauncherUI.mLogger.Info("Terms of Service accepted");
								}
								else
								{
									GameLauncherUI.mLogger.Info("Terms of Service not accepted");
									this.mUserId = string.Empty;
									this.mAuthKey = string.Empty;
									MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00052"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00053"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
									this.SendTelemetry("tos_declined");
									return false;
								}
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.SendTelemetry("tos_exception");
				GameLauncherUI.mLogger.Info("Terms of Service exception - skipping");
			}
			return true;
		}

		public bool SingleSignOnLogin(string authToken)
		{
			bool flag;
			string item = "";
			try
			{
				Dictionary<string, string> strs = null;
				string shardKey = ShardManager.ShardKey;
				string shardUrl = ShardManager.ShardUrl;
				ILog log = GameLauncherUI.mLogger;
				object[] shardRegion = new object[] { shardKey, shardUrl, ShardManager.ShardRegion, authToken };
				log.InfoFormat("Trying to log into shard'{0}:{1}' and region '{2}' with auth token '{3}'", shardRegion);
				this.SendTelemetry("login_try");
				this.SendTelemetry(((new WindowsPrincipal(WindowsIdentity.GetCurrent())).IsInRole(WindowsBuiltInRole.Administrator) ? "login_admin" : "login_no_admin"));
				this.SendTelemetry(string.Format("lang_{0}", CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower()));
				try
				{
					strs = Engine.Instance.SSOLogin(shardUrl, authToken, ShardManager.ShardRegion);
				}
				catch (WebServicesWrapperServerException webServicesWrapperServerException1)
				{
					WebServicesWrapperServerException webServicesWrapperServerException = webServicesWrapperServerException1;
					if (webServicesWrapperServerException.ErrorCode != -1730 && webServicesWrapperServerException.ErrorCode != -1612)
					{
						GameLauncherUI.mLogger.ErrorFormat("SSO Login exception WebServicesWrapperServerException: {0} - {1}", webServicesWrapperServerException.ErrorCode, webServicesWrapperServerException.ToString());
						int errorCode = webServicesWrapperServerException.ErrorCode;
						this.SendTelemetry(string.Format("sso_login_error_{0}", errorCode.ToString()));
						this.ShowInternalErrorMessage(webServicesWrapperServerException.ErrorCode, webServicesWrapperServerException.Message);
						flag = false;
						return flag;
					}
					else if (this.ShowTos(true))
					{
						strs = Engine.Instance.SSOLogin(shardUrl, authToken, ShardManager.ShardRegion, true);
					}
					else
					{
						flag = false;
						return flag;
					}
				}
				item = strs["username"];
				this.mUserId = strs["userId"];
				this.mAuthKey = strs["securityToken"];
				this.mRemoteUserId = strs["remoteUserId"];
				if (this.ShowTos(false))
				{
					this.mWebAuthKey = this.GetWebSecurityToken();
					this.SendBinaryHash(Assembly.GetExecutingAssembly().Location, "Downloader");
					if (this.mLoginFinished != null)
					{
						GameLauncher.ProdUI.LoginFinished loginFinished = this.mLoginFinished;
						object[] objArray = new object[] { this.mRemoteUserId, this.mWebAuthKey };
						base.BeginInvoke(loginFinished, objArray);
					}
					this.mHeartbeatTimer = new System.Threading.Timer(new TimerCallback(this.SendHeartbeat), this, 900000, 900000);
					GameLauncherUI.mLogger.Info("SSO Login successful");
					this.LoginSuccessful(item);
					return true;
				}
				else
				{
					flag = false;
				}
			}
			catch (WebServicesWrapperServerException webServicesWrapperServerException3)
			{
				WebServicesWrapperServerException webServicesWrapperServerException2 = webServicesWrapperServerException3;
				GameLauncherUI.mLogger.ErrorFormat("SSO Login exception WebServicesWrapperServerException: {0} - {1}", webServicesWrapperServerException2.ErrorCode, webServicesWrapperServerException2.ToString());
				int num = webServicesWrapperServerException2.ErrorCode;
				this.SendTelemetry(string.Format("sso_login_error_{0}", num.ToString()));
				this.ShowInternalErrorMessage(webServicesWrapperServerException2.ErrorCode, webServicesWrapperServerException2.Message);
				flag = false;
			}
			catch
			{
				GameLauncherUI.mLogger.Error("SSO Login exception");
				flag = false;
			}
			return flag;
		}

		public void StartDownload()
		{
			ulong num;
			ulong num1;
			if (!this.mDownloadStopped)
			{
				return;
			}
			this.mDownloadStopped = false;
			this.ScheduleDownloadingProcess();
			string fullPath = Path.GetFullPath(Environment.CurrentDirectory);
			char[] chrArray = new char[] { '\\' };
			string str = fullPath.Split(chrArray)[0];
			GameLauncher.ProdUI.UnsafeNativeMethods.GetDiskFreeSpaceEx(str, out this.mFreeBytesAvailable, out num, out num1);
			this.mDownloadStartTime = DateTime.Now;
			if (this.mCommandManager.Count > 0)
			{
				GameLauncherUI.mLogger.Debug("Executing next in command queue");
				this.mCommandManager.ExecuteNext();
			}
			if (this.mDownloadStarted != null)
			{
				base.BeginInvoke(this.mDownloadStarted);
			}
		}

		public void StopDownload()
		{
			if (this.mDownloadStopped)
			{
				return;
			}
			if (this.mDownloader != null)
			{
				this.mDownloader.VerifyProgressUpdated = null;
				this.mDownloader.VerifyFinished = null;
				this.mDownloader.VerifyFailed = null;
				this.mDownloader.DownloadProgressUpdated = null;
				this.mDownloader.DownloadFinished = null;
				this.mDownloader.DownloadFailed = null;
			}
			if (this.mCommandManager.CurrentCommand != null && (this.mCommandManager.CurrentCommand.Downloader.Downloading || this.mCommandManager.CurrentCommand.Downloader.Verifying))
			{
				this.mCommandManager.CurrentCommand.Downloader.Stop();
				this.mCommandManager.Clear();
			}
			this.mDownloadCompleted = false;
			this.mDownloadStopped = true;
			if (this.mDownloadProgressUpdated != null)
			{
				GameLauncher.ProdUI.DownloadProgressUpdated downloadProgressUpdated = this.mDownloadProgressUpdated;
				object[] objArray = new object[] { false, 0, " " };
				base.BeginInvoke(downloadProgressUpdated, objArray);
			}
		}

		public void SwitchScreen(ScreenType newScreen)
		{
			if (this.screens.ContainsKey(newScreen))
			{
				this.previousScreen = this.currentScreen;
				if (this.currentScreen != ScreenType.None)
				{
					this.screens[this.currentScreen].UnloadScreen();
					base.Controls.Remove(this.screens[this.currentScreen]);
				}
				this.currentScreen = newScreen;
				base.Controls.Add(this.screens[newScreen]);
				this.screens[newScreen].LoadScreen();
			}
		}

		public void SwitchToPreviousScreen()
		{
			if (this.previousScreen != ScreenType.None)
			{
				this.screens[this.currentScreen].UnloadScreen();
				base.Controls.Remove(this.screens[this.currentScreen]);
				this.screens[this.previousScreen].LoadScreen();
				base.Controls.Add(this.screens[this.previousScreen]);
				ScreenType screenType = this.previousScreen;
				this.previousScreen = this.currentScreen;
				this.currentScreen = screenType;
			}
		}

		private void wTimerServerCheck_Tick(object sender, EventArgs e)
		{
			if (!this.mEngineUp)
			{
				this.LoadServerData();
			}
		}
	}
}