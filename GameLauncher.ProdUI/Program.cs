using GameLauncher;
using GameLauncher.ProdUI.Properties;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace GameLauncher.ProdUI
{
	internal static class Program
	{
		private readonly static ILog mLogger;

		private static string GameDirRegistryKeyPath;

		private static string GameDirRegistryKeyName;

		private static string mCdnUrl;

		private static string mTermsOfService;

		private static string mUpdaterFile;

		private static string mApplicationDir;

		public static string AssemblyVersion
		{
			get
			{
				return Assembly.GetExecutingAssembly().GetName().Version.ToString();
			}
		}

		public static string CdnUrl
		{
			get
			{
				return Program.mCdnUrl;
			}
		}

		public static string TermsOfService
		{
			get
			{
				return Program.mTermsOfService;
			}
		}

		static Program()
		{
			Program.mLogger = LogManager.GetLogger(typeof(Program));
			Program.GameDirRegistryKeyPath = "HKEY_LOCAL_MACHINE\\software\\Electronic Arts\\Need For Speed World";
			Program.GameDirRegistryKeyName = "GameInstallDir";
			Program.mTermsOfService = "http://cdn.world.needforspeed.com/static/world/euala.txt";
			Program.mUpdaterFile = "GameLauncher.Updater.exe";
			Program.mApplicationDir = string.Empty;
		}

		private static string CalculateHash(string fileName)
		{
			string empty = string.Empty;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				using (SHA512 sHA512Managed = new SHA512Managed())
				{
					empty = Convert.ToBase64String(sHA512Managed.ComputeHash(fileStream));
					Program.mLogger.DebugFormat("Hash = '{0}'", empty);
				}
			}
			return empty;
		}

		private static string GetGameDirFromRegistry()
		{
			string value;
			try
			{
				value = (string)Registry.GetValue(Program.GameDirRegistryKeyPath, Program.GameDirRegistryKeyName, "");
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				Program.mLogger.Error(string.Concat("GetGameDirFromRegistryLocalMachine Exception: ", exception.ToString()));
				return "";
			}
			return value;
		}

		private static string GetLauncherInfo(string shard)
		{
			WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
			string str = webServicesWrapper.DoCall(shard, "/launcherinfo", null, null, RequestMethod.GET);
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.LoadXml(str);
			string empty = string.Empty;
			string innerText = string.Empty;
			foreach (XmlNode childNode in xmlDocument.FirstChild.ChildNodes)
			{
				string name = childNode.Name;
				string str1 = name;
				if (name == null)
				{
					continue;
				}
				if (str1 == "gameserver")
				{
					string innerText1 = childNode.InnerText;
				}
				else if (str1 == "cdn")
				{
					foreach (XmlNode xmlNodes in childNode.ChildNodes)
					{
						if (xmlNodes.Name == "game")
						{
							Program.mCdnUrl = xmlNodes.InnerText;
						}
						if (xmlNodes.Name != "launcher")
						{
							continue;
						}
						innerText = xmlNodes.InnerText;
					}
				}
				else if (str1 == "termsofservice")
				{
					foreach (XmlNode childNode1 in childNode.ChildNodes)
					{
						if (childNode1.Name.ToLower() != CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLower())
						{
							continue;
						}
						Program.mTermsOfService = childNode1.InnerText;
					}
				}
			}
			if (string.IsNullOrEmpty(Program.mCdnUrl))
			{
				Program.mLogger.Error("CDN Url not found");
				throw new Exception(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00062"));
			}
			return innerText;
		}

		private static bool HasAdministrativeRight()
		{
			return (new WindowsPrincipal(WindowsIdentity.GetCurrent())).IsInRole(WindowsBuiltInRole.Administrator);
		}

		private static bool HasEveryoneRight(string dirPath)
		{
			bool flag;
			SecurityIdentifier securityIdentifier = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
			DirectorySecurity accessControl = Directory.GetAccessControl(dirPath);
			AuthorizationRuleCollection accessRules = accessControl.GetAccessRules(true, true, typeof(SecurityIdentifier));
			IEnumerator enumerator = accessRules.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					if (((AuthorizationRule)enumerator.Current).IdentityReference != securityIdentifier)
					{
						continue;
					}
					flag = true;
					return flag;
				}
				return false;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return flag;
		}

		private static void InitializeUpdater()
		{
			Program.mApplicationDir = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\software\\Electronic Arts\\Need for Speed World", "GameInstallDir", "");
			Program.mUpdaterFile = Path.Combine(Program.mApplicationDir, Program.mUpdaterFile);
			int num = 10;
			while (File.Exists(Program.mUpdaterFile))
			{
				int num1 = num;
				num = num1 - 1;
				if (num1 <= 0)
				{
					break;
				}
				try
				{
					Program.mLogger.Debug(string.Concat("Deleting updater: ", Program.mUpdaterFile));
					File.SetAttributes(Program.mUpdaterFile, FileAttributes.Normal);
					File.Delete(Program.mUpdaterFile);
				}
				catch (Exception exception)
				{
					Program.mLogger.Debug(string.Concat("Exception trying to delete ", Program.mUpdaterFile));
					Thread.Sleep(500);
				}
			}
		}

		private static void LaunchInAdmin(string[] args)
		{
			ProcessStartInfo processStartInfo = new ProcessStartInfo()
			{
				Verb = "runas",
				FileName = Application.ExecutablePath
			};
			if ((int)args.Length > 0)
			{
				processStartInfo.Arguments = args[0];
			}
			try
			{
				Process.Start(processStartInfo);
				Program.mLogger.Info("Executing the game launcher with admin rights");
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				Program.mLogger.Fatal(string.Concat("Main Exception ", exception.ToString()));
			}
		}

		[STAThread]
		private static void Main(string[] args)
		{
			bool flag = false;
			try
			{
				bool flag1 = true;
				using (Mutex mutex = new Mutex(true, "GameLauncher", out flag1))
				{
					if (flag1)
					{
						if (!string.IsNullOrEmpty(Settings.Default.ForceLocale))
						{
							try
							{
								Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(Settings.Default.ForceLocale);
								Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(Settings.Default.ForceLocale);
							}
							catch (Exception exception)
							{
								Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
								Thread.CurrentThread.CurrentCulture = CultureInfo.CurrentCulture;
							}
						}
						else
						{
							Thread.CurrentThread.CurrentUICulture = CultureInfo.CurrentCulture;
							Thread.CurrentThread.CurrentCulture = CultureInfo.CurrentCulture;
						}
						Application.EnableVisualStyles();
						Application.SetCompatibleTextRenderingDefault(false);
						string gameDirFromRegistry = Program.GetGameDirFromRegistry();
						string currentDirectory = Environment.CurrentDirectory;
						if (string.IsNullOrEmpty(gameDirFromRegistry) || !Directory.Exists(gameDirFromRegistry))
						{
							if (Program.HasAdministrativeRight())
							{
								ChooseGameFolder chooseGameFolder = new ChooseGameFolder();
								Application.Run(chooseGameFolder);
								gameDirFromRegistry = chooseGameFolder.SelectedFolder;
								Directory.CreateDirectory(gameDirFromRegistry);
								if (string.IsNullOrEmpty(gameDirFromRegistry) || !Directory.Exists(gameDirFromRegistry))
								{
									Environment.Exit(1);
								}
								Program.SetGameDirFromRegistry(gameDirFromRegistry);
							}
							else
							{
								Program.LaunchInAdmin(args);
								return;
							}
						}
						if (!Program.HasEveryoneRight(gameDirFromRegistry))
						{
							if (Program.HasAdministrativeRight())
							{
								Program.SetEveryoneRight(gameDirFromRegistry);
							}
							else
							{
								Program.LaunchInAdmin(args);
								return;
							}
						}
						Environment.CurrentDirectory = gameDirFromRegistry;
						XmlConfigurator.Configure(Assembly.GetExecutingAssembly().GetManifestResourceStream("GameLauncher.ProdUI.Log4NetConfig.xml"));
						Hierarchy repository = (Hierarchy)LogManager.GetRepository();
						IAppender[] appenders = repository.GetAppenders();
						for (int i = 0; i < (int)appenders.Length; i++)
						{
							FileAppender fileAppender = appenders[i] as FileAppender;
							if (fileAppender != null)
							{
								fileAppender.File = Path.Combine(gameDirFromRegistry, "trace.log");
								fileAppender.ActivateOptions();
							}
						}
						flag = true;
						List<string> strs = new List<string>();
						string[] strArrays = args;
						for (int j = 0; j < (int)strArrays.Length; j++)
						{
							string str = strArrays[j];
							if (str.ToLower() != "/debug")
							{
								strs.Add(str);
							}
							else
							{
								repository.Root.Level = Level.Debug;
							}
						}
						if ((int)args.Length != strs.Count)
						{
							args = strs.ToArray();
						}
						try
						{
							if (File.Exists(Path.Combine(currentDirectory, "trace.log")))
							{
								File.SetAttributes(Path.Combine(currentDirectory, "trace.log"), FileAttributes.Normal);
								File.Delete(Path.Combine(currentDirectory, "trace.log"));
							}
						}
						catch (Exception exception1)
						{
							Program.mLogger.Debug("Cannot delete trace.log file in the local folder");
						}
						Configuration configuration = null;
						try
						{
							configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
						}
						catch (ConfigurationErrorsException configurationErrorsException1)
						{
							ConfigurationErrorsException configurationErrorsException = configurationErrorsException1;
							try
							{
								Program.mLogger.Error(configurationErrorsException.ToString());
								string filename = configurationErrorsException.Filename;
								if (filename != null && File.Exists(filename))
								{
									File.SetAttributes(filename, FileAttributes.Normal);
									File.Delete(filename);
								}
							}
							catch (Exception exception3)
							{
								Exception exception2 = exception3;
								Program.mLogger.Error("Exception when trying to delete corrupted config file");
								Program.mLogger.Error(string.Concat("Main Exception: ", exception2.ToString()));
							}
						}
						if (configuration == null)
						{
							configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
						}
						if (!File.Exists(configuration.FilePath))
						{
							try
							{
								Settings.Default.Upgrade();
								Settings.Default.Save();
							}
							catch (Exception exception5)
							{
								Exception exception4 = exception5;
								Program.mLogger.Warn("Failed to upgrade the settings from a previous version");
								Program.mLogger.Warn(string.Concat("Exception Program ", exception4.ToString()));
							}
						}
						string gameExecutable = Settings.Default.GameExecutable;
						char[] chrArray = new char[] { '.' };
						if ((int)Process.GetProcessesByName(gameExecutable.Split(chrArray)[0]).Length <= 0)
						{
							try
							{
								Program.mLogger.Info("--------------------------------------------------------------------------------");
								Program.mLogger.Info("");
								Program.mLogger.Info(string.Concat("Starting application: ", Assembly.GetExecutingAssembly().Location));
								Program.mLogger.Info(string.Concat("OS Version: ", Environment.OSVersion));
								Program.mLogger.Info(string.Concat("Locale: ", CultureInfo.CurrentCulture.Name));
								TimeZone currentTimeZone = TimeZone.CurrentTimeZone;
								DateTime now = DateTime.Now;
								Program.mLogger.InfoFormat("Time Zone: UTC{0} ({1})", currentTimeZone.GetUtcOffset(now), (currentTimeZone.IsDaylightSavingTime(now) ? currentTimeZone.DaylightName : currentTimeZone.StandardName));
								Program.mLogger.Info(string.Concat("Version: ", Assembly.GetExecutingAssembly().GetName().Version.ToString()));
								Program.mLogger.Info(string.Concat("Logging level: ", repository.Root.Level));
								Program.InitializeUpdater();
								string launcherInfo = null;
								foreach (string masterShardUrl in Settings.Default.MasterShardUrls)
								{
									try
									{
										launcherInfo = Program.GetLauncherInfo(masterShardUrl);
										break;
									}
									catch
									{
									}
								}
								if (launcherInfo == null)
								{
									throw new WebServicesWrapperHttpException();
								}
								if (Program.PatchGameLauncher(launcherInfo))
								{
									Application.Run(new GameLauncherUI(args));
									Program.mLogger.Info("Closing application");
								}
								else
								{
									return;
								}
							}
							catch (WebServicesWrapperHttpException webServicesWrapperHttpException1)
							{
								WebServicesWrapperHttpException webServicesWrapperHttpException = webServicesWrapperHttpException1;
								Program.mLogger.Error("Probably the server is down and we cannot patch");
								Program.mLogger.Error(string.Concat("Main Exception: ", webServicesWrapperHttpException.ToString()));                               
								MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00063"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00004"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
							catch (Exception exception9)
							{
								Exception exception8 = exception9;
								Program.mLogger.Error(string.Concat("Main Exception: ", exception8.ToString()));
								MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00008"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00004"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
						}
						else
						{
							Program.mLogger.Error("The game is running, exiting");
							MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00007"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00004"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
					else
					{
						return;
					}
				}
			}
			catch (ConfigurationErrorsException configurationErrorsException3)
			{
				ConfigurationErrorsException configurationErrorsException2 = configurationErrorsException3;
				if (flag)
				{
					Program.mLogger.Error(string.Concat("Main ConfigurationErrorsException: ", configurationErrorsException2.ToString()));
				}
				try
				{
					string empty = string.Empty;
					empty = ((ConfigurationErrorsException)configurationErrorsException2.InnerException == null ? configurationErrorsException2.Filename : ((ConfigurationErrorsException)configurationErrorsException2.InnerException).Filename);
					if (empty != null && File.Exists(empty))
					{
						File.SetAttributes(empty, FileAttributes.Normal);
						File.Delete(empty);
					}
				}
				catch (Exception exception11)
				{
					Exception exception10 = exception11;
					if (flag)
					{
						Program.mLogger.Error("Exception when trying to delete corrupted config file");
						Program.mLogger.Error(string.Concat("Main Exception: ", exception10.ToString()));
					}
				}
				MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00008"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00004"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			catch (Exception exception13)
			{
				Exception exception12 = exception13;
				if (flag)
				{
					Program.mLogger.Error(string.Concat("Main Exception: ", exception12.ToString()));
				}
				MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00008"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "PROGRAM00004"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static bool PatchGameLauncher(string gameLauncherCndUrl)
		{
			bool flag;
			XmlDocument xmlDocument = new XmlDocument();
			if (string.IsNullOrEmpty(gameLauncherCndUrl))
			{
				Program.mLogger.Error("GameLauncher Url not found");
				throw new Exception(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00062"));
			}
			try
			{
				try
				{
					xmlDocument.Load(string.Concat(gameLauncherCndUrl, "/DownloadInfo.xml"));
				}
				catch
				{
				}
				XmlNode xmlNodes = xmlDocument.SelectSingleNode("/update/application/version");
				XmlNode xmlNodes1 = xmlDocument.SelectSingleNode("/update/application/fileset");
				string str = Program.RemoveBuildFromVersion(Program.AssemblyVersion);
				bool flag1 = str != Program.RemoveBuildFromVersion(xmlNodes.InnerText);
				if (flag1)
				{
					Program.mLogger.InfoFormat("Versions do not match L-{0} != R-{1}", Program.AssemblyVersion, xmlNodes.InnerText);
				}
				else
				{
					string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
					try
					{
						foreach (XmlNode childNode in xmlNodes1.ChildNodes)
						{
							string value = childNode.Attributes["name"].Value;
							string str1 = Path.Combine(directoryName, value);
							if (File.Exists(str1))
							{
								string value1 = childNode.Attributes["hash"].Value;
								string str2 = Program.CalculateHash(str1);
								if (str2 == value1)
								{
									continue;
								}
								flag1 = true;
								Program.mLogger.InfoFormat("Hashes do not match for '{0}', L-{1} != R-{2}", value, str2, value1);
								break;
							}
							else
							{
								flag1 = true;
								Program.mLogger.InfoFormat("File {0} does not exists", value);
								break;
							}
						}
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						Program.mLogger.Error("a problem occurred while checking for the files integrity : assuming that self update is required");
						Program.mLogger.Error(string.Concat("Exception: ", exception.ToString()));
						flag1 = true;
					}
				}
				if (!flag1)
				{
					return true;
				}
				else
				{
					try
					{
						Program.mLogger.Info("Patching GameLauncher");
						if (File.Exists(Program.mUpdaterFile))
						{
							Program.mLogger.Debug(string.Concat("Deleting the updater ", Program.mUpdaterFile));
							File.SetAttributes(Program.mUpdaterFile, FileAttributes.Normal);
							File.Delete(Program.mUpdaterFile);
						}
						using (WebClient webClient = new WebClient())
						{
							Program.mLogger.DebugFormat("Downloading new updater from {0} to {1}", string.Concat(gameLauncherCndUrl, "/GameLauncher.Updater.exe"), Program.mUpdaterFile);
							webClient.DownloadFile(string.Concat(gameLauncherCndUrl, "/GameLauncher.Updater.exe"), Program.mUpdaterFile);
						}
						File.SetAttributes(Program.mUpdaterFile, FileAttributes.Normal);
						Program.mLogger.DebugFormat("Executing updater: {0} {1} \"{2}\"", Program.mUpdaterFile, gameLauncherCndUrl, Application.ExecutablePath);
						ProcessStartInfo processStartInfo = new ProcessStartInfo()
						{
							FileName = Program.mUpdaterFile,
							Arguments = string.Format("{0} \"{1}\"", gameLauncherCndUrl, Application.ExecutablePath),
							CreateNoWindow = true,
							UseShellExecute = true
						};
						Process.Start(processStartInfo);
					}
					catch (Exception exception3)
					{
						Exception exception2 = exception3;
						MessageBox.Show(ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00044"), ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00011"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
						Program.mLogger.Fatal("PatchGameLauncher Problem downloading or executing the updater");
						Program.mLogger.Fatal(string.Concat("PatchGameLauncher Exception: ", exception2.ToString()));
					}
					flag = false;
				}
			}
			catch (Exception exception5)
			{
				Exception exception4 = exception5;
				Program.mLogger.Fatal(string.Concat("Error loading/accessing file ", gameLauncherCndUrl, "/DownloadInfo.xml"));
				Program.mLogger.Error(string.Concat("PatchGameLauncher Exception: ", exception4.ToString()));
				flag = false;
			}
			return flag;
		}

		private static string RemoveBuildFromVersion(string version)
		{
			string[] strArrays = version.Split(new char[] { '.' });
			if ((int)strArrays.Length < 3)
			{
				return version;
			}
			return string.Concat(strArrays[0], strArrays[1], strArrays[2]);
		}

		private static void SetEveryoneRight(string dirPath)
		{
			SecurityIdentifier securityIdentifier = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
			DirectoryInfo directoryInfo = new DirectoryInfo(dirPath);
			DirectorySecurity accessControl = directoryInfo.GetAccessControl();
			accessControl.AddAccessRule(new FileSystemAccessRule(securityIdentifier, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
			directoryInfo.SetAccessControl(accessControl);
		}

		private static void SetGameDirFromRegistry(string gameInstallDir)
		{
			try
			{
				Registry.SetValue(Program.GameDirRegistryKeyPath, Program.GameDirRegistryKeyName, gameInstallDir);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				Program.mLogger.Error(string.Concat("SetGameDirFromRegistry Exception: ", exception.ToString()));
			}
		}
	}
}