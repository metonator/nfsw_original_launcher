using GameLauncher;
using GameLauncher.ProdUI.Controls;
using GameLauncher.ProdUI.Properties;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class DownloadScreen : BaseScreen
	{
		private bool isPlayButton;

		private bool isPlayButtonEnabled;

		private GameLauncherUI parentForm;

		private string remoteUserId = string.Empty;

		private string webAuthKey = string.Empty;

		private IContainer components;

		private Label wLabelDownloadProgress;

		private ProgressBar wProgressBar;

		private ExtendedWebBrowser wWebBrowser;

		private GlowingButton wPlayButton;

		private OptionsButton wOptionsButton;

		private ShardComboBox wShardComboBox;

		private Label wLabelShard;

		public DownloadScreen()
		{
			this.InitializeComponent();
		}

		public DownloadScreen(GameLauncherUI launcherForm) : base(launcherForm)
		{
			this.parentForm = launcherForm;
			this.InitializeComponent();
			this.LocalizeFE();
			this.InitializeSettings();
			this.ApplyEmbeddedFonts();
		}

		protected override void ApplyEmbeddedFonts()
		{
			FontFamily fontFamily = FontWrapper.Instance.GetFontFamily("MyriadProSemiCondBold.ttf");
			FontFamily fontFamily1 = FontWrapper.Instance.GetFontFamily("Reg-B-I.ttf");
			this.wPlayButton.wLabelButton.Font = new System.Drawing.Font(fontFamily1, 18.75f, FontStyle.Bold | FontStyle.Italic);
			this.wLabelDownloadProgress.Font = new System.Drawing.Font(fontFamily, 9.749999f, FontStyle.Bold);
			this.wLabelShard.Font = new System.Drawing.Font(fontFamily, 9.749999f, FontStyle.Bold);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void HandleWebBrowserDocumentLoaded(object sender, WebBrowserDocumentCompletedEventArgs args)
		{
			if ((double)this.mDpi > 96)
			{
				this.wWebBrowser.Document.Body.Style = string.Concat("zoom:", (int)((double)this.mDpi / 96 * 100), "%");
			}
		}

		private void InitializeComponent()
		{
			this.wLabelDownloadProgress = new Label();
			this.wProgressBar = new ProgressBar();
			this.wWebBrowser = new ExtendedWebBrowser();
			this.wPlayButton = new GlowingButton();
			this.wOptionsButton = new OptionsButton();
			this.wShardComboBox = new ShardComboBox();
			this.wLabelShard = new Label();
			base.SuspendLayout();
			this.wLabelDownloadProgress.BackColor = Color.Transparent;
			this.wLabelDownloadProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wLabelDownloadProgress.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLabelDownloadProgress.Location = new Point(17, 457);
			this.wLabelDownloadProgress.Name = "wLabelDownloadProgress";
			this.wLabelDownloadProgress.Size = new System.Drawing.Size(480, 15);
			this.wLabelShard.TabStop = false;
			this.wLabelDownloadProgress.Text = "DOWNLOAD WAITING...";
			this.wProgressBar.BackColor = Color.FromArgb(175, 197, 220);
			this.wProgressBar.ForeColor = Color.FromArgb(76, 178, 255);
			this.wProgressBar.Location = new Point(20, 436);
			this.wProgressBar.Name = "wProgressBar";
			this.wProgressBar.Size = new System.Drawing.Size(580, 18);
			this.wProgressBar.Style = ProgressBarStyle.Continuous;
			this.wLabelShard.TabStop = false;
			this.wWebBrowser.AllowWebBrowserDrop = false;
			this.wWebBrowser.IsWebBrowserContextMenuEnabled = false;
			this.wWebBrowser.Location = new Point(10, 95);
			this.wWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
			this.wWebBrowser.Name = "wWebBrowser";
			this.wWebBrowser.ScriptErrorsSuppressed = true;
			this.wWebBrowser.ScrollBarsEnabled = false;
			this.wWebBrowser.Size = new System.Drawing.Size(771, 310);
			this.wWebBrowser.TabStop = false;
			this.wWebBrowser.Url = new Uri("", UriKind.Relative);
			this.wWebBrowser.Visible = false;
			this.wWebBrowser.WebBrowserShortcutsEnabled = false;
			this.wWebBrowser.NewWindow2 += new EventHandler<NewWindow2EventArgs>(this.wWebBrowser_NewWindow2);
			this.wPlayButton.AutoSize = true;
			this.wPlayButton.BackColor = Color.Transparent;
			this.wPlayButton.Location = new Point(618, 424);
			this.wPlayButton.Name = "wPlayButton";
			this.wPlayButton.Size = new System.Drawing.Size(168, 58);
			this.wPlayButton.TabIndex = 1;
			this.wPlayButton.PreviewKeyDown += new PreviewKeyDownEventHandler(this.wPlayButton_PreviewKeyDown);
			this.wOptionsButton.AutoSize = true;
			this.wOptionsButton.BackColor = Color.Transparent;
			this.wOptionsButton.Location = new Point(741, 44);
			this.wOptionsButton.MinimumSize = new System.Drawing.Size(31, 29);
			this.wOptionsButton.Name = "wOptionsButton";
			this.wOptionsButton.Size = new System.Drawing.Size(40, 29);
			this.wOptionsButton.TabIndex = 3;
			this.wOptionsButton.PreviewKeyDown += new PreviewKeyDownEventHandler(this.wOptionsButton_PreviewKeyDown);
			this.wShardComboBox.AutoSize = true;
			this.wShardComboBox.BackColor = Color.Transparent;
			this.wShardComboBox.Location = new Point(548, 47);
			this.wShardComboBox.Name = "wShardComboBox";
			this.wShardComboBox.Size = new System.Drawing.Size(188, 26);
			this.wShardComboBox.TabIndex = 2;
			this.wLabelShard.BackColor = Color.Transparent;
			this.wLabelShard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999f, FontStyle.Bold);
			this.wLabelShard.ForeColor = Color.White;
			this.wLabelShard.ImageAlign = ContentAlignment.MiddleRight;
			this.wLabelShard.Location = new Point(376, 51);
			this.wLabelShard.Name = "wLabelShard";
			this.wLabelShard.Size = new System.Drawing.Size(171, 15);
			this.wLabelShard.TabStop = false;
			this.wLabelShard.Text = "SELECT SHARD:";
			this.wLabelShard.TextAlign = ContentAlignment.MiddleRight;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackgroundImage = Resources.nfsw_lp_96dpi_bg_s2_w_1bit_alpha;
			base.Controls.Add(this.wLabelShard);
			base.Controls.Add(this.wShardComboBox);
			base.Controls.Add(this.wOptionsButton);
			base.Controls.Add(this.wPlayButton);
			base.Controls.Add(this.wWebBrowser);
			base.Controls.Add(this.wProgressBar);
			base.Controls.Add(this.wLabelDownloadProgress);
			base.Name = "DownloadScreen";
			base.Controls.SetChildIndex(this.wLabelDownloadProgress, 0);
			base.Controls.SetChildIndex(this.wProgressBar, 0);
			base.Controls.SetChildIndex(this.wWebBrowser, 0);
			base.Controls.SetChildIndex(this.wPlayButton, 0);
			base.Controls.SetChildIndex(this.wOptionsButton, 0);
			base.Controls.SetChildIndex(this.wShardComboBox, 0);
			base.Controls.SetChildIndex(this.wLabelShard, 0);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		protected override void InitializeSettings()
		{
			this.backgrounds = new Dictionary<double, Image>()
			{
				{ 96, Resources.nfsw_lp_96dpi_bg_s2_w_1bit_alpha },
				{ 120, Resources.nfsw_lp_120dpi_bg_s2_w_1bit_alpha },
				{ 144, Resources.nfsw_lp_144dpi_bg_s2_w_1bit_alpha }
			};
			this.BackgroundImage = base.SelectBackgroundImage();
			this.wLabelDownloadProgress.Visible = false;
			this.wProgressBar.Visible = false;
			if (!string.IsNullOrEmpty(this.parentForm.CommandArgShard))
			{
				this.wShardComboBox.SetSelectedValue(this.parentForm.CommandArgShard);
			}
			this.wPlayButton.wLabelButton.Click += new EventHandler(this.wLabelButton_Click);
			this.wOptionsButton.wLabelOptions.Click += new EventHandler(this.wOptionsButton_Click);
			this.parentForm.DownloadStarted = new DownloadStarted(this.OnDownloadStarted);
			this.parentForm.DownloadProgressUpdated = new DownloadProgressUpdated(this.OnDownloadProgressUpdated);
			this.parentForm.DownloadFinished = new DownloadFinished(this.OnDownloadFinished);
			this.parentForm.LoginFinished = new LoginFinished(this.OnLoginFinished);
			this.parentForm.UpdateStarted = new UpdateStarted(this.OnUpdateStarted);
		}

		public override void LoadScreen()
		{
			base.LoadScreen();
			this.wShardComboBox.SetSelectedValue();
			this.wShardComboBox.ShardUrlChanged = new ShardUrlChanged(this.OnShardUrlChanged);
			this.wShardComboBox.ShardRegionChanged = new ShardRegionChanged(this.OnShardRegionChanged);
			this.parentForm.LoadServerStatusFinished = new LoadServerStatusFinished(this.LoadServerStatusFinished);
			this.parentForm.StartDownload();
			base.ActiveControl = this.wShardComboBox;
			this.Cursor = Cursors.Default;
		}

		private void LoadServerStatusFinished(bool serverUp, string statusMessage)
		{
			if (!string.IsNullOrEmpty(this.remoteUserId) && !string.IsNullOrEmpty(this.webAuthKey))
			{
				this.LoadWebStore(this.remoteUserId, this.webAuthKey);
			}
		}

		private void LoadWebStore(string remoteUserId, string webAuthKey)
		{
			this.remoteUserId = remoteUserId;
			this.webAuthKey = webAuthKey;
			if (!this.parentForm.PortalUp)
			{
				return;
			}
			string shardUrl = ShardManager.ShardUrl;
			string[] strArrays = shardUrl.Split(new char[] { '/' });
			if ((int)strArrays.Length > 3)
			{
				shardUrl = string.Concat(strArrays[2], "/", strArrays[3]);
			}
			this.wWebBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(this.HandleWebBrowserDocumentLoaded);
			ExtendedWebBrowser extendedWebBrowser = this.wWebBrowser;
			Uri uri = new Uri(string.Concat(this.parentForm.PortalDomain, "/webkit/lp/store?locale=", CultureInfo.CurrentCulture.Name.Replace('-', '\u005F')));
			object[] objArray = new object[] { remoteUserId, webAuthKey, ShardManager.ShardName, shardUrl, Environment.NewLine, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; MS-RTC LM 8; .NET4.0C; .NET4.0E)" };
			extendedWebBrowser.Navigate(uri, "", null, string.Format("userId: {0}{4}token: {1}{4}shard: {2}{4}worldserverurl: {3}{4}User-Agent: {5}", objArray));
			this.wWebBrowser.Visible = true;
			remoteUserId = string.Empty;
			webAuthKey = string.Empty;
		}

		protected override void LocalizeFE()
		{
			this.wLabelShard.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00072");
			this.wLabelDownloadProgress.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00017");
			this.wPlayButton.wLabelButton.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00041");
		}

		private void OnDownloadFinished()
		{
			this.SwitchPlayButtonToPlay();
		}

		private void OnDownloadProgressUpdated(bool progressVisible, int progressValue, string progressText)
		{
			ProgressBar progressBar = this.wProgressBar;
			bool flag = progressVisible;
			bool flag1 = flag;
			this.wLabelDownloadProgress.Visible = flag;
			progressBar.Visible = flag1;
			this.wProgressBar.Value = progressValue;
			if (!string.IsNullOrEmpty(progressText))
			{
				this.wLabelDownloadProgress.Text = progressText;
			}
		}

		private void OnDownloadStarted()
		{
			this.SwitchPlayButtonToDownloading();
			this.OnDownloadProgressUpdated(true, 0, ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00017"));
		}

		private void OnLoginFinished(string userId, string webAuthKey)
		{
			this.LoadWebStore(userId, webAuthKey);
		}

		private void OnShardRegionChanged()
		{
			if (base.IsActive)
			{
				this.Cursor = Cursors.WaitCursor;
				this.parentForm.SetRegion();
				this.Cursor = Cursors.Default;
			}
		}

		private void OnShardUrlChanged()
		{
			if (base.IsActive)
			{
				this.parentForm.PerformLogout();
				this.parentForm.LoadServerData();
				this.parentForm.SwitchScreen(ScreenType.Login);
			}
		}

		private void OnUpdateStarted()
		{
			this.SwitchPlayButtonToUpdate();
		}

		private void SwitchPlayButtonToDownloading()
		{
			string str = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00041");
			this.wPlayButton.DisableButton(str);
			this.isPlayButton = true;
			this.isPlayButtonEnabled = false;
			this.wLabelDownloadProgress.Visible = true;
			this.wProgressBar.Value = 0;
			this.wProgressBar.Visible = true;
		}

		private void SwitchPlayButtonToPlay()
		{
			string str = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHER00041");
			this.wPlayButton.EnableButton(str);
			this.isPlayButton = true;
			this.isPlayButtonEnabled = true;
			this.wProgressBar.Value = 0;
			this.wProgressBar.Visible = true;
		}

		private void SwitchPlayButtonToUpdate()
		{
			string str = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00027");
			this.wPlayButton.EnableButton(str);
			this.isPlayButton = false;
			this.isPlayButtonEnabled = true;
			this.wLabelDownloadProgress.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "GAMELAUNCHERDESIGNER00001");
			this.wLabelDownloadProgress.Visible = true;
			this.wProgressBar.Value = 0;
			this.wProgressBar.Visible = true;
		}

		private void wLabelButton_Click(object sender, EventArgs e)
		{
			if (this.isPlayButtonEnabled)
			{
				this.Cursor = Cursors.WaitCursor;
				if (!this.isPlayButton)
				{
					this.parentForm.PerformUpdate();
				}
				else
				{
					this.parentForm.PerformPlay();
				}
				this.Cursor = Cursors.Default;
			}
		}

		private void wOptionsButton_Click(object sender, EventArgs e)
		{
			this.parentForm.SwitchScreen(ScreenType.Options);
		}

		private void wOptionsButton_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				this.wOptionsButton.ButtonPressed(true);
				this.wOptionsButton.Update();
				this.wOptionsButton_Click(null, null);
				this.wOptionsButton.ButtonPressed(false);
				this.wOptionsButton.Update();
			}
		}

		private void wPlayButton_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (e.KeyCode == Keys.Return && this.isPlayButtonEnabled)
			{
				this.wPlayButton.ButtonPressed(true);
				this.wPlayButton.Update();
				this.wLabelButton_Click(null, null);
				this.wPlayButton.ButtonPressed(false);
				this.wPlayButton.Update();
			}
		}

		private void wWebBrowser_NewWindow2(object sender, NewWindow2EventArgs e)
		{
			e.Cancel = true;
			if (string.IsNullOrEmpty(this.parentForm.WebApplicationData[0]))
			{
				return;
			}
			string empty = string.Empty;
			try
			{
				empty = this.parentForm.WebApplicationData[1].Replace("%1", this.wWebBrowser.StatusText);
				Process.Start(this.parentForm.WebApplicationData[0], empty);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				GameLauncherUI.Logger.ErrorFormat("Problem running {0} {1}", this.parentForm.WebApplicationData[0], empty);
				GameLauncherUI.Logger.Error(string.Concat("wWebBrowser_NewWindow2 Exception: ", exception.ToString()));
			}
		}
	}
}