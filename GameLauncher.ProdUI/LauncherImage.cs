using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class LauncherImage
	{
		private float mDpi;

		public LauncherImage(float mDpi)
		{
			this.mDpi = mDpi;
		}

		public static Image DownloadImage(string link)
		{
			Image image;
			using (WebClient webClient = new WebClient())
			{
				using (MemoryStream memoryStream = new MemoryStream(webClient.DownloadData(link)))
				{
					image = Image.FromStream(memoryStream);
				}
			}
			return image;
		}

		public Image ResizeImage(Image sourceImage, float baseDpi)
		{
			Image bitmap;
			if (this.mDpi <= baseDpi)
			{
				bitmap = sourceImage;
			}
			else
			{
				float single = this.mDpi / baseDpi;
				bitmap = new Bitmap((int)((float)sourceImage.Width * single), (int)((float)sourceImage.Height * single));
				using (Graphics graphic = Graphics.FromImage(bitmap))
				{
					graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
					graphic.DrawImage(sourceImage, 0f, 0f, (float)sourceImage.Width * single, (float)sourceImage.Height * single);
				}
			}
			return bitmap;
		}

		public Image ResizeImage(Image sourceImage)
		{
			return this.ResizeImage(sourceImage, 96f);
		}

		public ImageList ResizeImageList(ImageList sourceImageList, float baseDpi)
		{
			ImageList imageList = new ImageList();
			foreach (Image image in sourceImageList.Images)
			{
				imageList.Images.Add(this.ResizeImage(image, baseDpi));
			}
			return imageList;
		}

		public static Image SetImgOpacity(Image imgPic, float imgOpac)
		{
			Bitmap bitmap = new Bitmap(imgPic.Width, imgPic.Height);
			Graphics graphic = Graphics.FromImage(bitmap);
			ColorMatrix colorMatrix = new ColorMatrix()
			{
				Matrix33 = imgOpac
			};
			ImageAttributes imageAttribute = new ImageAttributes();
			imageAttribute.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
			graphic.DrawImage(imgPic, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, imgPic.Width, imgPic.Height, GraphicsUnit.Pixel, imageAttribute);
			graphic.Dispose();
			return bitmap;
		}
	}
}