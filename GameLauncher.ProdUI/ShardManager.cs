using GameLauncher;
using GameLauncher.ProdUI.Properties;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Xml;

namespace GameLauncher.ProdUI
{
	public class ShardManager
	{
		private static Dictionary<string, ShardInfo> shards;

		public static string ShardKey
		{
			get
			{
				return Settings.Default.Shard;
			}
			set
			{
				if (ShardManager.shards.ContainsKey(value))
				{
					Settings.Default.Shard = value;
					Settings.Default.Save();
				}
			}
		}

		public static string ShardName
		{
			get
			{
				return ShardManager.shards[Settings.Default.Shard].ShardName;
			}
		}

		public static string ShardRegion
		{
			get
			{
				return ShardManager.shards[Settings.Default.Shard].RegionName;
			}
		}

		public static int ShardRegionId
		{
			get
			{
				return ShardManager.shards[Settings.Default.Shard].RegionId;
			}
		}

		public static Dictionary<string, ShardInfo> Shards
		{
			get
			{
				return ShardManager.shards;
			}
		}

		public static string ShardUrl
		{
			get
			{
				return ShardManager.shards[Settings.Default.Shard].Url;
			}
		}

		static ShardManager()
		{
			ShardManager.shards = new Dictionary<string, ShardInfo>();
			ShardManager.GetShardData();
		}

		public ShardManager()
		{
		}

		private static void GetShardData()
		{
			int num;
			string empty = string.Empty;
			ShardManager.shards = new Dictionary<string, ShardInfo>();
			foreach (string masterShardUrl in Settings.Default.MasterShardUrls)
			{
				try
				{
					WebServicesWrapper webServicesWrapper = new WebServicesWrapper();
					string str = webServicesWrapper.DoCall(masterShardUrl, "/getshardinfo", null, null, RequestMethod.GET);
					XmlDocument xmlDocument = new XmlDocument();
					xmlDocument.LoadXml(str);
					foreach (XmlNode childNode in xmlDocument.FirstChild.ChildNodes)
					{
						if (childNode.Name != "ShardInfo")
						{
							continue;
						}
						int.TryParse(childNode["RegionId"].InnerText, out num);
						ShardInfo shardInfo = new ShardInfo()
						{
							RegionId = num,
							RegionName = childNode["RegionName"].InnerText,
							ShardName = childNode["ShardName"].InnerText,
							Url = childNode["Url"].InnerText
						};
						ShardManager.shards.Add(shardInfo.ShardKey, shardInfo);
						if (!string.IsNullOrEmpty(empty))
						{
							continue;
						}
						empty = shardInfo.ShardKey;
					}
				}
				catch (Exception exception)
				{

                }
			}
			if (ShardManager.shards.Count == 0)
			{
				GameLauncherUI.Logger.Error("GetShardData: no master shards are up");
				throw new WebServicesWrapperHttpException();
			}
			if (!ShardManager.shards.ContainsKey(Settings.Default.Shard))
			{
				Settings.Default.Shard = empty;
			}
		}
	}
}