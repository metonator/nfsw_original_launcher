using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class SSOLoginScreen : BaseScreen
	{
		private GameLauncherUI parentForm;

		private IContainer components;

		public SSOLoginScreen(GameLauncherUI launcherForm) : base(launcherForm)
		{
			this.parentForm = launcherForm;
			this.InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
		}

		public override void LoadScreen()
		{
			this.PerformLogin();
		}

		public void PerformLogin()
		{
			bool flag = false;
			string environmentVariable = Environment.GetEnvironmentVariable("EAGenericAuthToken");
			if (environmentVariable != null)
			{
				this.parentForm.LoadServerData();
				if (!this.parentForm.SingleSignOnLogin(environmentVariable))
				{
					MessageBox.Show("Login expired, please re-login to run the game.", "SSO error");
				}
				else
				{
					flag = true;
				}
			}
			if (!flag)
			{
				this.parentForm.ReplaceScreen(ScreenType.Login, new LoginScreen(this.parentForm));
			}
		}
	}
}