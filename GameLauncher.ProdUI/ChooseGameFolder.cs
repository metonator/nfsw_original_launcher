using GameLauncher.ProdUI.Screens;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class ChooseGameFolder : Form
	{
		private IContainer components;

		private GameFolder wGameFolderScreen;

		public string SelectedFolder
		{
			get
			{
				return this.wGameFolderScreen.SelectedFolder;
			}
		}

		public ChooseGameFolder()
		{
			this.InitializeComponent();
			this.wGameFolderScreen = new GameFolder(this);
			base.Controls.Add(this.wGameFolderScreen);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(ChooseGameFolder));
			base.SuspendLayout();
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackColor = Color.White;
			base.ClientSize = new System.Drawing.Size(790, 490);
			this.ForeColor = Color.FromArgb(51, 153, 255);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			base.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(790, 490);
			base.Name = "ChooseGameFolderUI";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "Need for Speed™ World";
			base.TransparencyKey = Color.Red;
			base.ResumeLayout(false);
		}
	}
}