using log4net;
using log4net.Config;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class CustomMessageBox : Form
	{
		private const int SC_CLOSE = 61536;

		private const int MF_GRAYED = 1;

		private IContainer components;

		private Button wButton1;

		private PictureBox wPictureBoxIcon;

		private Button wButton2;

		private Panel panel1;

		private string _link = string.Empty;

		private CustomMessageBox.CustomMessageBoxButtons _buttons;

		private int MinLabelY = 22;

		private int MaxLabelX = 358;

		private int MinDialogX = 123;

		private int MaxDialogX = 407;

		private int MinDialogY = 145;

		private int ButtonSeparation = 8;

		private int ButtonEdgeSeparation = 13;

		private int LabelAdjustX = 87;

		private int LabelAdjustY = 113;

		private LinkLabel wLinkLabelText;

		private readonly static ILog mLogger;

		static CustomMessageBox()
		{
			CustomMessageBox.mLogger = LogManager.GetLogger(typeof(CustomMessageBox));
		}

		public CustomMessageBox(string text, string caption, CustomMessageBox.CustomMessageBoxButtons buttons, CustomMessageBox.CustomMessageBoxIcon icon)
		{
			BasicConfigurator.Configure();
			this.InitializeComponent();
			this.wLinkLabelText = new LinkLabel()
			{
				Font = SystemFonts.DialogFont,
				Location = new Point(60, 33),
				Name = "wLinkLabelText",
				Size = new System.Drawing.Size(88, 13),
				TabIndex = 3,
				TabStop = true
			};
			base.Controls.Add(this.wLinkLabelText);
			this.AdjustGraphicConstantsByDpi();
			this.Text = caption;
			this.wLinkLabelText.Text = text;
			this.wLinkLabelText.Font = SystemFonts.DialogFont;
			this.wPictureBoxIcon.Image = this.MapIcon(icon);
			this._buttons = buttons;
			this.SetButtonsBehavior();
		}

		private void AdjustGraphicConstantsByDpi()
		{
			using (Graphics graphic = base.CreateGraphics())
			{
				float dpiX = graphic.DpiX / 96f;
				this.MinLabelY = (int)((float)this.MinLabelY * dpiX);
				this.MaxLabelX = (int)((float)this.MaxLabelX * dpiX);
				this.MinDialogX = (int)((float)this.MinDialogX * dpiX);
				this.MaxDialogX = (int)((float)this.MaxDialogX * dpiX);
				this.MinDialogY = (int)((float)this.MinDialogY * dpiX);
				this.ButtonSeparation = (int)((float)this.ButtonSeparation * dpiX);
				this.ButtonEdgeSeparation = (int)((float)this.ButtonEdgeSeparation * dpiX);
				this.LabelAdjustX = (int)((float)this.LabelAdjustX * dpiX);
				this.LabelAdjustY = (int)((float)this.LabelAdjustY * dpiX);
			}
		}

		private void CustomMessageBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control && e.KeyCode == Keys.C || e.Control && e.KeyCode == Keys.Insert)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.AppendLine("---------------------------");
				stringBuilder.AppendLine(this.Text);
				stringBuilder.AppendLine("---------------------------");
				stringBuilder.AppendLine(this.wLinkLabelText.Text);
				stringBuilder.AppendLine("---------------------------");
				if (this.wButton1.Visible)
				{
					stringBuilder.Append(string.Concat(this.wButton1.Text, " "));
				}
				stringBuilder.AppendLine(this.wButton2.Text);
				stringBuilder.AppendLine("---------------------------");
				Clipboard.SetDataObject(stringBuilder.ToString());
			}
		}

		private void CustomMessageBox_Load(object sender, EventArgs e)
		{
			try
			{
				string text = this.wLinkLabelText.Text;
				CustomMessageBox.LinkDetection linkDetection = this.DetectLinks(text);
				if (!linkDetection.LinkDetected)
				{
					this.wLinkLabelText.LinkArea = new LinkArea(0, 0);
				}
				else
				{
					this.wLinkLabelText.LinkArea = linkDetection.Area;
					int start = linkDetection.Area.Start;
					LinkArea area = linkDetection.Area;
					this._link = text.Substring(start, area.Length);
					this.wLinkLabelText.LinkClicked += new LinkLabelLinkClickedEventHandler(this.wLinkLabelText_LinkClicked);
				}
				if (!string.IsNullOrEmpty(text))
				{
					using (Graphics graphic = base.CreateGraphics())
					{
						System.Drawing.Font dialogFont = SystemFonts.DialogFont;
						RectangleF rectangleF = new RectangleF(0f, 0f, (float)this.MaxLabelX, 1000f);
						CharacterRange[] characterRange = new CharacterRange[] { new CharacterRange(0, text.Length) };
						CharacterRange[] characterRangeArray = characterRange;
						StringFormat stringFormat = new StringFormat()
						{
							FormatFlags = StringFormatFlags.NoClip
						};
						stringFormat.SetMeasurableCharacterRanges(characterRangeArray);
						System.Drawing.Region[] regionArray = graphic.MeasureCharacterRanges(text, dialogFont, rectangleF, stringFormat);
						float height = regionArray[0].GetBounds(graphic).Height;
						CharacterRange[] characterRange1 = new CharacterRange[] { new CharacterRange(0, 1) };
						StringFormat stringFormat1 = new StringFormat()
						{
							FormatFlags = StringFormatFlags.NoClip
						};
						stringFormat1.SetMeasurableCharacterRanges(characterRange1);
						System.Drawing.Region[] regionArray1 = graphic.MeasureCharacterRanges("a", dialogFont, rectangleF, stringFormat1);
						float single = regionArray1[0].GetBounds(graphic).Height;
						if (height > single)
						{
							LinkLabel size = this.wLinkLabelText;
							RectangleF bounds = regionArray[0].GetBounds(graphic);
							size.Size = new System.Drawing.Size((int)bounds.Width, (int)height);
						}
						else
						{
							this.wLinkLabelText.AutoSize = true;
						}
						LinkLabel point = this.wLinkLabelText;
						int x = this.wLinkLabelText.Location.X;
						int minLabelY = this.MinLabelY;
						Point location = this.wLinkLabelText.Location;
						point.Location = new Point(x, Math.Max(minLabelY, location.Y - (int)((height / single - 1f) * this.wLinkLabelText.Font.Size)));
					}
				}
				int width = this.wLinkLabelText.Size.Width + this.LabelAdjustX;
				int num = this.wLinkLabelText.Size.Height + this.LabelAdjustY;
				base.Size = new System.Drawing.Size((width < this.MinDialogX ? this.MinDialogX : width), (num < this.MinDialogY ? this.MinDialogY : num));
				Button button = this.wButton2;
				int width1 = base.Width - this.ButtonEdgeSeparation - this.wButton2.Width;
				Point location1 = this.wButton2.Location;
				button.Location = new Point(width1, location1.Y);
				if (this.wButton1.Visible)
				{
					Button point1 = this.wButton1;
					int num1 = base.Width - this.ButtonEdgeSeparation - this.ButtonSeparation - this.wButton1.Width - this.wButton2.Width;
					Point location2 = this.wButton1.Location;
					point1.Location = new Point(num1, location2.Y);
					CustomMessageBox.UnsafeNativeMethods.EnableMenuItem(CustomMessageBox.UnsafeNativeMethods.GetSystemMenu(base.Handle, false), 61536, 1);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CustomMessageBox.mLogger.Error(string.Concat("CustomMessageBox_Load Exception: ", exception.ToString()));
			}
		}

		private CustomMessageBox.LinkDetection DetectLinks(string text)
		{
			LinkArea linkArea = new LinkArea(0, text.Length);
			bool flag = false;
			Regex regex = new Regex("(https?://\\S+[\\w\\d/])");
			string empty = string.Empty;
			int num = text.IndexOf("http://");
			if (num == -1)
			{
				num = text.IndexOf("https://");
				if (num != -1)
				{
					Match match = regex.Match(text.Substring(num));
					if (match.Success)
					{
						linkArea.Start = num;
						linkArea.Length = match.Groups[0].Length;
						flag = true;
					}
				}
			}
			else
			{
				Match match1 = regex.Match(text.Substring(num));
				if (match1.Success)
				{
					linkArea.Start = num;
					linkArea.Length = match1.Groups[0].Length;
					flag = true;
				}
			}
			return new CustomMessageBox.LinkDetection(linkArea, flag);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.wButton1 = new Button();
			this.wPictureBoxIcon = new PictureBox();
			this.wButton2 = new Button();
			this.panel1 = new Panel();
			((ISupportInitialize)this.wPictureBoxIcon).BeginInit();
			this.panel1.SuspendLayout();
			base.SuspendLayout();
			this.wButton1.BackColor = SystemColors.ButtonFace;
			this.wButton1.Location = new Point(34, 10);
			this.wButton1.Name = "wButton1";
			this.wButton1.Size = new System.Drawing.Size(74, 22);
			this.wButton1.TabIndex = 0;
			this.wButton1.Text = "1";
			this.wButton1.UseVisualStyleBackColor = false;
			this.wButton1.Click += new EventHandler(this.wButton1_Click);
			this.wPictureBoxIcon.Location = new Point(21, 22);
			this.wPictureBoxIcon.Name = "wPictureBoxIcon";
			this.wPictureBoxIcon.Size = new System.Drawing.Size(32, 32);
			this.wPictureBoxIcon.TabIndex = 1;
			this.wPictureBoxIcon.TabStop = false;
			this.wButton2.BackColor = SystemColors.ButtonFace;
			this.wButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.wButton2.Location = new Point(116, 10);
			this.wButton2.Name = "wButton2";
			this.wButton2.Size = new System.Drawing.Size(74, 22);
			this.wButton2.TabIndex = 2;
			this.wButton2.Text = "2";
			this.wButton2.UseVisualStyleBackColor = false;
			this.wButton2.Click += new EventHandler(this.wButton2_Click);
			this.panel1.BackColor = SystemColors.Control;
			this.panel1.Controls.Add(this.wButton2);
			this.panel1.Controls.Add(this.wButton1);
			this.panel1.Dock = DockStyle.Bottom;
			this.panel1.Location = new Point(0, 76);
			this.panel1.MinimumSize = new System.Drawing.Size(413, 42);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(413, 42);
			this.panel1.TabIndex = 3;
			base.AcceptButton = this.wButton1;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.BackColor = SystemColors.Window;
			base.CancelButton = this.wButton2;
			base.ClientSize = new System.Drawing.Size(405, 118);
			base.Controls.Add(this.wPictureBoxIcon);
			base.Controls.Add(this.panel1);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			base.KeyPreview = true;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "CustomMessageBox";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			base.StartPosition = FormStartPosition.CenterParent;
			this.Text = "CustomMessageBox";
			base.Load += new EventHandler(this.CustomMessageBox_Load);
			base.KeyDown += new KeyEventHandler(this.CustomMessageBox_KeyDown);
			((ISupportInitialize)this.wPictureBoxIcon).EndInit();
			this.panel1.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		private Image MapIcon(CustomMessageBox.CustomMessageBoxIcon index)
		{
			IntPtr intPtr;
			Image bitmap;
			try
			{
				IntPtr intPtr1 = (IntPtr)((long)index);
				IntPtr intPtr2 = CustomMessageBox.UnsafeNativeMethods.ExtractAssociatedIconEx(base.Handle, Path.Combine(Environment.SystemDirectory, "user32.dll"), ref intPtr1, out intPtr);
				bitmap = System.Drawing.Icon.FromHandle(intPtr2).ToBitmap();
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CustomMessageBox.mLogger.Error(string.Concat("MapIcon Exception: ", exception.ToString()));
				return null;
			}
			return bitmap;
		}

		private string[] RetrieveWebApplicationData()
		{
			string[] strArrays = new string[2];
			string value = (string)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\Shell\\Associations\\UrlAssociations\\http\\UserChoice", "Progid", string.Empty);
			string str = (string)Registry.GetValue(string.Format("HKEY_CURRENT_USER\\Software\\Classes\\{0}\\shell\\open\\command", value), "", string.Empty);
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue(string.Format("HKEY_CLASSES_ROOT\\{0}\\shell\\open\\command", value), "", string.Empty);
			}
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Classes\\http\\shell\\open\\command", "", string.Empty);
			}
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue("HKEY_CLASSES_ROOT\\http\\shell\\open\\command", "", string.Empty);
			}
			if (string.IsNullOrEmpty(str))
			{
				str = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Applications\\iexplore.exe\\shell\\open\\command", "", string.Empty);
			}
			if (string.IsNullOrEmpty(str))
			{
				string environmentVariable = Environment.GetEnvironmentVariable("ProgramFiles(x86)");
				if (string.IsNullOrEmpty(environmentVariable))
				{
					environmentVariable = Environment.GetEnvironmentVariable("ProgramFiles");
				}
				string str1 = Path.Combine(environmentVariable, "Internet Explorer\\IEXPLORE.EXE");
				if (!File.Exists(str1))
				{
					return strArrays;
				}
				strArrays[0] = str1;
			}
			string[] strArrays1 = str.Split(new char[] { '\"' });
			if ((int)strArrays1.Length > 1)
			{
				strArrays[0] = strArrays1[1];
			}
			if ((int)strArrays1.Length > 2)
			{
				strArrays[1] = string.Join("\"", strArrays1, 2, (int)strArrays1.Length - 2);
			}
			if (!strArrays[1].Contains("%1"))
			{
				strArrays[1] = string.Concat(strArrays[1], " \"%1\"");
			}
			return strArrays;
		}

		private void SetButtonsBehavior()
		{
			switch (this._buttons)
			{
				case CustomMessageBox.CustomMessageBoxButtons.OK:
				{
					this.wButton1.Visible = false;
					this.wButton2.Text = "OK";
					base.AcceptButton = this.wButton2;
					base.CancelButton = null;
					return;
				}
				case CustomMessageBox.CustomMessageBoxButtons.YesNo:
				{
					this.wButton1.Text = "Yes";
					this.wButton2.Text = "No";
					base.AcceptButton = this.wButton1;
					base.CancelButton = this.wButton2;
					return;
				}
				default:
				{
					return;
				}
			}
		}

		private void wButton1_Click(object sender, EventArgs e)
		{
			switch (this._buttons)
			{
				case CustomMessageBox.CustomMessageBoxButtons.OK:
				{
					throw new InvalidOperationException("The user clicked the button 1 and it should not be available");
				}
				case CustomMessageBox.CustomMessageBoxButtons.YesNo:
				{
					base.DialogResult = System.Windows.Forms.DialogResult.Yes;
					base.Close();
					return;
				}
			}
			throw new NotImplementedException();
		}

		private void wButton2_Click(object sender, EventArgs e)
		{
			switch (this._buttons)
			{
				case CustomMessageBox.CustomMessageBoxButtons.OK:
				{
					base.DialogResult = System.Windows.Forms.DialogResult.OK;
					break;
				}
				case CustomMessageBox.CustomMessageBoxButtons.YesNo:
				{
					base.DialogResult = System.Windows.Forms.DialogResult.No;
					break;
				}
				default:
				{
					throw new NotImplementedException();
				}
			}
			base.Close();
		}

		private void wLinkLabelText_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			string[] strArrays = null;
			string empty = string.Empty;
			try
			{
				strArrays = this.RetrieveWebApplicationData();
				if (!string.IsNullOrEmpty(strArrays[0]))
				{
					empty = strArrays[1].Replace("%1", this._link);
					Process.Start(strArrays[0], empty);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CustomMessageBox.mLogger.ErrorFormat("Problem running {0} {1}", strArrays[0], empty);
				CustomMessageBox.mLogger.Error(string.Concat("wLinkLabelText_LinkClicked Exception: ", exception.ToString()));
			}
		}

		public enum CustomMessageBoxButtons
		{
			OK,
			YesNo
		}

		public enum CustomMessageBoxIcon
		{
			Exclamation = 1,
			Question = 2,
			Error = 3,
			Information = 4
		}

		private struct LinkDetection
		{
			private LinkArea _area;

			private bool _linkDetected;

			public LinkArea Area
			{
				get
				{
					return this._area;
				}
			}

			public bool LinkDetected
			{
				get
				{
					return this._linkDetected;
				}
			}

			public LinkDetection(LinkArea area, bool linkDetected)
			{
				this._area = area;
				this._linkDetected = linkDetected;
			}
		}

		internal static class UnsafeNativeMethods
		{
			[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
			public static extern int EnableMenuItem(IntPtr hMenu, int wIDEnableItem, int wEnable);

			[DllImport("shell32.dll", CharSet=CharSet.None, ExactSpelling=false)]
			public static extern IntPtr ExtractAssociatedIconEx(IntPtr intPtr, string p, ref IntPtr intPtr_3, out IntPtr intPtr_4);

			[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
			public static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
		}
	}
}