using System;
using System.Runtime.CompilerServices;

namespace GameLauncher.ProdUI
{
	public class ShardInfo
	{
		public int RegionId
		{
			get;
			set;
		}

		public string RegionName
		{
			get;
			set;
		}

		public string ShardKey
		{
			get
			{
				return string.Format("{0} - {1}", this.ShardName, this.RegionName);
			}
		}

		public string ShardName
		{
			get;
			set;
		}

		public string Url
		{
			get;
			set;
		}

		public ShardInfo()
		{
		}
	}
}