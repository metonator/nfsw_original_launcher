using GameLauncher;
using GameLauncher.ProdUI.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace GameLauncher.ProdUI
{
	public class Tos : Form
	{
		private IContainer components;

		private TextBox wTextBoxTOS;

		private Label wLabelTOS;

		private ImageList wImageListButton96;

		private ImageList wImageListButton120;

		private ImageList wImageListButton144;

		private Label wLabelButtonAccept;

		private Label wLabelButtonDecline;

		private bool mDragging;

		private Point mDraggingStart;

		private Point mDraggingCursorStart;

		private bool mLabelButtonDeclineHOver;

		private bool mLabelButtonAcceptHOver;

		public Tos(string tos)
		{
			this.InitializeComponent();
			this.wTextBoxTOS.Text = tos.Replace("\n", Environment.NewLine);
			this.wTextBoxTOS.Select(this.wTextBoxTOS.Text.Length, 0);
			this.ApplyEmbeddedFonts();
			this.ApplyLocalization();
		}

		private void ApplyEmbeddedFonts()
		{
			FontFamily fontFamily = FontWrapper.Instance.GetFontFamily("MyriadProSemiCondBold.ttf");
			FontFamily fontFamily1 = FontWrapper.Instance.GetFontFamily("Reg-B-I.ttf");
			FontFamily fontFamily2 = FontWrapper.Instance.GetFontFamily("Reg-DB-I.ttf");
			this.wLabelTOS.Font = new System.Drawing.Font(fontFamily1, 12f, FontStyle.Bold | FontStyle.Italic);
			this.wTextBoxTOS.Font = new System.Drawing.Font(fontFamily, 9f, FontStyle.Bold);
			this.wLabelButtonAccept.Font = new System.Drawing.Font(fontFamily2, 14f, FontStyle.Italic);
			this.wLabelButtonDecline.Font = new System.Drawing.Font(fontFamily2, 14f, FontStyle.Italic);
		}

		private void ApplyLocalization()
		{
			this.wLabelTOS.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "TOS00001");
			this.wLabelButtonDecline.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "TOS00003");
			this.wLabelButtonAccept.Text = ResourceWrapper.Instance.GetString("GameLauncher.ProdUI.LanguageStrings", "TOS00004");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(Tos));
			this.wTextBoxTOS = new TextBox();
			this.wLabelTOS = new Label();
			this.wImageListButton96 = new ImageList(this.components);
			this.wImageListButton120 = new ImageList(this.components);
			this.wImageListButton144 = new ImageList(this.components);
			this.wLabelButtonAccept = new Label();
			this.wLabelButtonDecline = new Label();
			base.SuspendLayout();
			this.wTextBoxTOS.BackColor = Color.Black;
			this.wTextBoxTOS.Font = new System.Drawing.Font("MyriadProSemiCondBold", 8.999999f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.wTextBoxTOS.ForeColor = Color.White;
			this.wTextBoxTOS.Location = new Point(19, 31);
			this.wTextBoxTOS.Multiline = true;
			this.wTextBoxTOS.Name = "wTextBoxTOS";
			this.wTextBoxTOS.ReadOnly = true;
			this.wTextBoxTOS.ScrollBars = ScrollBars.Vertical;
			this.wTextBoxTOS.Size = new System.Drawing.Size(481, 353);
			this.wTextBoxTOS.TabIndex = 3;
			this.wLabelTOS.BackColor = Color.Transparent;
			this.wLabelTOS.Font = new System.Drawing.Font("Reg-B-I", 12f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLabelTOS.ForeColor = Color.FromArgb(76, 178, 255);
			this.wLabelTOS.Location = new Point(18, 6);
			this.wLabelTOS.Name = "wLabelTOS";
			this.wLabelTOS.Size = new System.Drawing.Size(481, 21);
			this.wLabelTOS.TabIndex = 2;
			this.wLabelTOS.Text = "NFS WORLD TERMS OF SERVICE";
			this.wLabelTOS.TextAlign = ContentAlignment.TopCenter;
			this.wLabelTOS.MouseMove += new MouseEventHandler(this.Tos_MouseMove);
			this.wLabelTOS.MouseDown += new MouseEventHandler(this.Tos_MouseDown);
			this.wLabelTOS.MouseUp += new MouseEventHandler(this.Tos_MouseUp);
			this.wImageListButton96.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButton96.ImageStream");
			this.wImageListButton96.TransparentColor = Color.Red;
			this.wImageListButton96.Images.SetKeyName(0, "nfsw_lp_96dpi_bg_s2_bt_logout_enabled.bmp");
			this.wImageListButton96.Images.SetKeyName(1, "nfsw_lp_96dpi_bg_s2_bt_logout_rollover.bmp");
			this.wImageListButton96.Images.SetKeyName(2, "nfsw_lp_96dpi_bg_s2_bt_logout_down.bmp");
			this.wImageListButton120.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButton120.ImageStream");
			this.wImageListButton120.TransparentColor = Color.Red;
			this.wImageListButton120.Images.SetKeyName(0, "nfsw_lp_120dpi_bg_s2_bt_logout_enabled.bmp");
			this.wImageListButton120.Images.SetKeyName(1, "nfsw_lp_120dpi_bg_s2_bt_logout_rollover.bmp");
			this.wImageListButton120.Images.SetKeyName(2, "nfsw_lp_120dpi_bg_s2_bt_logout_down.bmp");
			this.wImageListButton144.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("wImageListButton144.ImageStream");
			this.wImageListButton144.TransparentColor = Color.Red;
			this.wImageListButton144.Images.SetKeyName(0, "nfsw_lp_144dpi_bg_s2_bt_logout_enabled.bmp");
			this.wImageListButton144.Images.SetKeyName(1, "nfsw_lp_144dpi_bg_s2_bt_logout_rollover.bmp");
			this.wImageListButton144.Images.SetKeyName(2, "nfsw_lp_144dpi_bg_s2_bt_logout_down.bmp");
			this.wLabelButtonAccept.BackColor = Color.Transparent;
			this.wLabelButtonAccept.Font = new System.Drawing.Font("Reg-DB-I", 14.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLabelButtonAccept.ForeColor = Color.White;
			this.wLabelButtonAccept.ImageIndex = 0;
			this.wLabelButtonAccept.ImageList = this.wImageListButton96;
			this.wLabelButtonAccept.Location = new Point(374, 394);
			this.wLabelButtonAccept.Name = "wLabelButtonAccept";
			this.wLabelButtonAccept.Size = new System.Drawing.Size(131, 31);
			this.wLabelButtonAccept.TabIndex = 4;
			this.wLabelButtonAccept.Text = "I ACCEPT";
			this.wLabelButtonAccept.TextAlign = ContentAlignment.MiddleCenter;
			this.wLabelButtonAccept.MouseLeave += new EventHandler(this.wLabelButtonAccept_MouseLeave);
			this.wLabelButtonAccept.Click += new EventHandler(this.wLabelButtonAccept_Click);
			this.wLabelButtonAccept.MouseDown += new MouseEventHandler(this.wLabelButtonAccept_MouseDown);
			this.wLabelButtonAccept.MouseUp += new MouseEventHandler(this.wLabelButtonAccept_MouseUp);
			this.wLabelButtonAccept.MouseEnter += new EventHandler(this.wLabelButtonAccept_MouseEnter);
			this.wLabelButtonDecline.BackColor = Color.Transparent;
			this.wLabelButtonDecline.Font = new System.Drawing.Font("Reg-DB-I", 14.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
			this.wLabelButtonDecline.ForeColor = Color.White;
			this.wLabelButtonDecline.ImageIndex = 0;
			this.wLabelButtonDecline.ImageList = this.wImageListButton96;
			this.wLabelButtonDecline.Location = new Point(18, 394);
			this.wLabelButtonDecline.Name = "wLabelButtonDecline";
			this.wLabelButtonDecline.Size = new System.Drawing.Size(131, 31);
			this.wLabelButtonDecline.TabIndex = 5;
			this.wLabelButtonDecline.Text = "I DECLINE";
			this.wLabelButtonDecline.TextAlign = ContentAlignment.MiddleCenter;
			this.wLabelButtonDecline.MouseLeave += new EventHandler(this.wLabelButtonDecline_MouseLeave);
			this.wLabelButtonDecline.Click += new EventHandler(this.wLabelButtonDecline_Click);
			this.wLabelButtonDecline.MouseDown += new MouseEventHandler(this.wLabelButtonDecline_MouseDown);
			this.wLabelButtonDecline.MouseUp += new MouseEventHandler(this.wLabelButtonDecline_MouseUp);
			this.wLabelButtonDecline.MouseEnter += new EventHandler(this.wLabelButtonDecline_MouseEnter);
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = Color.Black;
			this.BackgroundImage = Resources.nfsw_lp_tos_96dpi_bg;
			base.ClientSize = new System.Drawing.Size(517, 438);
			base.Controls.Add(this.wLabelButtonDecline);
			base.Controls.Add(this.wLabelButtonAccept);
			base.Controls.Add(this.wLabelTOS);
			base.Controls.Add(this.wTextBoxTOS);
			this.ForeColor = Color.FromArgb(51, 153, 255);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			base.Icon = (System.Drawing.Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(517, 438);
			base.Name = "Tos";
			base.ShowInTaskbar = false;
			this.Text = "Terms of Service";
			base.TopMost = true;
			base.TransparencyKey = Color.Red;
			base.MouseUp += new MouseEventHandler(this.Tos_MouseUp);
			base.MouseDown += new MouseEventHandler(this.Tos_MouseDown);
			base.MouseMove += new MouseEventHandler(this.Tos_MouseMove);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private void Tos_MouseDown(object sender, MouseEventArgs e)
		{
			this.mDragging = true;
			this.mDraggingStart = base.Location;
			this.mDraggingCursorStart = System.Windows.Forms.Cursor.Position;
		}

		private void Tos_MouseMove(object sender, MouseEventArgs e)
		{
			if (this.mDragging)
			{
				int x = this.mDraggingStart.X;
				Point position = System.Windows.Forms.Cursor.Position;
				int num = x + position.X - this.mDraggingCursorStart.X;
				int y = this.mDraggingStart.Y;
				Point point = System.Windows.Forms.Cursor.Position;
				base.Location = new Point(num, y + point.Y - this.mDraggingCursorStart.Y);
			}
		}

		private void Tos_MouseUp(object sender, MouseEventArgs e)
		{
			this.mDragging = false;
		}

		private void wLabelButtonAccept_Click(object sender, EventArgs e)
		{
			base.DialogResult = System.Windows.Forms.DialogResult.OK;
			base.Close();
		}

		private void wLabelButtonAccept_MouseDown(object sender, MouseEventArgs e)
		{
			this.wLabelButtonAccept.ImageIndex = 2;
		}

		private void wLabelButtonAccept_MouseEnter(object sender, EventArgs e)
		{
			if (!this.mLabelButtonAcceptHOver)
			{
				this.wLabelButtonAccept.ImageIndex = 1;
				this.mLabelButtonAcceptHOver = true;
			}
		}

		private void wLabelButtonAccept_MouseLeave(object sender, EventArgs e)
		{
			if (this.mLabelButtonAcceptHOver)
			{
				this.wLabelButtonAccept.ImageIndex = 0;
				this.mLabelButtonAcceptHOver = false;
			}
		}

		private void wLabelButtonAccept_MouseUp(object sender, MouseEventArgs e)
		{
			this.wLabelButtonAccept.ImageIndex = 1;
		}

		private void wLabelButtonDecline_Click(object sender, EventArgs e)
		{
			base.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			base.Close();
		}

		private void wLabelButtonDecline_MouseDown(object sender, MouseEventArgs e)
		{
			this.wLabelButtonDecline.ImageIndex = 2;
		}

		private void wLabelButtonDecline_MouseEnter(object sender, EventArgs e)
		{
			if (!this.mLabelButtonDeclineHOver)
			{
				this.wLabelButtonDecline.ImageIndex = 1;
				this.mLabelButtonDeclineHOver = true;
			}
		}

		private void wLabelButtonDecline_MouseLeave(object sender, EventArgs e)
		{
			if (this.mLabelButtonDeclineHOver)
			{
				this.wLabelButtonDecline.ImageIndex = 0;
				this.mLabelButtonDeclineHOver = false;
			}
		}

		private void wLabelButtonDecline_MouseUp(object sender, MouseEventArgs e)
		{
			this.wLabelButtonDecline.ImageIndex = 1;
		}
	}
}