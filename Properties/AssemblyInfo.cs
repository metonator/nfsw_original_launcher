﻿using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Electronic Arts Inc")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © Electronic Arts Inc 2010")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.8.40.1599")]
[assembly: AssemblyProduct("NFSW Launcher")]
[assembly: AssemblyTitle("NFSW Launcher")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.8.40.1599")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: Guid("4d5e10fd-0c91-4216-925c-d3cb89d98db0")]
[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
